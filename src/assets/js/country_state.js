$(document).ready(function(){
  // For State Loads Starts
  $("#country").change(function (e){
    var state = 
    {
      '0': ['Not Available'],
      'Z1':['Australian Capital Territory','New South Wales','Northern Territory','Queensland','South Australia','Tasmania','Victoria','Western Australia'],
      'IN':['Delhi','Maharashtra','TamilNadu','Karnataka','Haryana','Uttar Pradesh','Andhra Pradesh','Jammu and Kashmir','West Bengal','Gujarat','Madhya Pradesh','Kerala','Punjab','Bihar','Rajasthan','Orissa','Assam','NA','Himachal Pradesh','Chhattisgarh'],
      'MY':['Johor','Kedah','Kelantan','Kuala Lumpur','Labuan','Melaka','Negeri Sembilan','Pahang','Penang','Perak','Perlis','Sabah','Sarawak','Selangor','Terengganu'],
      'CN':['Beijing','Anhui','Chongqing','Fujian','Gansu','Guangdong','Guangxi','Guizhou','Hainan','Hebei','Heilongjiang','Henan','Hubei','Hunan','Jiangsu','Jiangxi','Jilin','Liaoning','Nei','Mongol','Ningxia','Qinghai','Shaanxi','Shandong','Shanghai','Shanxi','Sichuan','Tianjin','Xinjiang','Xizang','(Tibet)','Yunnan','Zhejiang']
    }    
    var value = this.value;
    // Access the object like city['CT'] .. That gives the Array
    state[value] !== undefined ? state[value] : '0';
    var stateOptions = state[value];
    var $field8 = $('#field8'),
        $field9 = $('#field9');  
        $field8.html(''); // clear the existing options  
        $field9.html(''); // clear the existing options                
    $.each(stateOptions, function (i, o) {
      $('<option>' + o + '</option>').appendTo('#field8');
    });// ------------>each end tag          
  });//----------------->on-change end tag
  // For State Loads Ended
  // For City Loads Starts
  $("#field8").change(function (e){     
      var city = 
      {
        '0': ['Not Available'],
        'TamilNadu' : ['Coimbatore','Mettupalyam','Karur','Pollachi'],
        'Karnataka' : ['Bangalore','Mysore','Bellary','Mandya','Chikmagalur'
        ,'Chamarajanagar','Udupi','Chikkaballapura'],
        'Delhi'     : ['Faridabad','Panchkula','Sonipat','Hisar','Palwal','Thanesar']         
      }
      var value = this.value;
      city[value] !== undefined ? city[value] : '0';
      var cityOptions = city[value];
      var $field9 = $('#field9');              
      $field9.html(''); // clear the existing options 
      $.each(cityOptions, function (j, k) {
        $('<option>' + k + '</option>').appendTo('#field9');
      });// ------------>each end tag
  });//----------------->on-change end tag
  // For City Loads Ended
});