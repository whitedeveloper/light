/*

Template:  Webmin - Bootstrap 4 & Angular 5 Admin Dashboard Template
Author: potenzaglobalsolutions.com
Design and Developed by: potenzaglobalsolutions.com

NOTE: This file contains all scripts for the actual Template.

*/

/*================================================
[  Table of contents  ]
================================================

:: Predefined Variables 
:: Tooltip
:: Preloader
:: PHP Contact Form 
:: Counter
:: Back to top
:: NiceScroll
:: Mailchimp
:: PieChart 
:: DataTable
:: Wow animation on scroll
:: Select
:: Accordion
:: Search
:: Sidebarnav
:: Fullscreenwindow
:: Today date and time
:: Summernote
:: Colorpicker
:: Touchspin
:: Editormarkdown
:: Rating
:: Calendar List View
:: Repeater form
:: Wizard form
 
======================================
[ End table content ]
======================================*/
//POTENZA var
 
 (function($){
  "use strict";
  var POTENZA = {};

  /*************************
  Predefined Variables
*************************/ 
var $window = $(window),
    $document = $(document),
    $body = $('body'),
    $countdownTimer = $('.countdown'),
    $bar = $('.bar'),
    $pieChart = $('.round-chart'),
    $counter = $('.counter'),
    $datetp = $('.datetimepicker');
    //Check if function exists
    $.fn.exists = function () {
        return this.length > 0;
    };

/*************************
      Tooltip
*************************/
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover()
  
/*************************
        Preloader
*************************/  
  POTENZA.preloader = function () {
       $("#load").fadeOut();
       $('#pre-loader').delay(0).fadeOut('slow');
   };   

/*************************
      select
*************************/ 
POTENZA.fancyselect = function () {
  if ($('.fancyselect').exists()) {
    loadScript(plugin_path + 'jquery-nice-select/jquery-nice-select.js', function() {
       $('select.fancyselect:not(.ignore)').niceSelect();      
      });
    }     
};

/*************************
       Search
*************************/ 
POTENZA.searchbox = function () {
  //  if (jQuery('.search').exists()) {
  //     jQuery('.search-btn').on("click", function () {
  //       jQuery('.search').toggleClass("search-open");
  //       if( $('.search').hasClass('search-open')){
  //         // $('.cdk-overlay-container').appendTo('.search');
  //         $('.cdk-overlay-container #mat-autocomplete-2').addClass('d-none');
  //       }
  //       else{
  //         $('.cdk-overlay-container #mat-autocomplete-2').removeClass('d-none');
  //         // $('.cdk-overlay-container').appendTo('body');
  //       }
  //       return false;
  //     });
  //   //    jQuery("html, body").on('click', function (e) {
  //   //     if (!jQuery(e.target).hasClass("not-click")) {

  //   //          jQuery('.search').removeClass("search-open");
  //   //      }
  //   //  });
  //   }
}     

/*************************
    Sidebarnav
*************************/ 
POTENZA.Sidebarnav = function () { 
  /*Sidebar Navigation*/
    $(document).on('click', '#button-toggle', function (e) {
      $(".dropdown.open > .dropdown-toggle").dropdown("toggle");
      return false;
    });
    $(document).on('click', '#button-toggle', function (e) {
      $('.wrapper').toggleClass('slide-menu');
      return false;
    });
    /* Hide Sidebar When Click on Item Only In Mobile View */ 

    $( "body" ).delegate( "#sidebarnav .nav_item", "click", function() {
      if(window.innerWidth < 992){
        $('.wrapper').removeClass('slide-menu');
      }
    });

    // $(document).on("mouseenter mouseleave",".wrapper > .side-menu-fixed", function(e) {
    //   if (e.type == "mouseenter") {
    //     $wrapper.addClass("sidebar-hover"); 
    //   }
    //   else { 
    //     $wrapper.removeClass("sidebar-hover");  
    //   }
    //   return false;
    // });
    // $(document).on("mouseenter mouseleave",".wrapper > .side-menu-fixed", function(e) {
    //   if (e.type == "mouseenter") {
    //     $wrapper.addClass("sidebar-hover"); 
    //   }
    //   else { 
    //     $wrapper.removeClass("sidebar-hover");  
    //   }
    //   return false;
    // });
    
    // $(document).on("mouseenter mouseleave",".wrapper > .setting-panel", function(e) {
    //   if (e.type == "mouseenter") {
    //     $wrapper.addClass("no-transition"); 
    //   }
    //   else { 
    //     $wrapper.removeClass("no-transition");  
    //   }
    //   return false;
    // });
}

$("#back-to-top").on("click", function(e){
  $("html, body").animate({
      scrollTop: 0
  }, 600);
  return false;
});

/*************************
    Fullscreenwindow
*************************/ 

POTENZA.Fullscreenwindow = function () { 
    if ($('#btnFullscreen').exists()) {
   function toggleFullscreen(elem) {
    elem = elem || document.documentElement;
    if (!document.fullscreenElement && !document.mozFullScreenElement &&
      !document.webkitFullscreenElement && !document.msFullscreenElement) {
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
      } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }
  }
  document.getElementById('btnFullscreen').addEventListener('click', function() {
    toggleFullscreen();
  });
 }
}

/*************************
   Dynamic active menu
*************************/ 

POTENZA.navactivation = function () { 
    var path = window.location.pathname.split("/").pop();
    var target = $('.side-menu-fixed .navbar-nav a[href="'+path+'"]');
    target.parent().addClass('active');        
    $('.side-menu-fixed .navbar-nav li.active').parents('li').addClass('active');
}

/*************************
   Style Customizer
*************************/ 

POTENZA.stylecustomizer = function () { 
   var style_switcher = $('.style-customizer'),
    panelWidth = style_switcher.outerWidth(true);
    $('.style-customizer .opener').on("click", function(){
      var $this = $(this);
      if ($(".style-customizer.closed").length>0) {
        style_switcher.animate({"right" : "0px"});
        $(".style-customizer.closed").removeClass("closed");
        $(".style-customizer").addClass("opened");
      } else {
        $(".style-customizer.opened").removeClass("opened");
        $(".style-customizer").addClass("closed");
        style_switcher.animate({"right" : '-' + panelWidth});
      }
      return false;
    });

    $("input[name='sidebarnar_skin']").change(function() {    
     var l = $("input[name='sidebarnar_skin']:checked").val();     
     if(l != 0) $('.side-menu-fixed').addClass('light-side-menu');
     else $('.side-menu-fixed').removeClass('light-side-menu');
    });

    $("input[name='header_skin']").change(function() {    
         var l = $("input[name='header_skin']:checked").val();     
         if(l != 0) $('.admin-header').addClass('header-dark');
         else $('.admin-header').removeClass('header-dark');
    });
}

/****************************************************
          javascript
****************************************************/
var _arr  = {};
  function loadScript(scriptName, callback) {
    if (!_arr[scriptName]) {
      _arr[scriptName] = true;
      var body    = document.getElementsByTagName('body')[0];
      var script    = document.createElement('script');
      script.type   = 'text/javascript';
      script.src    = scriptName;
      // then bind the event to the callback function
      // there are several events for cross browser compatibility
      // script.onreadystatechange = callback;
      script.onload = callback;
      // fire the loading
      // body.appendChild(script);
    } else if (callback) {
      callback();
    }
  };

/****************************************************
     POTENZA Window load and functions
****************************************************/
  //Window load functions
    $window.on("load",function(){
          POTENZA.preloader()
          // POTENZA.pieChart();
    });
 //Document ready functions
    $document.ready(function () {
        // POTENZA.counters(),
        // POTENZA.goToTop(),
        // POTENZA.pniceScroll(),
        // POTENZA.mailchimp(),
        // POTENZA.accordion(),
        // POTENZA.datatables(),
        // POTENZA.wowanimation(),
        POTENZA.fancyselect(),
        POTENZA.searchbox(),
        POTENZA.Sidebarnav(),
        // POTENZA.todatdayandtime(),
        // POTENZA.summernoteeditor(),
        // POTENZA.colorpicker(),
        // POTENZA.editormarkdown(),
        // POTENZA.ptTouchSpin(),
        // POTENZA.ptrating(),
        // POTENZA.calendarlist(),
        // POTENZA.repeaterform(),
        // POTENZA.wizardform(),
        POTENZA.navactivation(),
        POTENZA.stylecustomizer(),
        POTENZA.Fullscreenwindow();
    });
})(jQuery);

/****************************************************
     Fix Header While scrolling down
****************************************************/

