import { Component, OnInit, Input, Output, EventEmitter,AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  ViewContainerRef} from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { User, UserService, ApiService, Errors } from '../../core';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { ToastsManager } from 'ng6-toastr';
import { Address } from '../../core/models/address'
import { Jsonp } from '@angular/http';
  
declare var Stripe: any;
@Component({
  selector: 'app-transcribe',
  templateUrl: './transcribe.component.html',
  styleUrls: ['./transcribe.component.css']
})
export class TranscribeComponent implements AfterViewInit, OnDestroy,OnInit {
  ngForm:FormGroup;
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.makeFormControl();
    this.setFormValue();
  }
  @ViewChild('cardInfo') cardInfo: ElementRef;
  currentUser:User
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  pamount:string;
 addr:Address;
  constructor(private cd: ChangeDetectorRef,private http: HttpClient,
    private userService:UserService,private router:Router,
     public toastr: ToastsManager, vcr: ViewContainerRef,
    private fb:FormBuilder,private location:Location) {
      this.toastr.setRootViewContainerRef(vcr); 
       this.addr=new Address();
      this.ngForm=this.fb.group(
        {   'firstname':['',Validators.required],
            'lastname':['',Validators.required],
            'address':['',Validators.required],
            'city':['',Validators.required],
            'country':['',Validators.required],
            'email':['',Validators.required],
            'phone':['',Validators.required],
            'pamount':['',Validators.required],
            'amount': ['',Validators.nullValidator]
            
        }
      )
  }

  ngAfterViewInit() {
    const style = {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '15px',

        '::placeholder': {
            color: '#CFD7E0',
        },
      },
    };
  
    this.card = elements.create('card', { style });
    this.card.mount(this.cardInfo.nativeElement);
  
    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);

    if (error) {
      this.toastr.error("Something went wrong. Try again!!!")
    } else {
      var credentials = this.ngForm.value;
      var amount=credentials['amount']
      var pamount= credentials['pamount']
            if(pamount){
              amount=pamount;
            }
           // form.controls['pay'].setValue('$ '+amount); 
      let headers_im= new HttpHeaders();
        let headers=    headers_im.append('token', token.id)
            .append('amount',amount)
            .append('email',this.currentUser.email); 
            this.addr.email=this.currentUser.email;
            this.addr.city=credentials['city'];
            this.addr.country=credentials['country'];
            this.addr.housNo=credentials['address'];
            this.addr.phoneNo=credentials['phone'];
            this.addr.postalCode=credentials['postalCode'];
            let optionsH = {headers:headers}; 
            this.http.post('https://api.light.sx/ext-api/v1/payment/charge', {address:this.addr}, optionsH)
              .subscribe(data => {
                this.toastr.success("Credits Addedd Successfully");
                this.router.navigateByUrl('/addcredit');
                this.resetForm();
              },
              err => {
                this.error=err;
                this.toastr.error("Something went wrong. Try again!!!")
                this.router.navigateByUrl('/addcredit');
             
               });
              
    }
  }
  makeFormControl(){
    this.ngForm.addControl('firstname',new FormControl());
    this.ngForm.addControl('email',new FormControl());
    this.ngForm.addControl('city',new FormControl());
    this.ngForm.addControl('country',new FormControl());
    this.ngForm.addControl('lastname',new FormControl());
    this.ngForm.addControl('address',new FormControl());
    this.ngForm.addControl('phone',new FormControl());
    this.ngForm.addControl('pamount',new FormControl);
    this.ngForm.addControl('amount',new FormControl);
   
  }
   back():void {
     this.location.back();
     }
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
 setFormValue(){
  this.ngForm.controls['phone'].setValue('');
    this.ngForm.controls['firstname'].setValue(this.currentUser.firstName);
    this.ngForm.controls['lastname'].setValue(this.currentUser.lastName);
    this.ngForm.controls['email'].setValue(this.currentUser.email);
    this.ngForm.controls['address'].setValue('');
    this.ngForm.controls['city'].setValue('');
    this.ngForm.controls['country'].setValue('');
 }
  resetForm(){
    this.ngForm.controls['phone'].setValue('');
    this.ngForm.controls['firstname'].setValue('');
    this.ngForm.controls['lastname'].setValue('');
    this.ngForm.controls['email'].setValue('');
    this.ngForm.controls['address'].setValue('');
    this.ngForm.controls['city'].setValue('');
    this.ngForm.controls['country'].setValue('');
  }

}
