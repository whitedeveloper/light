import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SharedModule } from '../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { TranscribeRoutingModule } from './transcribe-routing.module';
import { TranscribeComponent } from './transcribe/transcribe.component';

@NgModule({
  imports: [
    CommonModule,
    TranscribeRoutingModule,
    MatRadioModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatRippleModule,
  ],
  exports:[
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    TranscribeComponent
  ],
  declarations: [TranscribeComponent]
})
export class TranscribeModule { }
