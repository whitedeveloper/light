import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { User } from '../../core/models/user.model';
import { Errors } from '../../core/models/errors.model';
import { UserService } from '../../core/services/user.service';
import { Router } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { ToastsManager } from 'ng6-toastr';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { Listenlater } from '../../core/models/listenlater';
import { PlayerService } from '../../core/services/player.service';
import { Inprogress } from '../../core/models/inprogress';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { timingSafeEqual } from 'crypto';
import { CartService } from '../../core/services/cart.service';
import {CommonModalComponent} from '../../common/modal.component';

@Component({
  selector: 'app-inprogress',
  templateUrl: './inprogress.component.html',
  styleUrls: ['./inprogress.component.css']
})
export class InprogressComponent implements OnInit {
  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;

  currentUser: User;
  podcasts: Inprogress;
  // inProgress: Array<any> = [];
  errors: Errors = {errors: {}};
  sort_data = [ { key:"date_big", value:"Created Date((New → Old)" }, { key:"date", value:"Created Date(Old → New)" }, 
                { key:"name", value:"Episode Name(A → Z)" }, { key:"name_big", value:"Episode Name(Z → A)" }]
                
  action_data = [ { key:"transcribe", value:"Transcribe" }, { key:"listen_later", value:"Listen Later" } ]
  
  
  
  modal_episode: any;
  modal_podcast: any;
  modal_index: number;
  scroll_limit: any;

  constructor(private userService:UserService,private router:Router,
    private lightService:LightService, public toastr: ToastsManager, vcr: ViewContainerRef,
     public playerService:PlayerService, private modalService: NgbModal, private cartService:CartService) {
      this.toastr.setRootViewContainerRef(vcr);

  }
  ngOnInit() {
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.userService.currentUser.subscribe(
            (userData) => {
              this.currentUser = userData;
            }
          );
          this.scroll_limit = 10; 
          this.playerService.selectedAction = 'transcribe';
        } 
        else {
          this.router.navigateByUrl('/register');
        }
      }
    ); 
  }
  isPlayed: boolean = false;
  toggleChild(){
    this.isPlayed = !this.isPlayed;
  }
  playAudios(event, fileUrl, episode){
    if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == true)
    {
      this.playerService.api.pause();
      this.playerService.isPlaying = false;
    }
    else if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == false)
    {
      this.playerService.api.play();
      this.playerService.isPlaying = true;
    }
    else{
      this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
      .subscribe((res:any)=>{
        this.playerService.playAudio(fileUrl, episode.podcastName, res.releaseDate, episode.episodeName, episode.img, episode, episode.timeLeft);
        this.isPlayed=true;
        this.playerService.isAudioChanged = true;
      })
    }
  }
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  isAuthenticated: boolean;
  success(message: string) { 
    this.toastr.success(message);
  }
  
  error(message: string) {
    this.toastr.error(message);
  }

  subscribedPodcasts(podcastId:string){
    if(this.isAuthenticated){
      this.lightService.subscribedPodcasts(podcastId)
      .subscribe( data => {
        // this.success(data['message']);
        this.success('Subscribed to Podcast Successfully');
        //this.router.navigateByUrl('/')
      },
      err => {
        this.error("Podcast subscribed failed")
        this.errors = err;
      });
    }
    else{
      this.router.navigateByUrl('/login');

    }
  }
  listenLater(podcastId:string,episodeId:string){
    if(this.isAuthenticated){
    this.lightService.listenLater(podcastId,episodeId)
    .subscribe( data => {
      this.success(data['message']);
    },
    err => {
      this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
      this.errors = err;
    });
    }else{
      this.router.navigateByUrl('/login');
    }
  }
  apply_action(cartModal){
    for (var i = 0; i < this.playerService.selectValues.length; i++) {
      if(this.playerService.selectValues[i].selected == true)
      { 
        if(this.playerService.selectedAction == 'transcribe'){
          this.transcribe(i);
        }
        else
        {
          var item = this.playerService.inProgress[i];
          this.listenLater(item.podcastId, item.episodeId);
        }
      }
    }
    if(this.playerService.selectedAction == 'transcribe'){
      this.modalService.open(cartModal, { size: 'lg' });
    }
  }
  apply_transcribe(index, cartModal){
    this.transcribe(index);
    this.modalService.open(cartModal, { size: 'lg' });
  }
  transcribe(index){
    var item = this.playerService.inProgress[index];
    this.addToCart(item.episodeId, item.podcastName, item.episodeName, this.secondsToMinute(item.playtime), item.img, item.fileUrl, item.podcastId, );
  }
  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
    this.cartService.addEpisodes(this.currentUser.userid, id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
    .subscribe(data=>{
      this.success(data['message']);
      this.cartService.loadCartItems();
      
    });
  }  
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if( _seconds.indexOf(':') >= 0){
      if(_seconds.split(":").length == 2)
        return "00:"+_seconds;
      return _seconds;  
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  open_read_more_modal(content, episode, podkast, index) {
    this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
    .subscribe((res:any)=>{
      this.modal_episode = res;
      console.log(res)
    })
    // this.modal_episode = episode;
    this.modal_podcast = podkast;
    this.modal_index = index;
    this.modalService.open(content, { size: 'lg' });
  }
  onScroll(){
    this.scroll_limit += 5;
  }
  creditCheckout(link){
    if (this.cartService.checkOut() == false){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
      modalRef.componentInstance.src = link;
    }
    else{
      this.modalService.dismissAll();
    }
  }
}
