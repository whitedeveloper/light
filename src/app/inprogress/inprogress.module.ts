import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InprogressRoutingModule } from './inprogress-routing.module';
import { InprogressComponent } from './inprogress/inprogress.component';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { SharedModule } from '../shared/shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    InprogressRoutingModule,
    SharedModule,
    AudioPlayerModule,
    InfiniteScrollModule
  ],
  declarations: [InprogressComponent]
})
export class InprogressModule { }
