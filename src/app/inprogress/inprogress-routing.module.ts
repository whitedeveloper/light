import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InprogressComponent } from './inprogress/inprogress.component';
import { AuthGuard } from '../core';

const routes: Routes = [
  {
    path:'',
    component:InprogressComponent,canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InprogressRoutingModule { }
