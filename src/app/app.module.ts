import { NgtUniversalModule } from '@ng-toolkit/universal';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { ModuleWithProviders, NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { HomeModule } from './home/home.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  FooterComponent,
  HeaderComponent,
  SharedModule
} from './shared';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { LightService } from './core/services/light.service';
import { StripeService } from './core/services/stripe.service';
import { UserService, HttpTokenInterceptor, ApiService } from './core';
import { HTTP_INTERCEPTORS , HttpClientModule} from '@angular/common/http';
import { PaymentModule } from './payment/payment.module';
import { AudioPlayerModule } from './audio-player/audio-player.module';
import { AlertService } from './core/services/alert.service';
import {ToastModule} from 'ng6-toastr';
import { HashLocationStrategy, LocationStrategy, PathLocationStrategy , CommonModule} from '@angular/common';
import { YouTubeSearchService } from './search/search/you-tube-search.service';
import { HttpModule } from '@angular/http';
import { SearchModule } from './search/search.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { LoaderComponent } from './loader/loader.component';
import { LoaderInterceptorService } from './core/interceptors/loader.interceptor';
import { LoaderService } from './core/services/loader.service';
import { TransactionModule } from './transaction/transaction.module';
import {EpisodedetailModule} from './episodedetail/episodedetail.module'
import { TranscribeComponent } from './transcribe/transcribe/transcribe.component';
import { SafeHtmlPipe } from './core/services/safe-html';
import { CartService } from './core/services/cart.service';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { VgControlsModule } from 'videogular2/controls';
import { VgCoreModule } from 'videogular2/core';
import {CommonModalComponent} from './common/modal.component';
import {CommonModalModule} from './common/modal.module';
import {GoTopButtonModule} from 'ng2-go-top-button';
import { CookieService } from 'ngx-cookie-service';

enableProdMode()
@NgModule({
  declarations: [AppComponent, FooterComponent, HeaderComponent, LoaderComponent],
  imports:[
 CommonModule,
NgtUniversalModule,
 
 TransferHttpCacheModule,
HttpClientModule,
 
    CommonModalModule,
    
    CoreModule,
    SharedModule,
    HomeModule,
    AuthModule,
    MatAutocompleteModule,
    MatInputModule,
    MatRadioModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpModule,
    SearchModule,
    TransactionModule,
    AudioPlayerModule,
    NgbModule.forRoot( ),
    MDBBootstrapModule.forRoot(),
    ToastModule.forRoot(),
    NgxLoadingModule.forRoot({animationType: ngxLoadingAnimationTypes.circleSwish,
      backdropBackgroundColour: 'rgba(0,0,0,0.0)', 
      backdropBorderRadius: '10px',
      primaryColour: 'rgba(0,0,0,0.5)', 
      secondaryColour: 'rgba(0,0,0,0.5)', 
      tertiaryColour: 'rgba(0,0,0,0.5)'}),
    MatProgressSpinnerModule,
    VgCoreModule,
    VgControlsModule,
    GoTopButtonModule,
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    LoaderService, {
      provide:HTTP_INTERCEPTORS,
      useClass:LoaderInterceptorService,
      multi: true
    },
    LightService,
    StripeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    CartService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    YouTubeSearchService,{
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    ApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    AlertService,
    CookieService
  ],
  entryComponents: [CommonModalComponent]
})
export class AppModule {}
