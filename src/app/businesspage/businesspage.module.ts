import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinesspageRoutingModule } from './businesspage-routing.module';
import { BusinesspageComponent } from './businesspage/businesspage.component';
import { SharedModule } from '../shared';
import { HttpClientModule } from '@angular/common/http';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  imports: [
    CommonModule,
    BusinesspageRoutingModule,
    MatTabsModule,
    SharedModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatCardModule,
    HttpClientModule,
    CKEditorModule,
  ],
  declarations: [BusinesspageComponent],
  exports:[
    MatFormFieldModule,
    MatInputModule,
  ]
})
export class BusinesspageModule {
}
