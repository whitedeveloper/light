import { WINDOW } from '@ng-toolkit/universal';
import { User, UserService, Errors, ProfilesService } from '../../core';
import * as $ from 'jquery';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import {Location} from '@angular/common';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, Optional, Inject, forwardRef, ViewContainerRef } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { ToastsManager } from 'ng6-toastr';
import { BusinessProfile } from '../../core/models/businessprofile';
import { PublicProfile } from '../../core/models/publicprofile';
import { Router, ActivatedRoute } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-businesspage',
  templateUrl: './businesspage.component.html',
  styleUrls: ['./businesspage.component.css']
})
export class BusinesspageComponent implements OnInit{
  errors: Errors = {errors: {}};
  isSubmitting = false;
  profileForm: FormGroup;
  businessForm: FormGroup;
  businessProfile:BusinessProfile;
  publicProfile:PublicProfile;
  banner:File;
  public_banner:File;
  private isUploading:boolean = false;
  selectedFiles:FileList;
  public_selectedFiles:FileList;
  name:any;
  searchTerm: FormControl = new FormControl();
  searchResult=[];
  search_podcastName: string;
  search_podcastId: number;
  page: number;

  url_suffix:string;
  tab_index: string;
  currentUser:User;
  business_image_src: string;
  public_image_src: string;

  public Editor = ClassicEditor;
  public editor_model = {
      editorData: '<p>Hello, world!</p>'
  };

  editor: any;

  config: any = {
    toolbar: [
        'heading', 'bulletedList', 'numberedList', 'fontFamily', 'fontSize', 'undo', 'redo'
    ]
  };
  constructor(@Inject(WINDOW) private window: Window, private userService:UserService,private fb:FormBuilder,private profileService:ProfilesService,
    private location:Location,private router: Router,
    private HttpClient: HttpClient,public toastr: ToastsManager, vcr: ViewContainerRef, private lightService:LightService
    )
  {
    this.toastr.setRootViewContainerRef(vcr);
    this.profileForm=this.fb.group({
      pubProfileUrl:['',Validators.required],
      name:['',Validators.required],
      file:['',Validators.required]
  
    });
    this.businessForm=this.fb.group({
      businessName:['',Validators.required],
      bustagLines:['',Validators.required],
      podcastURL:['',Validators.required],
      file:['',Validators.required]
      
    });
    
    this.search_podcastName = '';
    this.search_podcastId = 0;
    this.business_image_src = this.public_image_src = '';
    this.selectedFiles = null;
    this.public_selectedFiles = null;
  }
  ngOnInit(): void {
    this.makeFormControl();
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.profileService.getBusinessProfile().subscribe((res:any)=>this.renderBusinessProfile(res));
    this.profileService.getPublicProfile().subscribe((res:any)=>this.renderPublicProfile(res)); 

    this.searchTerm.valueChanges
       .debounceTime(300)
		   .subscribe(data => {
   			this.lightService.search_word(data).subscribe(response =>{
          this.searchResult=response['docSearchResults'] ;
        },  err => {
          
          this.searchTerm.setValue(this.search_podcastName);
  
        })
     })
    
  }

  onSearchChange(searchValue : string ) {  
    this.url_suffix = searchValue;
  }
  renderBusinessProfile(res:any):void {
    if(res.code == "FAILED")
      return;
    this.businessForm.get("businessName").setValue(res.businessProfile.name);
    this.businessForm.get("bustagLines").setValue(res.businessProfile.tags);
    this.search_podcastName = res.businessProfile.podcasturl.split('/')[0];
    this.search_podcastId = res.businessProfile.podcasturl.split('/')[1];
    this.searchTerm.setValue(this.search_podcastName);
    this.business_image_src = res.businessProfile.banner;
    // this.businessForm.get("podcastURL").setValue(res.businessProfile.podcasturl);
    this.businessForm.get("bannerAds").setValue(res.businessProfile.bannerAds);

  }
  renderPublicProfile(res:any):void {
    if(res.code == "FAILED")
      return;
    this.profileForm.get("name").setValue(res.publicProfile.name);
    this.editor_model.editorData = res.publicProfile.name;
    this.url_suffix = res.publicProfile.url;
    this.public_image_src = res.publicProfile.banner;
    this.profileForm.get("pubProfileUrl").setValue(res.publicProfile.url);
  }

  makeFormControl(){

    this.businessForm.addControl('businessName',new FormControl());
    this.businessForm.addControl('file',new FormControl());
    this.businessForm.addControl('bustagLines',new FormControl());
    this.businessForm.addControl('podcastURL',new FormControl());
    this.businessForm.addControl('bannerAds',new FormControl());
    this.profileForm.addControl('name',new FormControl());
    this.profileForm.addControl('file',new FormControl());
    this.profileForm.addControl('pubProfileUrl',new FormControl());

  }
  back():void {
    this.location.back();
  }  
  selectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFiles = event.target.files;

      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
       this.business_image_src = event.target['result'];
      }
    }
  }
  public_selectFile(event){
    if (event.target.files && event.target.files[0]) {
      this.public_selectedFiles = event.target.files;
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
       this.public_image_src = event.target['result'];
      }
    }
  }
  uploadBusiness() {
    this.isSubmitting = true;
    this.errors = {errors: {}};

    if(this.selectedFiles != null)
      this.banner=this.selectedFiles.item(0);

    const businessPro=this.businessForm.value;
    this.businessProfile={
      banner:this.banner,
      bannerAds:businessPro['bannerAds'],
      name: businessPro['businessName'],
      tags: businessPro['bustagLines'],
      podcasturl: this.search_podcastName+"/"+this.search_podcastId.toString(),
      userId: 1
    }
    const formdata: FormData = new FormData();  
    formdata.append("banner",this.banner);  
    formdata.append('name',this.businessProfile.name);  
    formdata.append('podcasturl',this.businessProfile.podcasturl);  
    formdata.append('bannerAds',this.businessProfile.bannerAds);  
    formdata.append('tags',this.businessProfile.tags);   

    this.profileService.updateBusinessProfile(formdata ).subscribe( data => {  
      this.success(data['message']);  
      //this.router.navigateByUrl('/');  
    },  
    err => {  
      this.error(JSON.stringify(err)+" Update Profile Failed");  
      this.errors = err;  
      this.isSubmitting = false;  
    });    
  }

  uploadProfile() {  
    
    this.isSubmitting = true;
    this.errors = {errors: {}};
    this.public_banner = null;  
    if(this.public_selectedFiles != null)
      this.public_banner=this.public_selectedFiles.item(0);

    const publicPro=this.profileForm.value;
    const formdata: FormData = new FormData(); 

    if(this.public_banner != null) 
      formdata.append("banner", this.public_banner);  
    formdata.append('name', this.editor_model.editorData);  
    formdata.append('url', publicPro['pubProfileUrl']);  
    formdata.append('bannerAds', '');  
    formdata.append('tags', '');    

    this.profileService.updatePublicProfile(formdata).subscribe( data => {  
      this.success(data['message']);  
      //this.router.navigateByUrl('/');  
    },  
    err => {  
      this.error(JSON.stringify(err)+" Update Profile Failed");  
      this.errors = err;  
      this.isSubmitting = false;  
    });    
  }
  onEnterDown(event) {
    this.search_podcast();
  }
  search_podcast(){
    let keyword = this.searchTerm.value;
    const author='';
    const t='';
    const w= '';
    this.router.navigate(['/search'],{queryParams:{ q:keyword,a:author,t:t,w:w,p:this.page}});
    this.window.location.reload();
  }
  success(message: string) {
    this.toastr.success(message);
    this.tab_index = "tab1";
  }
  
  error(message: string) {
      this.toastr.error(message);
  }

  select_podcast(id: number, name: string){
    this.search_podcastName = name;
    this.search_podcastId = id;
  }
}
