import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinesspageComponent } from './businesspage/businesspage.component';

const routes: Routes = [
  {
    path:'',
    component:BusinesspageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinesspageRoutingModule { }
