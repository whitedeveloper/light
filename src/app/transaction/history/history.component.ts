import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { User, UserService, Errors, ProfilesService, Cart } from '../../core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import {Location} from '@angular/common';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { ToastsManager } from 'ng6-toastr';
import { Router } from '@angular/router';
import { CartService } from '../../core/services/cart.service';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  errors: Errors = {errors: {}};
  currentUser:User;
  customers:any[];
  price: Array<any> = [];
  total_amount: any;
  total_duration: any;
  podkast:Cart;
  purchaseHistory: any = [];
  constructor(private userService:UserService,private cartService:CartService, private profileService:ProfilesService, private location:Location,private router: Router,
    private HttpClient: HttpClient,public toastr: ToastsManager, vcr: ViewContainerRef
  )
  {
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.purchaseHistory['transcribes'] = [];
    this.loadPurchaseHistory();
    this.loadTransactionHistory();
  }

  loadTransactionHistory(){
    this.profileService.getTransactionHistory()
    .subscribe((res:any)=>this.renderHistory(res));

  }

  loadPurchaseHistory(){
    this.profileService.getPurchaseHistory().subscribe((res:any)=>{
      this.purchaseHistory = res;
    })
  }

  renderHistory(res:any){
    this.customers=res['customer'];
  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if(_seconds == null)
      return "00:00";
    if( typeof _seconds === 'string' && _seconds.indexOf(':') >= 0){
      if(_seconds.split(":").length == 2)
        return "00:"+_seconds;
      return _seconds;  
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
}
