import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TransactionRoutingModule } from './transaction-routing.module';
import { HistoryComponent } from './history/history.component';
import { SharedModule } from '../shared';
import { HttpClientModule } from '@angular/common/http';
import {NgxWigModule} from 'ngx-wig';
@NgModule({
  imports: [
    CommonModule,
    TransactionRoutingModule,
    MatTabsModule,
    SharedModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatRippleModule,
    MatIconModule,
    MatAutocompleteModule,
    MatProgressBarModule,
    MatCardModule,
    HttpClientModule,
    NgxWigModule,
    CKEditorModule,
  ],
  declarations: [HistoryComponent],
  exports:[
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatIconModule,
    MatProgressBarModule
  ]
})
export class TransactionModule { }
