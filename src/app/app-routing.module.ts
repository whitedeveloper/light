import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './core';

const routes: Routes = [
  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfileModule',
    canActivate: [AuthGuard]
  },
   {
    path: 'newrelease',
    loadChildren: './newrelease/newrelease.module#NewreleaseModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'listenlater',
    loadChildren: './listenlater/listenlater.module#ListenlaterModule',
    canActivate:[AuthGuard]
  },
  {
    path:'podcastdetails',
    loadChildren:'./podcastdetail/podcastdetail.module#PodcastdetailModule'
  },
  {
    path:'episodedetails',
    loadChildren:'./episodedetail/episodedetail.module#EpisodedetailModule'
  },
  {
    path:'search',
    loadChildren:'./search/search.module#SearchModule'
  },
  {
    path:'addcredit',
    loadChildren:'./payment/payment.module#PaymentModule',
    canActivate:[AuthGuard]
    
  },
  {
    path:'referral',
    loadChildren:'./referral/referral.module#ReferralModule',
    canActivate:[AuthGuard]
    
  },
  {
    path:'transaction',
    loadChildren:'./transaction/transaction.module#TransactionModule',
    canActivate: [AuthGuard]
  },
  {
    path:'businesspage',
    loadChildren:'./businesspage/businesspage.module#BusinesspageModule',
    canActivate:[AuthGuard]
  },
  {
    path:'subscription',
    loadChildren:'./subscription/subscription.module#SubscriptionModule',
    canActivate:[AuthGuard]
  },
  {
    path:'inprogress',
    loadChildren:'./inprogress/inprogress.module#InprogressModule',
    canActivate:[AuthGuard]

  },
  {
    path:'bookmark',
    loadChildren:'./bookmark/bookmark.module#BookmarkModule',
    canActivate:[AuthGuard]

  },
  {
    path:'checkout',
    loadChildren:'./checkout/checkout.module#CheckoutModule',
    canActivate:[AuthGuard]
  },
  {
    path:'collection',
    loadChildren:'./collection/collection.module#CollectionModule',
    canActivate:[AuthGuard]
  },
  {
    path:'collectiondetail',
    loadChildren:'./collectiondetail/collectiondetail.module#CollectiondetailModule',
    canActivate:[AuthGuard]
  },
  {
    path:'advertiser',
    loadChildren:'./aboutus/advertiser/advertiser.module#AdvertiserModule',
  },
  {
    path:'podcastenthusiast',
    loadChildren:'./aboutus/podcastenthusiast/podcastenthusiast.module#PodcastenthusiastModule',
  },
  {
    path:'podcaster',
    loadChildren:'./aboutus/podcaster/podcaster.module#PodcasterModule',
  },
  {
    path:'aboutus',
    loadChildren:'./aboutus/aboutus/aboutus.module#AboutusModule',
  },
  {
    path:'**',
    loadChildren:'./publicpage/publicpage.module#PublicpageModule',
    canActivate:[AuthGuard]
  },
  // {path: '**', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preload all modules; optionally we could
    // implement a custom preloading strategy for just some
    // of the modules (PRs welcome 😉)
    preloadingStrategy: PreloadAllModules,
    onSameUrlNavigation: 'reload',
    // useHash: false,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
