import {Component,Input, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { ToastsManager } from 'ng6-toastr';
import { StripeService } from '../core/services/stripe.service';

@Component({
  selector: 'common-modal', 
  templateUrl: './modal.component.html',
})
export class CommonModalComponent {
    @Input() src;
    @ViewChild('cardInfo') cardInfo: ElementRef;
    card: any;
    cardHandler = this.onChange.bind(this);
    error: string;
    checkoutForm:FormGroup;
    submitted = false;
    isLoading = false;

    constructor(private modalService: NgbModal, public activeModal: NgbActiveModal, private cd: ChangeDetectorRef, 
        private fb:FormBuilder, public toastr: ToastsManager, private stripeService: StripeService) {
        this.checkoutForm = this.fb.group(
            {   
                'amount':['', Validators.required],
                'other_amount':['', ],
            }
        );
        this.checkoutForm.addControl('amount',new FormControl());
        this.checkoutForm.addControl('other_amount',new FormControl());
    }

    ngAfterViewInit() {
        const style = {
          base: {
            iconColor: '#666EE8',
            color: '#31325F',
            lineHeight: '40px',
            fontWeight: 300,
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSize: '15px',
    
            '::placeholder': {
                color: '#CFD7E0',
            },
          },
        };
      
        this.card = elements.create('card', { style });
        this.card.mount(this.cardInfo.nativeElement);
        this.card.addEventListener('change', this.cardHandler);
    }

    onChange({ error }) {
        if (error) {
        this.error = error.message;
        } else {
        this.error = null;
        }
        this.cd.detectChanges();
    }

    ngOnDestroy() {
        this.card.removeEventListener('change', this.cardHandler);
        this.card.destroy();
    }

    async onSubmit() {
        this.submitted = true;

        if (this.checkoutForm.invalid) {
            return;
        }
        var credentials = this.checkoutForm.value;
        if(credentials.amount == 'other')
            credentials.amount = credentials.other_amount;

        if(credentials.amount < 5)
            this.toastr.error('Charge should be more than $5');

        const { token, error } = await stripe.createToken(this.card);
        if (error) {
            this.toastr.error(error.message);
        } 
        else {
            this.stripeService.payment_charge(credentials, token).subscribe(
                data => {
                    this.isLoading = false;
                    this.toastr.success('Success');
                    this.activeModal.dismiss('Cross click')
                },
                err =>{
                    
                }
            );
        }
    }
    get f() { return this.checkoutForm.controls; }
}