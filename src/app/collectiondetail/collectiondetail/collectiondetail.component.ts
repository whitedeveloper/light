import { Component, OnInit, ViewChild } from '@angular/core';
import { User, UserService } from '../../core';
import { ActivatedRoute, Router } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { PlayerService } from '../../core/services/player.service';
import {CollectionSearchService} from '../../core/services/collection-search.service'

@Component({
  selector: 'app-collectiondetail',
  templateUrl: './collectiondetail.component.html',
  styleUrls: ['./collectiondetail.component.css']
})
export class CollectiondetailComponent implements OnInit {
  currentUser:User
  isAuthenticated: boolean;
  keyword: string;
  highlights: any;
  collectionId: string;
  isPlayed: boolean = false;

  colors = ["clr-1", "clr-2", "clr-3", "clr-4", "clr-5","clr-6", "clr-7", "clr-8", "clr-9", "clr-10"];
  public query: string; // keyword of highlighting in highlighted text
  highlight_collections: Object; //array of highlighted text
  scroll_limit: any;

  constructor(private route:ActivatedRoute, private userService:UserService,private router:Router, private lightService:LightService,
              private playerService:PlayerService, private collectionSearchSerivce: CollectionSearchService) {
    route.queryParams
    .filter(params => params.collectionId)
    .subscribe(params=>{
      this.collectionId=params.collectionId;
    });
    this.lightService.getHighlightByCollectionId(this.collectionId)
    .subscribe((res:any)=>this.renderHighlights(res))
  }
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
        } 
        else {
          this.router.navigateByUrl('/register');
        }
      }
    ); 
    this.keyword = "";
    this.highlights = [];
    this.scroll_limit = 10; 
  }
  renderHighlights(res){
    for(var key in res) {
      this.highlights = res[key];
      this.highlight_collections = res[key];
    }
  }
  //====== Highlight text within highlighted texts
  search_word(event:any) {
    var keywordList = this.query.replace(/\s+/g, ' ').trim().split(" ");
    this.highlight_collections =  JSON.parse(JSON.stringify(this.highlights));
    var flag = 0;
    for( let index in keywordList ){
      if(keywordList[index]!="")
      {
        flag = 1;
      }
    }
    if(flag == 0)
      return false;
    for (let collectionIndex in this.highlight_collections)
    {
      var new_content = this.highlight_collections[collectionIndex]['highlightedText'];
      for( let index in keywordList ){
        new_content=new_content.replace(new RegExp(keywordList[index], "gi"), match=>{
          return '<span class="'+this.colors[Number(index)%10]+'">' + match + '</span>';
        });    
      }
      this.highlight_collections[collectionIndex]['highlightedText'] = new_content;
    }
  }
  onScroll(){
    this.scroll_limit += 5;
  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  playAudios(event, highlight){
    var episode = null;
    this.lightService.getEpisodesDetail(highlight.episodeId, highlight.podcastId)
    .subscribe((res:any)=>{
      episode = res;
      this.playerService.playAudio(episode.fileUrl, highlight.podcastName, episode.releaseDate, highlight.episodeTitle, highlight.image, episode, highlight.startTime/1000);
      this.playerService.isAudioChanged = true;
    })
  }
  onSearchCollection(event){
    this.collectionSearchSerivce.keyword = this.keyword;
    this.router.navigateByUrl('/collection');
  }
}
