import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollectiondetailRoutingModule } from './collectiondetail-routing.module';
import { CollectiondetailComponent } from './collectiondetail/collectiondetail.component';
import { SharedModule } from '../shared';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AudioPlayerModule } from '../audio-player/audio-player.module';

@NgModule({
  imports: [
    CommonModule,
    CollectiondetailRoutingModule,
    SharedModule,
    
    MatInputModule,
    MatButtonModule,
    
    
    InfiniteScrollModule,
    AudioPlayerModule,
  ],
  exports:[
    MatButtonModule,
    
    MatInputModule,
    
    
  ],
  declarations: [CollectiondetailComponent]
})
export class CollectiondetailModule { }
