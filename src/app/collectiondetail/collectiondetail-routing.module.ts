import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollectiondetailComponent } from './collectiondetail/collectiondetail.component';
import { AuthGuard } from '../core';

const routes: Routes = [
  {
    path:'',
    component: CollectiondetailComponent, canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollectiondetailRoutingModule { }
