import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PodcastenthusiastComponent } from './podcastenthusiast.component';
import { AdvertiserRoutingModule } from './podcastenthusiast-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AdvertiserRoutingModule,
  ],
  declarations: [PodcastenthusiastComponent]
})
export class PodcastenthusiastModule { }
