import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PodcastenthusiastComponent } from './podcastenthusiast.component';


const routes: Routes = [
  {
    path: '',
    component: PodcastenthusiastComponent,
   
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvertiserRoutingModule {}
