import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PodcasterComponent } from './podcaster.component';
import { AdvertiserRoutingModule } from './podcaster-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AdvertiserRoutingModule,
  ],
  declarations: [PodcasterComponent]
})
export class PodcasterModule { }
