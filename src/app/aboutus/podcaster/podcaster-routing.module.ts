import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PodcasterComponent } from './podcaster.component';


const routes: Routes = [
  {
    path: '',
    component: PodcasterComponent,
   
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvertiserRoutingModule {}
