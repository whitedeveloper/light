import { Component, OnInit, Input, Output, EventEmitter,AfterViewInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef, ViewContainerRef} from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { User, UserService, ApiService, Errors } from '../../core';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { ToastsManager } from 'ng6-toastr';
import { Address } from '../../core/models/address'
import { Jsonp } from '@angular/http';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StripeService } from '../../core/services/stripe.service';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.css']
})
export class StripeComponent implements AfterViewInit, OnDestroy,OnInit {

  ngForm:FormGroup;
  @ViewChild('cardInfo') cardInfo: ElementRef;
  currentUser:User
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  stripe_token: string = null;
  submitted = false;
  isLoading = false;

  constructor(private cd: ChangeDetectorRef,private http: HttpClient, private userService:UserService,private router:Router,
    public toastr: ToastsManager, vcr: ViewContainerRef, private fb:FormBuilder,private location:Location, 
    private modalService: NgbModal, private stripeService: StripeService) {
  
      this.toastr.setRootViewContainerRef(vcr); 
      this.ngForm = this.fb.group(
        {   'firstname':[''],
            'lastname':[''],
            'address':[''],
            'city':[''],
            'country':[''],
            'email':[''],
            'phone':[''],
            'amount':['', Validators.required],
            'other_amount':['', ],
            'zip_code': [''],
        }
      )
  }

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        this.makeFormControl();
        this.setFormValue();
      }
    );
  }

  ngAfterViewInit() {
    const style = {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '15px',

        '::placeholder': {
            color: '#CFD7E0',
        },
      },
    };
  
    this.card = elements.create('card', { style });
    this.card.mount(this.cardInfo.nativeElement);
    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    this.submitted = true;
    // stop here if form is invalid
    if (this.ngForm.invalid) {
        return;
    }
    var credentials = this.ngForm.value;
    if(credentials.amount == 'other')
      credentials.amount = credentials.other_amount;

    if(credentials.amount < 5)
      this.toastr.error('Charge should be more than $5');
    
    
    const { token, error } = await stripe.createToken(this.card);
    this.isLoading = true;
    if (error) {
      this.toastr.error(error.message);
      this.isLoading = false;
    } 
    else {
      this.stripe_token = token;
      this.stripeService.payment_charge(credentials, this.stripe_token).subscribe(
        data => {
          this.isLoading = false;
          this.toastr.success('Success');
          this.userService.setCurrentUser();
        },
        err =>{
         
        }
      );      
    }
  }
  makeFormControl(){
    this.ngForm.addControl('firstname',new FormControl());
    this.ngForm.addControl('email',new FormControl());
    this.ngForm.addControl('city',new FormControl());
    this.ngForm.addControl('country',new FormControl());
    this.ngForm.addControl('lastname',new FormControl());
    this.ngForm.addControl('address',new FormControl());
    this.ngForm.addControl('phone',new FormControl());
    this.ngForm.addControl('amount', new FormControl);
    this.ngForm.addControl('other_amount', new FormControl);
    this.ngForm.addControl('zip_code', new FormControl);
  }

  back():void {
    this.location.back();
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  setFormValue(){      
      this.ngForm.controls['firstname'].setValue(this.currentUser.firstName);
      this.ngForm.controls['lastname'].setValue(this.currentUser.lastName);
      this.ngForm.controls['email'].setValue(this.currentUser.email);
      if(this.currentUser.hasOwnProperty('address'))
      {
        this.ngForm.controls['phone'].setValue(this.currentUser['address'].phoneNo);
        this.ngForm.controls['address'].setValue(this.currentUser['address'].houseNo);
        this.ngForm.controls['city'].setValue(this.currentUser['address'].city);
        this.ngForm.controls['country'].setValue(this.currentUser['address'].country);
        this.ngForm.controls['amount'].setValue('');
        this.ngForm.controls['other_amount'].setValue('');
        this.ngForm.controls['zip_code'].setValue(this.currentUser['address'].postalCode);
      }
  }

  resetForm(){
    this.ngForm.controls['phone'].setValue('');
    this.ngForm.controls['firstname'].setValue('');
    this.ngForm.controls['lastname'].setValue('');
    this.ngForm.controls['email'].setValue('');
    this.ngForm.controls['address'].setValue('');
    this.ngForm.controls['city'].setValue('');
    this.ngForm.controls['country'].setValue('');
    this.ngForm.controls['amount'].setValue('');
    this.ngForm.controls['other_amount'].setValue('');
    this.ngForm.controls['zip_code'].setValue('');
  }
  get f() { return this.ngForm.controls; }
}
 