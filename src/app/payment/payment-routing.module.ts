import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StripeComponent } from './stripe/stripe.component';
import { AuthGuard } from '../core';

const routes: Routes = [
  {
    path:'',
    component:StripeComponent,canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
