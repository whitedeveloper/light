import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng6-toastr';
import { PlayerService } from '../../core/services/player.service';
import { SubscribedPodcast } from '../../core/models/subscribedPodcast';
import { UserService, User, Errors } from '../../core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { Listenlater } from '../../core/models/listenlater';

import { LightService } from '../../core/services/light.service';


@Component({
  selector: 'app-subscribe',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {

  currentUser:User;
  podcasts:SubscribedPodcast;
  errors: Errors = {errors: {}};
  sort_data = [{ key:"name", value:"Podcast Name" }, { key:"date", value:"Created Date" }]

  constructor(private userService:UserService,private router:Router,
    private lightService:LightService, public toastr: ToastsManager, vcr: ViewContainerRef, private playerService:PlayerService) {
      this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      });
    
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.getSubscribe();
        } 
        else {
          this.router.navigateByUrl('/register');
        }
      }
    ); 
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  isAuthenticated: boolean;
  success(message: string) { 
    this.toastr.success(message);
  }
  
  error(message: string) {
    this.toastr.error(message);
  }
  renderPodcast(res:any):void {
    this.podcasts=res;
    this.sort_by('name');
  }
  sort_by(key){
    if ( key == 'name' )
    {
      this.podcasts.subscribeDto.sort((n1, n2) => {
        return this.naturalCompare(n1.podcastName, n2.podcastName);
      });
    }
    else if ( key == 'date' )
    {
      this.podcasts.subscribeDto.sort((n1, n2) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }
  }
  naturalCompare(a, b) {
    var ax = [], bx = [];
 
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
 
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
 
    return ax.length - bx.length;
 }
  getSubscribe(){
    this.lightService.getSubscription()
    .subscribe((res:any)=>this.renderPodcast(res));
    
  }
}
