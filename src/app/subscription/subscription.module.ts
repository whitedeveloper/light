import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { SharedModule } from '../shared';
import { SubscriptionRoutingModule } from './subscription-routing.module';
import { SubscriptionComponent } from './subscription/subscription.component';
@NgModule({
  imports: [
    CommonModule,
    SubscriptionRoutingModule,
    AudioPlayerModule,
    SharedModule
  ],
  declarations: [SubscriptionComponent,]
})
export class SubscriptionModule { }
