import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PublicpageResolver } from './publicpage-resolver.service';
import { PublicpageComponent } from './publicpage.component';


const routes: Routes = [
  {
    path: '',
    component: PublicpageComponent,
   
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicpageRoutingModule {}
