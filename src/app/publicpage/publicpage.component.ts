import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';

import { User, UserService, Profile } from '../core';
import { FormBuilder } from '@angular/forms';
import { AlertService } from '../core/services/alert.service';
import { ToastsManager } from 'ng6-toastr';
import { LightService } from '../core/services/light.service';

@Component({
  selector: 'app-publicpage',
  templateUrl: './publicpage.component.html',
  styleUrls:[
    './publicpage.component.css'
  ]
})

export class PublicpageComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,private fb:FormBuilder,private alertService:AlertService,
    private userService: UserService,private router:Router,private location:Location,
    public toastr: ToastsManager, vcr: ViewContainerRef, private lightService:LightService
  ) 
  { 
    this.toastr.setRootViewContainerRef(vcr);
    
  }

  currentUser: User;
  bannerUrl: string;
  content: string;

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    let public_url = this.router.url.split('/')[1];
    this.lightService.getPublicByName(public_url)
    .subscribe((res:any)=>this.renderPublicPage(res));
  }

  renderPublicPage(res:any):void {
    if(res.code == 'FAILED')
      this.router.navigateByUrl('/');
    else{
      this.bannerUrl = res.publicProfile.banner;
      this.content = res.publicProfile.name;
    }  
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
}
