import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { PublicpageComponent } from './publicpage.component';

import { PublicpageResolver } from './publicpage-resolver.service';
import { SharedModule } from '../shared';
import { PublicpageRoutingModule } from './publicpage-routing.module';
import { MatFormFieldModule, MatButtonModule, MatInputModule, MatRippleModule,MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    SharedModule,
    PublicpageRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatRippleModule,
    MatSelectModule
  ],
  declarations: [
   
    PublicpageComponent
    
  ],
  exports:[
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule
  ],
  providers: [
    PublicpageResolver
  ]
})
export class PublicpageModule {}
