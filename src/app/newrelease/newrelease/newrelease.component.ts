import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { User, UserService } from '../../core';
import { Router } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlayerService } from '../../core/services/player.service';
import { CartService } from '../../core/services/cart.service';
import {CommonModalComponent} from '../../common/modal.component';
import { ToastsManager } from 'ng6-toastr';

@Component({
  selector: 'app-newrelease',
  templateUrl: './newrelease.component.html',
  styleUrls: ['./newrelease.component.css']
})
export class NewreleaseComponent implements OnInit {
currentUser:User
  isAuthenticated: boolean;

  newEpisodes: any = [];
  scroll_limit: any;

  modal_episode: any;
  modal_podcast: any;
  modal_index: number;
  isPlayed: boolean = false;
  selectedAll: any;
  selectValues: Array<{episodeId: string, podcastId:string, selected: boolean}> = [];
  sort_data = [{ key:"name", value:"Episode Name(A → Z)" }, { key:"name_big", value:"Episode Name(Z → A)" }, { key:"date", value:"Release Date(Old → New)" },
              { key:"date_big", value:"Release Date(New → Old)" }, { key:"duration", value:"Duration(Short → Long)" }, { key:"duration_big", value:"Duration(Long → Short)" }]

  constructor(private userService:UserService,private router:Router, private lightService:LightService, private modalService: NgbModal,
              private playerService:PlayerService, private cartService:CartService, public toastr: ToastsManager, vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.lightService.getNewRelease()
          .subscribe((res:any)=>this.renderEpisodes(res));
        } 
        else {
          this.router.navigateByUrl('/register');
        }
      }
    ); 
    this.scroll_limit = 10;
  }
  renderEpisodes(res){
    this.newEpisodes = res;
    for (let item of this.newEpisodes)
    {
      if(item.podcastTitle != null)
      {
        this.selectValues.push({ episodeId: item['episodeId'], podcastId: item['podcastId'], selected: false });
      } 
    } 
    this.sort_by('date_big');
  }
  onScroll(){
    this.scroll_limit += 5; 
  }
  toggleChild(){
    this.isPlayed = !this.isPlayed;
  }
  playAudios(event, episode, episode_detail = null){
    
    if(episode.audioFilePath == this.playerService.fileUrl && this.playerService.isPlaying == true)
    {
      this.playerService.api.pause();
      this.playerService.isPlaying = false;
    }
    else if(episode.audioFilePath == this.playerService.fileUrl && this.playerService.isPlaying == false)
    {
      this.playerService.api.play();
      this.playerService.isPlaying = true;
    }
    else{
      this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
      .subscribe((res:any)=>{
        this.playerService.playAudio(episode.audioFilePath, episode.podcastTitle, res.releaseDate, episode.episodeTitle, episode.imageUrl, episode, 0);
        this.isPlayed=true;
        this.playerService.isAudioChanged = true;
      })
    }
  }
  toTime(timeString){
    var timeTokens = timeString.split(':');
    return 3600*Number(timeTokens[0])+60*Number(timeTokens[1])+Number(timeTokens[2]);
  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  open_read_more_modal(content, episode, podkast, index) {
    this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
    .subscribe((res:any)=>{
      this.modal_episode = res;
    })
    this.modal_podcast = podkast;
    this.modal_index = index;
    this.modalService.open(content, { size: 'lg' });
  }
  apply_action(cartModal){
    for (var i = 0; i < this.selectValues.length; i++) {
      if(this.selectValues[i].selected == true)
      {
        this.transcribe(i);
      }
    }
    this.modalService.open(cartModal, { size: 'lg' });
  }
  apply_transcribe(index, cartModal){
    this.transcribe(index);
    this.modalService.open(cartModal, { size: 'lg' });
  }
  transcribe(index){
    var item = this.newEpisodes[index];
    this.addToCart(item.episodeId, item.podcastTitle, item.episodeTitle, this.secondsToMinute(item.duration), item.imageUrl, item.audioFilePath, item.podcastId);
  }
  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
    this.cartService.addEpisodes(this.currentUser.userid, id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
    .subscribe(data=>{
      this.success(data['message']);
      this.cartService.loadCartItems();
    });
  } 
  creditCheckout(link){
    if (this.cartService.checkOut() == false){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
      modalRef.componentInstance.src = link;
    }
    else{
      this.modalService.dismissAll();
    }
  }
  subscribedPodcasts(podcastId:string){
    // if(this.isAuthenticated){
    this.lightService.subscribedPodcasts(podcastId)
    .subscribe( data => {
      this.success(data['message']);
      this.success('Subscribed to Podcast Successfully');
      //this.router.navigateByUrl('/')
      },
    err => {
      this.error("Podcast subscribed failed")

      });
  }
  listenLater(podcastId:string,episodeId:string){
    // if(this.isAuthenticated){
    this.lightService.listenLater(podcastId,episodeId)
    .subscribe( data => {
      this.success(data['message']);
    },
    err => {
      this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
    });
  }
  success(message: string) { 
    this.toastr.success(message);
  }
  error(message: string) {
    this.toastr.error(message);
  }
  selectAll() {
    for (var i = 0; i < this.selectValues.length; i++) {
      this.selectValues[i].selected = this.selectedAll;
    }
  }
  checkIfAllSelected() {
    this.selectedAll = this.selectValues.every(function(item:any) {
        return item.selected == true;
      })
  }
  sort_by(key){
    if ( key == 'name' )
    {
      this.newEpisodes.sort((n1, n2) => {
        return this.naturalCompare(n1.episodeTitle, n2.episodeTitle);
      });
    }
    else if ( key == 'date' )
    {
      this.newEpisodes.sort((n1, n2) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }
    else if ( key == 'duration' )
    {
      this.newEpisodes.sort((n1, n2) => {
        return this.naturalCompare(this.secondsToMinute(n1.duration), this.secondsToMinute(n2.duration));
      });
    }
    else if ( key == 'name_big' )
    {
      this.newEpisodes.sort((n2, n1) => {
        return this.naturalCompare(n1.episodeTitle, n2.episodeTitle);
      });
    }
    else if ( key == 'date_big' )
    {
      this.newEpisodes.sort((n2, n1) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }
    else if ( key == 'duration_big' )
    {
      this.newEpisodes.sort((n2, n1) => {
        return this.naturalCompare(this.secondsToMinute(n1.duration), this.secondsToMinute(n2.duration));
      });
    }
  }
  naturalCompare(a, b) {
    var ax = [], bx = [];
 
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
 
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
 
    return ax.length - bx.length;
  }
}
