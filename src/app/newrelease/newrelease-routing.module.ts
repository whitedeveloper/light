import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewreleaseComponent } from './newrelease/newrelease.component';
import { AuthGuard } from '../core';

const routes: Routes = [
  {
    path:'',
    component: NewreleaseComponent, canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewreleaseRoutingModule { }
