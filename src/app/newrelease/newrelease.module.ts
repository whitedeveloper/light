import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewreleaseRoutingModule } from './newrelease-routing.module';
import { NewreleaseComponent } from './newrelease/newrelease.component';
import { SharedModule } from '../shared';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
@NgModule({
  imports: [
    CommonModule,
    NewreleaseRoutingModule,
    SharedModule,
    InfiniteScrollModule,
  ],
  declarations: [NewreleaseComponent]
})
export class NewreleaseModule { }
