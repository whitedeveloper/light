import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferralRoutingModule } from './referral-routing.module';
import { ReferralComponent } from './referral/referral.component';
import { SharedModule } from '../shared';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReferralRoutingModule,
    AudioPlayerModule,
    InfiniteScrollModule
  ],
  declarations: [ReferralComponent]
})
export class ReferralModule { }

