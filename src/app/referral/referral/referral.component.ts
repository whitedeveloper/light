import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { UserService, Errors, User } from '../../core';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { ToastsManager } from 'ng6-toastr';

@Component({
  selector: 'app-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.css']
})
export class ReferralComponent implements OnInit {
  isAuthenticated: boolean;
    currentUser:User;
 
    @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;
    constructor(private route:ActivatedRoute, 
      private lightService:LightService,
      private userService:UserService,public toastr: ToastsManager, vcr: ViewContainerRef,private router:Router){
        this.router.routeReuseStrategy.shouldReuseRoute = function(){
          return false;
        }
        this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit():void {
   
      this.userService.currentUser.subscribe(
        (userData) => {
          this.currentUser = userData;
        }
      );

      this.userService.isAuthenticated.subscribe(
        (authenticated) => {
          this.isAuthenticated = authenticated;
  
          // set the article list accordingly
          if (authenticated) {
          } else {
          }
        }
      ); 
  }
}
