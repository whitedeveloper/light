import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollectionRoutingModule } from './collection-routing.module';
import { CollectionComponent } from './collection/collection.component';
import { SharedModule } from '../shared';
import { MatButtonModule, MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    CollectionRoutingModule,
    SharedModule,
    
    MatInputModule,
    MatButtonModule,
    
    
  ],
  exports:[
    MatButtonModule,
    
    MatInputModule,
    
  ],
  declarations: [CollectionComponent]
})
export class CollectionModule { }
