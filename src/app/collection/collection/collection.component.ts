import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { User, UserService } from '../../core';
import { Router } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { PlayerService } from '../../core/services/player.service';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { ToastsManager } from 'ng6-toastr';
import {CollectionSearchService} from '../../core/services/collection-search.service'

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {
  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;
  currentUser:User
  isAuthenticated: boolean;
  keyword: string;
  collections: any;
  collection_names: Array<{collectionName: string, business_name: string, public_url: string}> = [];
  colors = ["clr-1", "clr-2", "clr-3", "clr-4", "clr-5","clr-6", "clr-7", "clr-8", "clr-9", "clr-10"];
  public query: string; // keyword of highlighting in highlighted text
  highlight_collections: Object; //array of highlighted text
  isPlayed: boolean = false;
  collections_mine: any = [];
  once_searched: boolean = false;

  constructor(private userService:UserService,private router:Router, private lightService:LightService, private playerService:PlayerService,
              public toastr: ToastsManager, private collectionSearchSerivce: CollectionSearchService) {

  }
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.keyword = this.collectionSearchSerivce.keyword;
          if(this.keyword != ''){

            this.lightService.getCollection()
            .subscribe((res:any)=>this.renderMineCollection(res));

            // this.onSearchCollection(null);
          }
        } 
        else {
          this.router.navigateByUrl('/register');
        }
      }
    ); 
    this.collections = [];
    this.collection_names = [];
  }

  onSearchCollection(event){
    this.once_searched = true;
    let request_form = {"keyword": this.keyword, "createdByMe": false};
    this.lightService.searchCollection(request_form).subscribe((res:any)=>this.renderCollections(res));
  }
  renderMineCollection(res:any):void{
    for(let item of res){
      if(item.createdBy == this.currentUser.userid){
        this.collections_mine.push(item);
      }
    }
  }
  renderCollections(res:any):void {
    this.collection_names = [];
    this.highlight_collections = res;
    this.collections = res;

    var index = 0;
    for(var key in res) {
      this.collection_names.push({collectionName  : res[key][0].collectionName, 
                                  business_name   : '', 
                                  public_url      : ''});

      this.lightService.getBusinessByUserId(res[key][0].userId).subscribe(
        (result:any)=>{
          if(result.code == "SUCCESS"){
            this.collection_names[index].business_name = result['businessProfile'].name
          }
          index++;
        }
      );
    }
  }
  //====== Highlight text within highlighted texts
  search_word(event:any) {
    var keywordList = this.query.replace(/\s+/g, ' ').trim().split(" ");
    this.highlight_collections =  JSON.parse(JSON.stringify(this.collections));
    var flag = 0;
    for( let index in keywordList ){
      if(keywordList[index]!="")
      {
        flag = 1;
      }
    }
    if(flag == 0)
      return false;
    for (let collectionIndex in this.highlight_collections)
    {
        for( let highlightIndex in this.highlight_collections[collectionIndex])
        {
          var new_content = this.highlight_collections[collectionIndex][highlightIndex]['highlightedText'];
          for( let index in keywordList ){
            new_content=new_content.replace(new RegExp(keywordList[index], "gi"), match=>{
              return '<span class="'+this.colors[Number(index)%10]+'">' + match + '</span>';
            });    
          }
          this.highlight_collections[collectionIndex][highlightIndex]['highlightedText'] = new_content;
        }
    }
  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  playAudios(event, highlight){
    var episode = null;
    this.lightService.getEpisodesDetail(highlight.episodeId, highlight.podcastId)
    .subscribe((res:any)=>{
      episode = res;
      this.playerService.playAudio(episode.fileUrl, highlight.podcastName, episode.releaseDate, highlight.episodeTitle, highlight.image, episode, highlight.startTime/1000);
      this.playerService.isAudioChanged = true;
    })
  }

  linkPublicPage(userId){
    this.lightService.getPublicByUserId(userId).subscribe(
      (result:any)=>{
        if(result.code == "SUCCESS"){
          this.router.navigateByUrl(result['publicProfile'].url);
        }
      }
    );
    
  }
  warn(message: string) {
    this.toastr.warning(message);
  }
}
