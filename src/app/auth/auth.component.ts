import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { Errors, UserService } from '../core';
import { AlertService } from '../core/services/alert.service';
import { ToastsManager } from 'ng6-toastr';
import { createCredentials } from 'crypto';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  authType: String = '';
  title: String = '';
  errors: Errors = {errors: {}};
  isSubmitting = false;
  submitted = false;
  authForm: FormGroup;
  loginForm: FormGroup;
  captchaResponse: string = '';
  referral: string = '';

  constructor(
    private route: ActivatedRoute, private router: Router, private userService: UserService, private cookieService: CookieService,
    private fb: FormBuilder, private alertService:AlertService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    // use FormBuilder to create a form group
    this.authForm = this.fb.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required],
      'username': ['', Validators.required],
      'firstname': ['', Validators.required],
      'lastname': ['', Validators.required],
      'reCaptcha': [''],
    });
    this.loginForm = this.fb.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required],
    });

    this.referral = this.cookieService.get('referral');
  }

  ngOnInit() {
    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    };
    this.route.url.subscribe(data => {
      // Get the last piece of the URL (it's either 'login' or 'register')
      this.authType = data[data.length - 1].path;
      // Set a title for the page accordingly
      this.title = (this.authType === 'login') ? 'Login' : 'Register';
      // add form control for username if this is the register page
      this.authForm.addControl('email', new FormControl());
      this.authForm.addControl('password', new FormControl());
      this.authForm.addControl('username', new FormControl());
      this.authForm.addControl('firstname', new FormControl());
      this.authForm.addControl('lastname', new FormControl());
      this.authForm.addControl('reCaptcha', new FormControl());

      this.loginForm.addControl('email', new FormControl());
      this.loginForm.addControl('password', new FormControl());

    });
  }
  resolved(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
  submitLoginForm(){
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.isSubmitting = true;
    const credentials = this.loginForm.value;
    this.userService
    .attemptAuth(this.authType, credentials)
    .subscribe(
      data => {
        this.success("Login Success");
        this.router.navigateByUrl('/');
    },
      err => {
        this.error('Login Failed');
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
  submitRegisterForm() {
    this.submitted = true;

    if (this.authForm.invalid) {
      return;
    }

    this.isSubmitting = true;
    this.errors = {errors: {}};
    const credentials = this.authForm.value;
    credentials.reCaptcha = this.captchaResponse;
    if(this.referral != '')
      credentials.referralCode = this.referral;
    this.userService.attemptAuth(this.authType, credentials).subscribe(
      data => {
        this.success("Registeration Success");
        this.router.navigateByUrl('/');
      },
      err => {
        this.error(err.error.message);
        this.errors = err;
        this.isSubmitting = false;
        
      }
    );
  }
  get f() { return this.authForm.controls; }
  get f_login() { return this.loginForm.controls; }

  success(message: string) { 
    this.toastr.success(message);
  }

  error(message: string) {
      this.toastr.error(message);
  }

  info(message: string) {
      this.alertService.info(message);
  }

  warn(message: string) {
      this.alertService.warn(message);
  }

  clear() {
      this.alertService.clear();
  }
  onScriptLoad() {
  }
 
  onScriptError() {
  }
  
}
