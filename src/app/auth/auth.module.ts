import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthComponent } from './auth.component';
import { NoAuthGuard } from './no-auth-guard.service';
import { SharedModule } from '../shared';
import { AuthRoutingModule } from './auth-routing.module';
import { RecaptchaModule } from 'ng-recaptcha';

@NgModule({
  imports: [
    SharedModule,
    AuthRoutingModule,
    RecaptchaModule,
  ],
  declarations: [
    AuthComponent
  ],
  providers: [
    NoAuthGuard,
  ]
})
export class AuthModule {}
