import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';

import { User, UserService, Profile } from '../core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AlertService } from '../core/services/alert.service';
import { ToastsManager } from 'ng6-toastr';
import { ProfilesService } from '../core/services/profiles.service';

export interface Country {
  short: string;
  publicName: string;
  states: Array<string>;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls:[
    './profile.component.css'
  ]
})

export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  paymentMethodFrom: FormGroup;
  currentStates : Array<string>;
  countries : Country[] = [
    {short: "US", publicName: "USA", states: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District Of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]},
    {short: "AU", publicName: "Australia", states: ["Australian Capital Territory", "New South Wales", "Northern Territory", "Queensland", "South Australia", "Tasmania", "Victoria", "Western Australia"]},
    {short: "BR", publicName: "Brazil", states: ["Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goiás", "Maranhão", "Mato Grasso", "Mato Grosso do Sul", "Minas Gerais", "Paraná", "Paraíba", "Pará", "Pernambuco", "Piauí", "Rio Grande do Norte", "Rio Grande do Sul", "Rio de Janeiro", "Rondônia", "Roraima", "Santa Catarina", "Sergipe", "São Paulo", "Tocantins"]},
    {short: "CA", publicName: "Canada", states: ["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland", "Northwest Territories", "Nova Scotia", "Nunavut", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", "Yukon"]},
    {short: "DE", publicName: "Germany", states: ["Baden-Württemberg", "Bayern", "Berlin", "Brandenburg", "Bremen", "Hamburg", "Hessen", "Mecklenburg-Vorpommern", "Niedersachsen", "Nordrhein-Westfalen", "Rheinland-Pfalz", "Saarland", "Sachsen", "Sachsen-Anhalt", "Schleswig-Holstein", "Thüringen"]},
    {short: "IE", publicName: "Ireland", states: ["Carlow", "Cavan", "Clare", "Cork", "Donegal", "Dublin", "Galway", "Kerry", "Kildare", "Kilkenny", "Laois", "Leitrim", "Limerick", "Longford", "Louth", "Mayo", "Meath", "Monaghan", "Offaly", "Roscommon", "Sligo", "Tipperary", "Waterford", "Westmeath", "Wexford", "Wicklow"]},
    {short: "MX", publicName: "Mexico", states: ["Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas", "Chihuahua", "Coahuila", "Colima", "Distrito Federal", "Durango", "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "México", "Nayarit", "Nuevo León", "Oaxaca", "Puebla", "Querétaro", "Quintana Roo", "San Luis Potosi", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"]},
    {short: "NL", publicName: "Netherlands", states: ["Drenthe", "Flevoland", "Friesland", "Gelderland", "Groningen", "Limburg", "Noord-Brabant", "Noord-Holland", "Overijssel", "Utrecht", "Zeeland", "Zuid-Holland"]},
    {short: "CN", publicName: "China", states: ['Beijing','Anhui','Chongqing','Fujian','Gansu','Guangdong','Guangxi','Guizhou','Hainan','Hebei','Heilongjiang','Henan','Hubei','Hunan','Jiangsu','Jiangxi','Jilin','Liaoning','Nei','Mongol','Ningxia','Qinghai','Shaanxi','Shandong','Shanghai','Shanxi','Sichuan','Tianjin','Xinjiang','Xizang','(Tibet)','Yunnan','Zhejiang']},
    {short: "MY", publicName: "Malaysia", states: ['Johor','Kedah','Kelantan','Kuala Lumpur','Labuan','Melaka','Negeri Sembilan','Pahang','Penang','Perak','Perlis','Sabah','Sarawak','Selangor','Terengganu']},
    {short: "IN", publicName: "India", states: ['Delhi','Maharashtra','TamilNadu','Karnataka','Haryana','Uttar Pradesh','Andhra Pradesh','Jammu and Kashmir','West Bengal','Gujarat','Madhya Pradesh','Kerala','Punjab','Bihar','Rajasthan','Orissa','Assam','NA','Himachal Pradesh','Chhattisgarh']},
    {short: "Z1", publicName: "Austria-SEAD", states: ['Australian Capital Territory','New South Wales','Northern Territory','Queensland','South Australia','Tasmania','Victoria','Western Australia']},
    {short: "YE", publicName: "Yemen", states: ['Amran','Dhale','Al Bayda','Al Hudaydah','Al Jawf','Al Mahwit','Amanat Al Asimah','Dhamar','Hajjah','Ibb','Ma rib','Rayma','Sa dah','Sanaa','Taiz','Aden','Abyan','Al Mahrah','Hadramaut','Socotra','Lahij','Shabwah']},
    {short: "YT", publicName: "Mayotte", states: ["Acoua", "Bandraboua", "Bandrele", "Boueni","Chiconi","Chirongui","Dembeni","Dzaoudzi","Kani-Keli","Koungou","Mamoudzou","Mtsamboro","M'Tsangamouji","Ouangani","Pamandzi","Sada","Tsingoni"]},
    {short: "ZA", publicName: "South Africa", states: []},
    {short: "ZM", publicName: "Zambia", states: []},
    {short: "ZW", publicName: "Zimbabwe", states: []},
    {short: "ZY", publicName: "Taiwan(CO Only)", states: []},
    {short: "ZZ", publicName: "Others", states: []},
    {short: "AM", publicName: "Armenia", states: []},
    {short: "GB", publicName: "United Kingdom", states: []},
    {short: "SI", publicName: "Slovenia", states: []},
    {short: "LS", publicName: "Lesotho", states: []},
    {short: "MC", publicName: "Monaco", states: []},
    {short: "CU", publicName: "Cuba", states: []},
    {short: "SL", publicName: "Sierra Leone", states: []},
    {short: "FO", publicName: "Faroe Islands", states: []},
    {short: "GQ", publicName: "Equatorial Guin", states: []},
    {short: "KY", publicName: "Cayman Islands", states: []},
    {short: "ME", publicName: "Montenegro", states: []},
    {short: "MN", publicName: "Mongolia", states: []},
    {short: "AF", publicName: "Afghanistan", states: ["Badakhshan", "Badghis", "Baghlan", "Balkh", "Bamyan", "Daykundi", "Farah", "Faryab", "Ghazni", "Ghor", "Helmand", "Herat", "Jowzjan", "Kabul", "Kandahar", "Kapisa", "Khost", "Kunar", "Kunduz", "Laghman", "Logar", "Wardak", "Nangarhar", "Nimruz", "Nuristan", "Paktia", "Paktika", "Panjshir", "Parwan", "Samangan", "Sar-e Pol", "Takhar", "Urozgan", "Zabul"]},
    {short: "AZ", publicName: "Azerbaijan", states: []},
    {short: "BS", publicName: "Bahamas", states: []},
    {short: "CK", publicName: "Cook Islands", states: []},
    {short: "EC", publicName: "Ecuador", states: []},
    {short: "NZ", publicName: "New Zealand", states: []},
    {short: "QA", publicName: "Qatar", states: []},
    {short: "SJ", publicName: "Svalbard", states: []},
    {short: "IR", publicName: "Iran", states: []},
    {short: "KI", publicName: "Kiribati", states: []},
    {short: "LC", publicName: "St. Lucia", states: []},
    {short: "MD", publicName: "Moldova", states: []},
    {short: "BI", publicName: "Burundi", states: []},
    {short: "BW", publicName: "Botswana", states: []},
    {short: "CH", publicName: "Switzerland", states: []},
    {short: "CY", publicName: "Cyprus", states: []},
    {short: "PE", publicName: "Peru", states: []},
    {short: "PS", publicName: "Palestine", states: []},
    {short: "SA", publicName: "Saudi Arabia", states: []},
    {short: "SO", publicName: "Somalia", states: []},
    {short: "GY", publicName: "Guyana", states: []},
    {short: "KR", publicName: "South Korea", states: []},
    {short: "AO", publicName: "Angola", states: []},
    {short: "BZ", publicName: "Belize", states: []},
    {short: "RU", publicName: "Russian Fed.", states: []},
    {short: "MR", publicName: "Mauretania", states: []},
    {short: "SH", publicName: "Saint Helena", states: []},
    {short: "FJ", publicName: "Fiji", states: []},
    {short: "FK", publicName: "Falkland Islnds", states: []},
    {short: "FM", publicName: "Micronesia", states: []},
    {short: "FR", publicName: "France", states: []},
    {short: "GA", publicName: "Gabon", states: []},
    {short: "GD", publicName: "Grenada", states: []},
    {short: "GE", publicName: "Georgia", states: []},
    {short: "GF", publicName: "French Guayana", states: []},
    {short: "GG", publicName: "GUERNSEY", states: []},
    {short: "GH", publicName: "Ghana", states: []},
    {short: "GI", publicName: "Gibraltar", states: []},
    {short: "GL", publicName: "Greenland", states: []},
    {short: "GM", publicName: "Gambia", states: []},
    {short: "GN", publicName: "Guinea", states: []},
    {short: "GP", publicName: "Guadeloupe", states: []},
    {short: "GR", publicName: "Greece", states: []},
    {short: "GS", publicName: "S. Sandwich Ins", states: []},
    {short: "GT", publicName: "Guatemala", states: []},
    {short: "GU", publicName: "Guam", states: []},
    {short: "GW", publicName: "Guinea-Bissau", states: []},
    {short: "HK", publicName: "Hong Kong", states: []},
    {short: "HM", publicName: "Heard/McDon.Isl", states: []},
    {short: "HN", publicName: "Honduras", states: []},
    {short: "HR", publicName: "Croatia", states: []},
    {short: "HT", publicName: "Haiti", states: []},
    {short: "HU", publicName: "Hungary", states: []},
    {short: "ID", publicName: "Indonesia", states: []},
    {short: "IL", publicName: "Israel", states: []},
    {short: "IO", publicName: "Brit.Ind.Oc.Ter", states: []},
    {short: "IQ", publicName: "Iraq", states: []},
    {short: "IS", publicName: "Iceland", states: []},
    {short: "IT", publicName: "Italy", states: []},
    {short: "JE", publicName: "JERSEY", states: []},
    {short: "JM", publicName: "Jamaica", states: []},
    {short: "JO", publicName: "Jordan", states: []},
    {short: "JP", publicName: "Japan", states: []},
    {short: "KE", publicName: "Kenya", states: []},
    {short: "KG", publicName: "Kyrgyzstan", states: []},
    {short: "KH", publicName: "Cambodia", states: []},
    {short: "KM", publicName: "Comoros", states: []},
    {short: "KN", publicName: "St Kitts&amp;Nevis", states: []},
    {short: "KP", publicName: "North Korea", states: []},
    {short: "KW", publicName: "Kuwait", states: []},
    {short: "KZ", publicName: "Kazakhstan", states: []},
    {short: "LA", publicName: "Laos", states: []},
    {short: "LB", publicName: "Lebanon", states: []},
    {short: "LI", publicName: "Liechtenstein", states: []},
    {short: "LK", publicName: "Sri Lanka", states: []},
    {short: "LR", publicName: "Liberia", states: []},
    {short: "LT", publicName: "Lithuania", states: []},
    {short: "LU", publicName: "Luxembourg", states: []},
    {short: "LV", publicName: "Latvia", states: []},
    {short: "LY", publicName: "Libya", states: []},
    {short: "M4", publicName: "MIAMI", states: []},
    {short: "MA", publicName: "Morocco", states: []},
    {short: "MF", publicName: "SAINT MARTIN", states: []},
    {short: "MG", publicName: "Madagascar", states: []},
    {short: "MH", publicName: "Marshall Islnds", states: []},
    {short: "MK", publicName: "Macedonia", states: []},
    {short: "ML", publicName: "Mali", states: []},
    {short: "MM", publicName: "Myanmar", states: []},
    {short: "MO", publicName: "Macau", states: []},
    {short: "MP", publicName: "N.Mariana Islnd", states: []},
    {short: "MQ", publicName: "Martinique", states: []},
    {short: "MS", publicName: "Montserrat", states: []},
    {short: "MT", publicName: "Malta", states: []},
    {short: "AD", publicName: "Andorra", states: []},
    {short: "AE", publicName: "Utd.Arab Emir.", states: []},
    {short: "AG", publicName: "Antigua/Barbuda", states: []},
    {short: "AI", publicName: "Anguilla", states: []},
    {short: "AL", publicName: "Albania", states: []},
    {short: "AN", publicName: "Dutch Antilles", states: []},
    {short: "AQ", publicName: "Antarctica", states: []},
    {short: "AR", publicName: "Argentina", states: []},
    {short: "AS", publicName: "Samoa, America", states: []},
    {short: "AT", publicName: "Austria", states: []},
    {short: "AW", publicName: "Aruba", states: []},
    {short: "AX", publicName: "Åland", states: []},
    {short: "BA", publicName: "Bosnia-Herz.", states: []},
    {short: "BB", publicName: "Barbados", states: []},
    {short: "BD", publicName: "Bangladesh", states: []},
    {short: "BE", publicName: "Belgium", states: []},
    {short: "BF", publicName: "Burkina Faso", states: []},
    {short: "BG", publicName: "Bulgaria", states: []},
    {short: "BH", publicName: "Bahrain", states: []},
    {short: "BJ", publicName: "Benin", states: []},
    {short: "BL", publicName: "Blue", states: []},
    {short: "BM", publicName: "Bermuda", states: []},
    {short: "BN", publicName: "Brunei Daruss.", states: []},
    {short: "BO", publicName: "Bolivia", states: []},
    {short: "BT", publicName: "Bhutan", states: []},
    {short: "BV", publicName: "Bouvet Islands", states: []},
    {short: "BY", publicName: "Belarus", states: []},
    {short: "C2", publicName: "Canary Island", states: []},
    {short: "C3", publicName: "CURACAO", states: []},
    {short: "CC", publicName: "Coconut Islands", states: []},
    {short: "CD", publicName: "Dem. Rep. Congo", states: []},
    {short: "CF", publicName: "CAR", states: []},
    {short: "CG", publicName: "Rep.of Congo", states: []},
    {short: "CI", publicName: "Cote d'Ivoire", states: []},
    {short: "CL", publicName: "Chile", states: []},
    {short: "CM", publicName: "Cameroon", states: []},
    {short: "CO", publicName: "Colombia", states: []},
    {short: "CR", publicName: "Costa Rica", states: []},
    {short: "CS", publicName: "Serbia/Monten.", states: []},
    {short: "CV", publicName: "Cape Verde", states: []},
    {short: "CX", publicName: "Christmas Islnd", states: []},
    {short: "CZ", publicName: "Czech Republic", states: []},
    {short: "DJ", publicName: "Djibouti", states: []},
    {short: "DK", publicName: "Denmark", states: []},
    {short: "DM", publicName: "Dominica", states: []},
    {short: "DO", publicName: "Dominican Rep.", states: []},
    {short: "DZ", publicName: "Algeria", states: []},
    {short: "EE", publicName: "Estonia", states: []},
    {short: "EG", publicName: "Egypt", states: []},
    {short: "EH", publicName: "West Sahara", states: []},
    {short: "ER", publicName: "Eritrea", states: []},
    {short: "ES", publicName: "Spain", states: []},
    {short: "ET", publicName: "Ethiopia", states: []},
    {short: "EU", publicName: "European Union", states: []},
    {short: "FI", publicName: "Finland", states: []},
    {short: "MU", publicName: "Mauritius", states: []},
    {short: "MV", publicName: "Maldives", states: []},
    {short: "MW", publicName: "Malawi", states: []},
    {short: "MZ", publicName: "Mozambique", states: []},
    {short: "NA", publicName: "Namibia", states: []},
    {short: "NC", publicName: "New Caledonia", states: []},
    {short: "NE", publicName: "Niger", states: []},
    {short: "NF", publicName: "Norfolk Islands", states: []},
    {short: "NG", publicName: "Nigeria", states: []},
    {short: "NI", publicName: "Nicaragua", states: []},
    {short: "NO", publicName: "Norway", states: []},
    {short: "NP", publicName: "Nepal", states: []},
    {short: "NR", publicName: "Nauru", states: []},
    {short: "NT", publicName: "NATO", states: []},
    {short: "NU", publicName: "Niue", states: []},
    {short: "OM", publicName: "Oman", states: []},
    {short: "OR", publicName: "Orange", states: []},
    {short: "PA", publicName: "Panama", states: []},
    {short: "PF", publicName: "Frenc.Polynesia", states: []},
    {short: "PG", publicName: "Pap. New Guinea", states: []},
    {short: "PH", publicName: "Philippines", states: []},
    {short: "PK", publicName: "Pakistan", states: []},
    {short: "PL", publicName: "Poland", states: []},
    {short: "PM", publicName: "St.Pier,Miquel.", states: []},
    {short: "PN", publicName: "Pitcairn Islnds", states: []},
    {short: "PR", publicName: "Puerto Rico", states: []},
    {short: "PT", publicName: "Portugal", states: []},
    {short: "PW", publicName: "Palau", states: []},
    {short: "PY", publicName: "Paraguay", states: []},
    {short: "RE", publicName: "Reunion", states: []},
    {short: "RO", publicName: "Romania", states: []},
    {short: "RS", publicName: "Serbia", states: []},
    {short: "RW", publicName: "Rwanda", states: []},
    {short: "S1", publicName: "SAIPAN", states: []},
    {short: "SB", publicName: "Solomon Islands", states: []},
    {short: "SC", publicName: "Seychelles", states: []},
    {short: "SD", publicName: "Sudan", states: []},
    {short: "SE", publicName: "Sweden", states: []},
    {short: "SG", publicName: "Singapore", states: []},
    {short: "SK", publicName: "Slovakia", states: []},
    {short: "SM", publicName: "San Marino", states: []},
    {short: "SN", publicName: "Senegal", states: []},
    {short: "SR", publicName: "Suriname", states: []},
    {short: "SS", publicName: "South Sudan", states: []},
    {short: "ST", publicName: "S.Tome,Principe", states: []},
    {short: "SV", publicName: "El Salvador", states: []},
    {short: "SY", publicName: "Syria", states: []},
    {short: "SZ", publicName: "Swaziland", states: []},
    {short: "T1", publicName: "TAHITI", states: []},
    {short: "TC", publicName: "Turksh Caicosin", states: []},
    {short: "TD", publicName: "Chad", states: []},
    {short: "TF", publicName: "French S.Territ", states: []},
    {short: "TG", publicName: "Togo", states: []},
    {short: "TH", publicName: "Thailand", states: []},
    {short: "TJ", publicName: "Tajikistan", states: []},
    {short: "TK", publicName: "Tokelau Islands", states: []},
    {short: "TL", publicName: "East Timor", states: []},
    {short: "TM", publicName: "Turkmenistan", states: []},
    {short: "TN", publicName: "Tunisia", states: []},
    {short: "TO", publicName: "Tonga", states: []},
    {short: "TP", publicName: "East Timor", states: []},
    {short: "TR", publicName: "Turkey", states: []},
    {short: "TT", publicName: "Trinidad,Tobago", states: []},
    {short: "TV", publicName: "Tuvalu", states: []},
    {short: "TW", publicName: "Taiwan", states: []},
    {short: "TZ", publicName: "Tanzania", states: []},
    {short: "UA", publicName: "Ukraine", states: []},
    {short: "UG", publicName: "Uganda", states: []},
    {short: "UM", publicName: "Minor Outl.Isl.", states: []},
    {short: "UN", publicName: "United Nations", states: []},
    {short: "UY", publicName: "Uruguay", states: []},
    {short: "UZ", publicName: "Uzbekistan", states: []},
    {short: "VA", publicName: "Vatican City", states: []},
    {short: "VC", publicName: "St. Vincent", states: []},
    {short: "VE", publicName: "Venezuela", states: []},
    {short: "VG", publicName: "Brit.Virgin Is.", states: []},
    {short: "VI", publicName: "Amer.Virgin Is.", states: []},
    {short: "VN", publicName: "Vietnam", states: []},
    {short: "VU", publicName: "Vanuatu", states: []},
    {short: "WF", publicName: "Wallis,Futuna", states: []},
    {short: "WS", publicName: "Samoa", states: []},
    {short: "XK", publicName: "Kosovo", states: []},

  ];
  current_country :string;
  currentUser: User;
  address: any;

  constructor(
    private route: ActivatedRoute,private fb:FormBuilder,private alertService:AlertService,
    private userService: UserService,private router:Router,private location:Location,
    public toastr: ToastsManager, vcr: ViewContainerRef, private profileService:ProfilesService
    // ,public dialog: MatDialog
  ) { 
    this.toastr.setRootViewContainerRef(vcr);

    this.profileForm=this.fb.group({
      userName:['',Validators.required],
      firstName:['',Validators.required],
      lastName:['',Validators.required],
      address:[''],
      city:[''],
      postalCode:[''],
      country:[''],
      email:[''],
      password:[''],
      state:[''],
      noreferral:['']
    });

    this.paymentMethodFrom=this.fb.group({
      creditcard:['',Validators.required],
      paypal:['',Validators.required],
      googlepay:['',Validators.required],
    });

    this.current_country = "US";
    this.currentStates = this.countries[0].states;
  }

  // custom functions WU
  countryChanged($event){
    // Access the object like city['CT'] .. That gives the Array
    for (let index = 0; index < this.countries.length; index++) {
      if(this.countries[index].short == $event.value){
        this.current_country = this.countries[index].short;
        this.profileForm.controls['country'].setValue(this.current_country);
        this.currentStates = this.countries[index].states;
        this.profileForm.controls['state'].setValue(this.currentStates[0]);
      }
    }
  }
  
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
       
        if(this.currentUser.hasOwnProperty('address'))
        {
          this.setFormValue();
          this.makeFormControl();
        }
      }
    );
  }
  setFormValue(){
    this.profileForm.get('userName').setValue(this.currentUser.username);
    this.profileForm.get('firstName').setValue(this.currentUser.firstName);
    this.profileForm.get('lastName').setValue(this.currentUser.lastName);
    this.profileForm.get('email').setValue(this.currentUser.email);
    this.profileForm.get('noreferral').setValue(this.currentUser.noOfRefferral);
    this.profileForm.get('address').setValue(this.currentUser['address'].houseNo);
    this.profileForm.get('city').setValue(this.currentUser['address'].city);
    this.profileForm.get('country').setValue(this.currentUser['address'].country);
    this.profileForm.get('password').setValue('');
    this.profileForm.get('state').setValue(this.currentUser['address'].state);
    this.profileForm.get('postalCode').setValue(this.currentUser['address'].postalCode);
    this.profileForm.get('userName').disable();
    this.profileForm.get('email').disable();
    this.profileForm.get('password').disable();
    this.profileForm.get('noreferral').disable();

    this.paymentMethodFrom.get('creditcard').setValue('');
    this.paymentMethodFrom.get('paypal').setValue('');
    this.paymentMethodFrom.get('googlepay').setValue('');
  }
  makeFormControl(){
    this.profileForm.addControl('userName', new FormControl());
    this.profileForm.addControl('firstName', new FormControl());
    this.profileForm.addControl('email', new FormControl());
    this.profileForm.addControl('city', new FormControl());
    this.profileForm.addControl('country', new FormControl());
    this.profileForm.addControl('postalCode', new FormControl());
    this.profileForm.addControl('lastName', new FormControl());
    this.profileForm.addControl('address', new FormControl());
    this.profileForm.addControl('password', new FormControl());

    this.paymentMethodFrom.addControl('creditcard', new FormControl());
    this.paymentMethodFrom.addControl('paypal', new FormControl());
    this.paymentMethodFrom.addControl('googlepay', new FormControl());
  }

  submitProfileForm() {
    this.profileService.updateProfile(this.profileForm.value ).subscribe( data => {  
      this.success(data['message']);  
      //this.router.navigateByUrl('/');  
    },  
    err => {  
      this.error(JSON.stringify(err)+" Update Profile Failed");  
    });    
    // this.success("Profile saved");
  }

  submitPaymentForm(){

  }
  
  success(message: string) { 
    this.toastr.success(message);
  }
  
  error(message: string) {
      this.toastr.error(message);
  }
  
  info(message: string) {
      this.alertService.info(message);
  }
  
  warn(message: string) {
      this.alertService.warn(message);
  }
  
  clear() {
      this.alertService.clear();
  }
  get f() { return this.profileForm.controls; }
}
