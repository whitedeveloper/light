import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { ProfileComponent } from './profile.component';

import { ProfileResolver } from './profile-resolver.service';
import { SharedModule } from '../shared';
import { ProfileRoutingModule } from './profile-routing.module';
import { MatFormFieldModule, MatButtonModule, MatInputModule, MatRippleModule,MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    SharedModule,
    ProfileRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatRippleModule,
    MatSelectModule
  ],
  declarations: [
   
    ProfileComponent
    
  ],
  exports:[
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule
  ],
  providers: [
    ProfileResolver
  ]
})
export class ProfileModule {}
