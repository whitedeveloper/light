import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search/search.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { SafeHtmlPipe } from '../core/services/safe-html';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SearchRoutingModule,
    AudioPlayerModule,
    NgbModule.forRoot( ),
    InfiniteScrollModule,

  ],
  declarations: [SearchComponent]
})
export class SearchModule { }
