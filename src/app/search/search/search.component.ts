import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, ViewChild, ViewContainerRef , Inject} from '@angular/core';
import { Podcastdto, DocSearchResults } from '../../core/models/podcastdto.interface';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UserService, ApiService, User, Errors } from '../../core';
import { LightService } from '../../core/services/light.service';
import { HttpParams } from '@angular/common/http';
import { PlayerService } from '../../core/services/player.service';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { ToastsManager } from 'ng6-toastr';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../core/services/cart.service';
import {CommonModalComponent} from '../../common/modal.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  errors: Errors = {errors: {}};
  isSubmitting = false;
  p:number;
  currentUser:User;
  podcasts:Podcastdto;
  episodes:Array<any> = [];
  itemsPerPage: number;
  totalItems: number;
  pages: number;
  previousPage: number;
  q:string;
  a:string;
  t:string;
  w:string;
  podcastId:string;
  view_more_state: Array<number> = [];
  //==========================
  isPlayed: boolean = false;
  isAuthenticated: boolean;
  modal_episode: any;
  modal_podcast: any;
  modal_index: number;
  scroll_limit: any;
  is_loading: boolean;
  total_result: number = 0;

  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;

  constructor(@Inject(WINDOW) private window: Window,  private router: Router, private apiService:ApiService, private route:ActivatedRoute, private lightService:LightService, 
    private userService:UserService,private playerService:PlayerService, public toastr: ToastsManager, vcr: ViewContainerRef, 
    private modalService: NgbModal, private cartService:CartService) 
  {

    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         // trick the Router into believing it's last link wasn't previously loaded
         this.router.navigated = false;
         // if you need to scroll back to top, here is the right place
         window.scrollTo(0, 0);
      }
    });

    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
        } else {
        }
      }
    ); 

    route.queryParams
     .subscribe(params=>{
      this.pages=params['page'];
      this.q=params['q'];
      this.a=params['a'];
      this.t=params['t'];
      this.w=params['w']; 
      this.podcastId=params['podcastId'];

    });
    this.is_loading = true;
    this.toastr.setRootViewContainerRef(vcr);
    this.isPlayed = false;
    this.scroll_limit = 10; 
  }

  ngOnInit() {
   
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    let params=new HttpParams().append('q',this.q)
    .append('a',this.a)
    .append('t',this.t)
    .append('w',this.w)
    .append('podcastId', this.podcastId);
    this.episodes = [];
    this.apiService.get('/getAll',params)
    .subscribe((res:any)=>this.renderPodcast(res));
  }
  renderPodcast(res:any):void {
    this.total_result = res.totalResultCount;
    this.is_loading = false;
    this.podcasts=res;
    this.view_more_state = [];
    for (let top of this.podcasts.docSearchResults)
    {
      for (let index in top.topPodcast.episodes)
      {
        this.view_more_state.push(3);
      }
    }
    var index = 0;
    for (let podkast of this.podcasts.docSearchResults) {
      if(podkast.topPodcast.episodes != null){
        for (let episode of podkast.topPodcast.episodes){
          index++;
        }
      }
    }   
    this.loadPage(0);
    this.totalItems=index;
    this.itemsPerPage=10;
  }
  
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.pages=page;
      var index = 0;
      this.episodes = [];
      for (let podkast of this.podcasts.docSearchResults) {
        if(podkast.topPodcast.episodes != null){
          for (let episode of podkast.topPodcast.episodes){
            index++;
            // if(index >= this.itemsPerPage*(this.pages-1) && index <= this.itemsPerPage*(this.pages)-1)
            {  
              var group:Array<any> = [];
              group.push(podkast);
              group.push(episode);
              this.episodes.push(group);
            }
          }
        }  
      }
    } 
    const element = document.querySelector('#search_result_content');
    element.scrollIntoView();
  }

  playAudios(event, episode, podkast, startTime = 0, isSegment = false){
    if (isSegment == false)
    {
      if(episode.fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == true)
      {
        this.playerService.api.pause();
        this.playerService.isPlaying = false;
      }
      else if(episode.fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == false)
      {
        this.playerService.api.play();
        this.playerService.isPlaying = true;
      }
      else{
        this.playerService.playAudio(episode.fileUrl, podkast.topPodcast.name, episode.releaseDate, episode.title, podkast.topPodcast.thumbnail, episode, startTime);
        this.playerService.isAudioChanged = true;
      }
    }
    else{
      this.playerService.playAudio(episode.fileUrl, podkast.topPodcast.name, episode.releaseDate, episode.title, podkast.topPodcast.thumbnail, episode, startTime);
      this.playerService.isAudioChanged = true;
      if(this.playerService.api != null)
        this.playerService.api.seekTime(startTime);
    }
  }
  success(message: string) { 
    this.toastr.success(message);
  }

  error(message: string) {
    this.toastr.error(message);
  }

  info(message: string) {
    this.toastr.info(message);
  }   

  listenLater(podcastId:string,episodeId:string){
    // if(this.isAuthenticated)
    {
      this.lightService.listenLater(podcastId,episodeId)
      .subscribe( data => {
        this.success(data['message']);
        // this.router.navigateByUrl('/');
      },
      err => {
        this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
        this.errors = err;
        this.isSubmitting = false;
      });
    }
    // else
    // {
    //   this.router.navigateByUrl('/login');
    // }
  }
  subscribedPodcasts(podcastId:string){
    // if(this.isAuthenticated)
    {
      this.lightService.subscribedPodcasts(podcastId)
      .subscribe( data => {
        // this.success(data['message']);
        this.success('Subscribed to Podcast Successfully');
        // this.router.navigateByUrl('/')
        },
      err => {
        this.error("Podcast subscribed failed")
        this.errors = err;
        this.isSubmitting = false;

        });
    }
  }
  open_read_more_modal(content, episode, podkast, index) {
    this.modal_episode = episode;
    this.modal_podcast = podkast;
    this.modal_index = index;
    this.modalService.open(content, { size: 'lg' });
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  loadData() {
    this.lightService.getTopPodcasts( this.pages ).subscribe((res:any)=>this.renderPodcast(res));
  } 
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if( _seconds == null)
      return '00:00:00';
    if(typeof _seconds == 'string')
    {
      if( _seconds.indexOf(':') >= 0){
        if(_seconds.split(":").length == 2)
          return "00:"+_seconds;
        return _seconds;  
      }
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  view_more(index){
    if(this.view_more_state[index] == 3)
      this.view_more_state[index] = 5;
    else
      this.view_more_state[index] = 3;  
  }
  public highlight(content) {
      if(!this.q) {
          return content;
      }
      return content.replace(new RegExp(this.q, "gi"), match => {
          return '<span class="clr-1">' + match + '</span>';
      });
  }
  apply_transcribe(index, cartModal){
    this.transcribe(index);
    this.modalService.open(cartModal, { size: 'lg' });
  }
  transcribe(index){
    var item = this.episodes[index][1];
    var podcast = this.episodes[index][0].topPodcast;
    this.addToCart(item.episodeId, podcast.name, item.title, this.secondsToMinute(item.duration), item.thumbnail, item.fileUrl, podcast.podcastId);
  }

  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
    this.cartService.addEpisodes(this.currentUser.userid, id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
    .subscribe(data=>{
      this.success(data['message']);
      this.cartService.loadCartItems();
    });
  }  

  strip_html_tags(str)
  {
    if ((str===null) || (str===''))
        return false;
    else
    {
      str = str.toString();
      return str.replace(/<[^>]*>/g, '');
    }
  }

  creditCheckout(link){
    if (this.cartService.checkOut() == false){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
      modalRef.componentInstance.src = link;
    }
    else{
      this.modalService.dismissAll();
    }
  }

  onScroll(){
    this.scroll_limit += 5;
  }

  goToPublicPage(userId){
    this.lightService.getPublicByUserId(userId).subscribe((res:any)=>{

      let url = this.window.location.href;
      url = url.substring(0, url.indexOf(this.router.url)) + '/' + res.publicProfile.url;
      
      this.window.open(url, '_blank');
      // this.router.navigate(['/'+res.publicProfile.url]);
    },
    err=>{

    });
  }
  remove_ptag(string){
    return string.replace(/<\/?[^>]+(>|$)/g, "");
  }
}  

