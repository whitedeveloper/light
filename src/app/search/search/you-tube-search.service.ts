import { Injectable, Inject } from '@angular/core';

import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import { SearchResult } from './search-result.model';

import { JwtService } from '../../core';

@Injectable()
export class YouTubeSearchService {
  constructor(
    private http: HttpClient, private jwtService:JwtService
  ) {
    
  }
  aurl='https://api.light.sx/ext-api/v1/podcast/searchPodcasts';

  static BASE_URL ="https://api.light.sx/ext-api/v1/podcast";

  
  
  query(
    URL: string,
    params?: Array<string>
  ): Observable<SearchResult[]> {
    let queryURL = `${this.aurl}${URL}`;
    if (params) {
      queryURL = `${queryURL}?${params.join('&')}`;
    }
    const token = this.jwtService.getToken();
    let headers=
new HttpHeaders();
headers.append('Content-Type', 'application/json');
headers.append('X-ACCESS-TOKEN', `Token ${token}`);
let optionsH = {
 headers:headers
 };
   
       return this.http.post(
        queryURL,null,optionsH
        )
          .map((res: any) => res
          
        );
    }
    /*  search(query: string): Observable<SearchResult[]> {
    return this.query(`/searchPodcasts`, [
      `query=${query}`,
      `P=1`
    
    ]); 
  } 
 */
   search(query: string): Observable<SearchResult[]> {
    const params: string = [
      `query=${query}`,
      `P=1`
    ].join('&');
    const token = this.jwtService.getToken();

    let headers=
    new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
     headers:headers
     };
    let queryUrl = `${this.aurl}?${params}`;
    return this.http.post(queryUrl,null,{headers:headers}).map(response => {
      return <any>response['docSearchResults'].map(item => {
        return new SearchResult({
          id: item.topPodcast.episodeId,
          title: item.topPodcast.name,
          description: item.topPodcast.description,
          thumbnailUrl: item.topPodcast.thumbnail
        });
      });
    });
  }  
}
export const LIGHT_PROVIDERS: Array<any> = [
  { provide: YouTubeSearchService, useClass: YouTubeSearchService }
];