import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { User } from '../../core/models/user.model';
import { Listenlater } from '../../core/models/listenlater';
import { Errors } from '../../core/models/errors.model';
import { UserService } from '../../core/services/user.service';
import { Router } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { ToastsManager } from 'ng6-toastr';
import { PlayerService } from '../../core/services/player.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../core/services/cart.service';
import { Cart } from '../../core/models/cart.model';
import { CartDto } from '../../core/models/CartDto.model';
import {CommonModalComponent} from '../../common/modal.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  
  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;

  currentUser:User;
  cartDtos:Cart[] = [];
  errors: Errors = {errors: {}};
  podkast:Cart;
  modal_episode:any;
  modal_podcast:any;
  price: Array<any> = [];
  total_amount: any;
  total_duration: any;
  loadingMessage=true;
  scroll_limit: number = 10;

  constructor(private userService:UserService,private router:Router,
    private lightService:LightService, public toastr: ToastsManager, vcr: ViewContainerRef, 
    private playerService:PlayerService, private modalService: NgbModal, public cartService:CartService) {
    this.toastr.setRootViewContainerRef(vcr);

  }

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.getCart();
        }
        else {
          this.router.navigateByUrl('/register');
        }
      }
    ); 
  }
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  isAuthenticated: boolean;
  success(message: string) { 
    this.toastr.success(message);
  }
  error(message: string) {
    this.toastr.error(message);
  }
  renderPodcast(res:any):void {
    this.cartDtos=res.cartDtos;
    this.cartService.cartItems = res.cartDtos;
  }
  getCart(){
    this.cartService.loadCartItems();
  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if(_seconds == null)
      return "00:00";
    if( typeof _seconds === 'string' && _seconds.indexOf(':') >= 0){
      if(_seconds.split(":").length == 2)
        return "00:"+_seconds;
      return _seconds;  
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  
  // transcript(mp3:string,episodeId:string){
  //   this.loadingMessage = true;
  
  //   this.lightService.turboTranscripts(mp3,episodeId).subscribe(
  //      data=>{
  //       this.success(data['status']);
  //       this.loadingMessage=false;
  //         if(data['status'] =='success'){
  //           this.processTranscript(data['srtFile'],episodeId);
  //         }
  //     },
  //     err => {
  //       this.error("Episode transcribe failed");
  //       this.loadingMessage=false;
  //       this.errors = err;
  //       console.log("subscribe error: "+JSON.stringify(err));
  //     }
  //   )  
  // }

  // checkBalance(duration:number):boolean{
  //   let bal=true;
  //     this.lightService.checkBalance(duration).subscribe(data=>{
  //       bal=data;
  //     })
  //     return bal;
    
  // }
  // processTranscript(srtFile:File,episodeId:string){
  //   this.lightService.indexEpisode(srtFile,episodeId).subscribe(data=>{
  //     this.success(data['message']);
  //     console.log("transcribe status "+data['message']);
  //   },
  //     err=> {
  //       this.error("Update index failed!!");
  //     }
    
  //   )
  // }
  // showPayment(){
  //   this.router.navigateByUrl("/addcredit");
  // }

  // checkOut(){
  //   if(this.total_amount<=this.currentUser.balance){
  //     this.checkBalance(this.total_amount);
  //     for(let cart of this.cartDtos){
  //       this.transcript(cart['mp3'],cart['episodeId']);
  //     }
  //   }
  //   else{
  //     this.showPayment();
  //   }
  // }

  onScroll(){
    this.scroll_limit += 5;
  }
  open_read_more_modal(content, episode, podkast) {
    this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
    .subscribe((res:any)=>{
      this.modal_episode = res;
    })
    // this.modal_episode = episode;
    this.modal_podcast = podkast;
    this.modalService.open(content, { size: 'lg' });
  }
  playAudios(event, fileUrl, episode){

    if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == true)
    {
      this.playerService.api.pause();
      this.playerService.isPlaying = false;
    }
    else if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == false)
    {
      this.playerService.api.play();
      this.playerService.isPlaying = true;
    }
    else{
      this.playerService.playAudio(fileUrl, this.modal_podcast.podcastName, episode.createDate, episode.episodeName, episode.thumbnail, episode);
      this.playerService.isAudioChanged = true;
    }
  }
  creditCheckout(){
    var rt = this.cartService.checkOut();
    if ( rt == false ){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
    }
    else{
      this.modalService.dismissAll();
    }
  }
  selectAll() {
    for (var i = 0; i < this.cartService.selectValues.length; i++) {
      this.cartService.selectValues[i].selected = this.cartService.selectedAll;
    }
  }
  checkIfAllSelected() {
    this.cartService.selectedAll = this.cartService.selectValues.every(function(item:any) {
        return item.selected == true;
    })
  }
}
