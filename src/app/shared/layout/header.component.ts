import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import * as $ from 'jquery';
import { User, UserService, ApiService, Errors, PodcastCategories } from '../../core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LightService } from '../../core/services/light.service';
import { SearchResult } from '../../search/search/search-result.model';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html',
  styleUrls:[ './header.component.css']
})
export class HeaderComponent implements OnInit {
  resultss: SearchResult[];
  loading: boolean;
  results: any[] = [];
  category:string;
  queryField: FormControl = new FormControl();
  podcastCategories:PodcastCategories[];
  hide:any;
  query:any;
  submit:any;
  errors: Errors = {errors: {}};
  isSubmitting = false;
  isLoading=false;
  searchTerm: FormControl = new FormControl();
  searchPodcast: FormControl = new FormControl();
  podcastId:string;
  podcasts:object;
  searchResult=[];
  searchResult_advanced=[];
  docSearchResults=[];
  searchForm:FormGroup;
  itemsPerPage: number;
  totalItems: number;
  page: number;
  previousPage: number;
  search_podcastName: string;
  advanced_open: boolean = false;
  advanced_podcast_id: string='';
  constructor(@Inject(WINDOW) private window: Window, 
    private userService: UserService, private lightService:LightService
    ,private service:ApiService,private route:ActivatedRoute,
    private router:Router, private fb:FormBuilder
  ) {
    route.queryParams
    .filter(params => params.page)
    .subscribe(params=>{
      this.page=params.page
    });

     // use FormBuilder to create a form group
     this.searchForm = this.fb.group({
      'keyword': '',
      'author': '',
      't': '',
      'w': '',
    });
    this.search_podcastName = '';
  }

  currentUser: User;

  ngOnInit() {
    this.setFormControl();
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        if(authenticated){
          this.lightService.getPodcastCategory().subscribe(
            ( res:any) => {
              this.renderCategory(res);
            }
          )
        }
      }
    ); 
    if(this.advanced_open == false)
    {
      this.searchTerm.valueChanges
      .debounceTime(300)
      .subscribe(data => {
        if(this.advanced_open == false)
        {
          this.isLoading = true;
          this.lightService.search_word(data).subscribe(response =>{
          this.isLoading = false;
          this.searchResult=response['docSearchResults'] ;
          },  
          err => {
          // this.searchTerm.setValue(this.search_podcastName);
          })
        }
      });
    }

    this.searchPodcast.valueChanges
       .debounceTime(300)
		   .subscribe(data => {
        this.isLoading = true;
   			this.lightService.search_word(data).subscribe(response =>{
          this.isLoading = false;
          this.searchResult_advanced=response['docSearchResults'] ;
          this.searchResult = [];
        },  err => {
          // this.searchTerm.setValue(this.search_podcastName);
        })  
    });
       
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
        $('#sidebarCollapse2').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });

    $('.search-btn').on("click", () => {
      $('.search').toggleClass("search-open");
      if( $('.search').hasClass('search-open')){
        // $('.cdk-overlay-container').appendTo('.search');
        $('.cdk-overlay-container #mat-autocomplete-2').addClass('d-none');
        this.advanced_open = true;
      }
      else{
        $('.cdk-overlay-container #mat-autocomplete-2').removeClass('d-none');
        this.advanced_open = false;
        this.searchResult = [];
        // $('.cdk-overlay-container').appendTo('body');
      }

    });

}
  updateResults(results: SearchResult[]): void {
    this.resultss = results;
  }
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  setFormControl(){
    this.searchForm.addControl('keyword',new FormControl());
    this.searchForm.addControl('author',new FormControl);
    this.searchForm.addControl('t',new FormControl());
    this.searchForm.addControl('w',new FormControl());
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = {errors: {}};
    const searchParsm = this.searchForm.value;
    const q = this.searchTerm.value;
    const author = searchParsm['author'];
    const t = searchParsm['t'];
    const w = searchParsm['w'];
    this.getPodcastId(this.searchPodcast.value);
    const podcastId = this.advanced_podcast_id;
    this.router.navigate(['/search'], {queryParams:{ q: q, a: author, t: t, w: w, p:'', podcastId:podcastId}});
  }
  renderCategory(res:any):void{
    this.podcastCategories=res
  }
  renderPodcast(res:any):void {
    this.podcasts=res;
  }
  onEnterDown(event) {
    this.search_podcast();
  }
  search_podcast(){
    let keyword = this.searchTerm.value;
    const author='';
    const t='';
    const w= '';
    this.router.navigate(['/search'],{queryParams:{ q:keyword, a:author, t:t, w:w, p:this.page, podcastId: ''}});
    // window.location.reload();
  }
  select_podcast(id: number, name: string){
    this.search_podcastName = name;
  }
  getPodcastId(podcastName: string){
    for(let item of this.searchResult_advanced)
    {
      if(item.topPodcast.name == podcastName)
      {
        this.advanced_podcast_id = item.topPodcast.podcastId;
        return;
      }
    }
    this.advanced_podcast_id = '';
  }
}
