import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PodcastdetailRoutingModule } from './podcastdetail-routing.module';
import { PodcastdetailComponent } from './podcastdetail/podcastdetail.component';
import { SharedModule } from '../shared';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PodcastdetailRoutingModule,
    AudioPlayerModule,
    InfiniteScrollModule
  ],
  declarations: [PodcastdetailComponent]
})
export class PodcastdetailModule { }

