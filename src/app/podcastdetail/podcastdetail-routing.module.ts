import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PodcastdetailComponent } from './podcastdetail/podcastdetail.component';

const routes: Routes = [
  {
    path:'',
    component:PodcastdetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PodcastdetailRoutingModule { }
