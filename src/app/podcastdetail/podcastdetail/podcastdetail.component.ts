import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, ViewChild, ViewContainerRef , Inject} from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { UserService, Errors, User } from '../../core';
import { PlayerService } from '../../core/services/player.service';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { ToastsManager } from 'ng6-toastr';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../core/services/cart.service';
import * as $ from 'jquery';
import {CommonModalComponent} from '../../common/modal.component';
import { CookieService } from 'ngx-cookie-service';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-podcastdetail',
  templateUrl: './podcastdetail.component.html',
  styleUrls: ['./podcastdetail.component.css']
})
export class PodcastdetailComponent implements OnInit {
  isAuthenticated: boolean;
  errors: Errors = {errors: {}};
  isSubmitting = false;
  podcastId:string;
  podcasts:object;
  currentUser:User;
  isPlayed: boolean = false;
  selectedAll: any;
  sort_data = [{ key:"name", value:"Episode Name(A → Z)" }, { key:"name_big", value:"Episode Name(Z → A)" }, { key:"date", value:"Release Date(Old → New)" },
              { key:"date_big", value:"Release Date(New → Old)" }, { key:"duration", value:"Duration(Short → Long)" }, { key:"duration_big", value:"Duration(Long → Short)" }]

  action_data = [ { key:"transcribe", value:"Transcribe" }, { key:"listen_later", value:"Listen Later" } ]
  selectValues: Array<{episodeId: string, selected: boolean}> = [];
  selectedAction: string;
  modal_episode: any;
  modal_podcast: any;
  modal_index: number;
  scroll_limit: any;
  is_read_more: boolean;
  referral: string;
  
  toggleChild(){
  this.isPlayed = !this.isPlayed;
   }
   @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;
    constructor(@Inject(WINDOW) private window: Window, private route:ActivatedRoute, 
      private lightService:LightService,
      private userService:UserService,public toastr: ToastsManager, vcr: ViewContainerRef,
      private router:Router, private playerService:PlayerService,  private modalService: NgbModal, private cartService:CartService, 
      private cookieService: CookieService, private metaService: Meta){
        this.router.routeReuseStrategy.shouldReuseRoute = function(){
          return false;
        }
  
        this.router.events.subscribe((evt) => {
          if (evt instanceof NavigationEnd) {
             // trick the Router into believing it's last link wasn't previously loaded
             this.router.navigated = false;
             // if you need to scroll back to top, here is the right place
             window.scrollTo(0, 0);
          }
        });

        this.toastr.setRootViewContainerRef(vcr);

          route.queryParams
          .filter(params => params.podcastId)
          .subscribe(params=>{
            this.podcastId=params.podcastId
            this.referral = params.referral;
          });
  }

  ngOnInit():void {

      this.metaService.addTag({ name: 'description', content: 'How to use Angular 4 meta service' });
      this.metaService.updateTag({ property: 'og:image', content: 'https://is1-ssl.mzstatic.com/image/thumb/Music124/v4/53/d2/13/53d2138f-51ad-b07d-357c-c3b5b25da296/source/600x600bb.jpg' });

      this.userService.currentUser.subscribe(
        (userData) => {
          this.currentUser = userData;
        }
      );

      this.lightService
      .getPodcastDetail(this.podcastId)
      .subscribe((res:any)=>this.renderPodcast(res));

      this.userService.isAuthenticated.subscribe(
        (authenticated) => {
          this.isAuthenticated = authenticated;
  
          // set the article list accordingly
          if (authenticated) {
          }  
          else {
            if(this.referral){
              var expiredDate = new Date(); 
              expiredDate.setDate( expiredDate.getDate() + 3 );
              this.cookieService.set('referral', this.referral);
            }
          }
        }
      ); 
      this.selectedAction = 'transcribe';
      this.scroll_limit = 10;
      this.is_read_more = false;
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  renderPodcast(res:any):void {
    this.podcasts=res;
    this.sort_by('date_big');
    //--initialize check box values---------------
    for (let item of this.podcasts['docSearchResults'][0].topPodcast.episodes)
    {
      this.selectValues.push({ episodeId: item.episodeId, selected: false });
    }  

    let instance = this;
    $(function() {
      // $('p.episode_description').each((i, item) => {
      //   var element = $(item)
      //                     .clone()
      //                     .addClass('episode_description')
      //                     // .css({display: 'inline', width: 'auto'})
      //                     .appendTo($(item).parent());    
      //     // debugger
      //     if( element.height() > $(item).height() ) {
      //         instance.is_read_more = true;
      //     }
      //     element.remove();
      // });
      if ($('.pod_des_hidden').height() < $('.pod_des').height()){
        instance.is_read_more = true;
      }
      $('.pod_des_hidden').addClass('d-none');
    });
  }
  playAudios(event, episode, podkast=null){
    if(episode.fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == true)
    {
      this.playerService.api.pause();
      this.playerService.isPlaying = false;
    }
    else if(episode.fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == false)
    {
      this.playerService.api.play();
      this.playerService.isPlaying = true;
    }
    else{
      this.playerService.playAudio(episode.fileUrl, podkast.topPodcast.name, episode.releaseDate, episode.title, podkast.topPodcast.thumbnail, episode);
      this.playerService.isAudioChanged = true;
    }
  }
  subscribedPodcasts(podcastId:string){
    if(this.isAuthenticated){
  this.lightService.subscribedPodcasts(podcastId)
  .subscribe( data => {
    // this.success(data['message']);
    this.success('Subscribed to Podcast Successfully');
    //this.router.navigateByUrl('/')
    },
  err => {
    this.error("Podcast subscribed failed")
    this.errors = err;
    this.isSubmitting = false;

    });
    }else{
      this.router.navigateByUrl('/login');

    }
  }
  listenLater(podcastId:string,episodeId:string){
    if(this.isAuthenticated){
    this.lightService.listenLater(podcastId,episodeId)
    .subscribe( data => {
      this.success(data['message']);
      //this.router.navigateByUrl('/');
    },
    err => {
      this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
      this.errors = err;
      this.isSubmitting = false;
    });
    }else{
      // this.router.navigateByUrl('/login'); 
    }
  }
  apply_action(cartModal){
    for (var i = 0; i < this.selectValues.length; i++) {
      if(this.selectValues[i].selected == true)
      {
        if(this.selectedAction == 'transcribe'){
          this.transcribe(i);
        }
        else{
          this.listenLater(this.podcasts['docSearchResults'][0].topPodcast.podcastId, this.selectValues[i].episodeId);
        }
      }
    }
    if(this.selectedAction == 'transcribe'){
      this.modalService.open(cartModal, { size: 'lg' });
    }
  }
  apply_transcribe(index, cartModal){
    this.transcribe(index);
    this.modalService.open(cartModal, { size: 'lg' });
  }
  transcribe(index){
    var episode = this.podcasts['docSearchResults'][0].topPodcast.episodes[index];
    var podcast = this.podcasts['docSearchResults'][0].topPodcast
    this.addToCart(episode.episodeId, podcast.name, episode.title, this.secondsToMinute(episode.duration), podcast.thumbnail, episode.fileUrl, podcast.podcastId);
  }
  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
    this.cartService.addEpisodes(this.currentUser.userid, id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
    .subscribe(data=>{
      this.success(data['message']);
      this.cartService.loadCartItems();
    });
  } 
  success(message: string) { 
    this.toastr.success(message);
  }

  error(message: string) {
    this.toastr.error(message);
  }

  info(message: string) {
    this.toastr.info(message);
  }

  warn(message: string) {
    this.toastr.warning(message);
  }

  sort_by(key){
    if ( key == 'name' )
    {
      this.podcasts['docSearchResults'][0].topPodcast.episodes.sort((n1, n2) => {
        return this.naturalCompare(n1.title, n2.title);
      });
    }
    else if ( key == 'date' )
    {
      this.podcasts['docSearchResults'][0].topPodcast.episodes.sort((n1, n2) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }else if ( key == 'duration' )
    {
      this.podcasts['docSearchResults'][0].topPodcast.episodes.sort((n1, n2) => {
        return this.naturalCompare(this.secondsToMinute(n1.duration), this.secondsToMinute(n2.duration));
      });
    }
    else if ( key == 'name_big' )
    {
      this.podcasts['docSearchResults'][0].topPodcast.episodes.sort((n2, n1) => {
        return this.naturalCompare(n1.title, n2.title);
      });
    }
    else if ( key == 'date_big' )
    {
      this.podcasts['docSearchResults'][0].topPodcast.episodes.sort((n2, n1) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }
    else if ( key == 'duration_big' )
    {
      this.podcasts['docSearchResults'][0].topPodcast.episodes.sort((n2, n1) => {
        return this.naturalCompare(this.secondsToMinute(n1.duration), this.secondsToMinute(n2.duration));
      });
    }
  }
  naturalCompare(a, b) {
    var ax = [], bx = [];

    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });

    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }

    return ax.length - bx.length;
  }  
  selectAll() {
    for (var i = 0; i < this.selectValues.length; i++) {
      this.selectValues[i].selected = this.selectedAll;
    }
  }
  checkIfAllSelected() {
    this.selectedAll = this.selectValues.every(function(item:any) {
        return item.selected == true;
      })
  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if( _seconds.indexOf(':') >= 0){
      if(_seconds.split(":").length == 2)
        return "00:"+_seconds;
      return _seconds;  
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  open_read_more_modal(content, episode, podkast, index) {
    this.modal_episode = episode;
    this.modal_podcast = podkast;
    this.modal_index = index;
    this.modalService.open(content, { size: 'lg' });
  }
  onScroll(){
    this.scroll_limit += 5;
  }
  podcast_read_more(){
    this.is_read_more = false;
  }
  creditCheckout(link){
    if (this.cartService.checkOut() == false){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
      modalRef.componentInstance.src = link;
    }
    else{
      this.modalService.dismissAll();
    }
  }
  sharelink(index = ''){
    if(this.isAuthenticated){
      let params = [`t=${index}`, `referral=${this.currentUser.referralCode}`];
      this.copyStringToClipboard(this.window.location.href + '&' + params.join('&'));
      this.success('Copied Shared Link');
    }
  }
  copyStringToClipboard (str) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(str).select();
    document.execCommand("copy");
    $temp.remove();
  }
  remove_ptag(string){
    return string.replace(/<\/?[^>]+(>|$)/g, "");
  }
}
