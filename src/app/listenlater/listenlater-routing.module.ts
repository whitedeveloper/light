import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListenlaterComponent } from './listenlater/listenlater.component';
import { AuthGuard } from '../core';

const routes: Routes = [
  {
    path:'',
    component:ListenlaterComponent, canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListenlaterRoutingModule { }
