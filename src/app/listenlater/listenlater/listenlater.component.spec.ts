import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListenlaterComponent } from './listenlater.component';

describe('ListenlaterComponent', () => {
  let component: ListenlaterComponent;
  let fixture: ComponentFixture<ListenlaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListenlaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListenlaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
