import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { UserService, User, Errors } from '../../core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { ToastsManager } from 'ng6-toastr';
import { Podcastdto } from '../../core/models/podcastdto.interface';
import { Listenlater } from '../../core/models/listenlater';
import { PlayerService } from '../../core/services/player.service';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../core/services/cart.service';
import {CommonModalComponent} from '../../common/modal.component';

@Component({
  selector: 'app-listenlater',
  templateUrl: './listenlater.component.html',
  styleUrls: ['./listenlater.component.css']
})
export class ListenlaterComponent implements OnInit {
  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;

  currentUser:User;
  podcasts: any = [];
  errors: Errors = {errors: {}};
  sort_data = [{ key:"name", value:"Episode Name(A → Z)" }, { key:"name_big", value:"Episode Name(Z → A)" }, { key:"date", value:"Release Date(Old → New)" },
              { key:"date_big", value:"Release Date(New → Old)" }, { key:"duration", value:"Duration(Short → Long)" }, { key:"duration_big", value:"Duration(Long → Short)" }]
  selectedAll: any;
  selectValues: Array<{episodeId: string, podcastId:string, selected: boolean}> = [];
  modal_episode: any;
  modal_podcast: any;
  modal_index: number;
  scroll_limit: any;

  constructor(private userService:UserService,private router:Router,
    private lightService:LightService, public toastr: ToastsManager, vcr: ViewContainerRef, private playerService:PlayerService, 
    private modalService: NgbModal, private cartService:CartService) {
      this.toastr.setRootViewContainerRef(vcr);

  }

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.getListenlater();
        } 
        else {
          this.error("Please Login");
          this.router.navigateByUrl('/register');
        }
      }
    ); 
    this.scroll_limit = 10;
  }
  isPlayed: boolean = false;
  toggleChild(){
  this.isPlayed = !this.isPlayed;
  }
  playAudios(event, fileUrl, episode){
    if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == true)
    {
      this.playerService.api.pause();
      this.playerService.isPlaying = false;
    }
    else if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == false)
    {
      this.playerService.api.play();
      this.playerService.isPlaying = true;
    }
    else{
      this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
      .subscribe((res:any)=>{
        this.playerService.playAudio(fileUrl, episode.podcastName, res.releaseDate, episode.episodeName, episode.img, episode);
        this.isPlayed=true;
        this.playerService.isAudioChanged = true;
      })
      
    }
  }
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  isAuthenticated: boolean;
  success(message: string) { 
    this.toastr.success(message);
  }
  error(message: string) {
    this.toastr.error(message);
  }
  renderPodcast(res:any):void {
    this.podcasts = [];
    //--initialize check box values---------------
    for (let item of res.bookMarks)
    {
      if(item.podcastName != null)
      {
        this.podcasts.push(item);
        this.selectValues.push({ episodeId: item['episodeId'], podcastId: item['podcastId'], selected: false });
      } 
    } 
    this.sort_by('date_big');
  }
  getListenlater(){
    this.lightService.getListenLater()
    .subscribe((res:any)=>this.renderPodcast(res));

  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if( _seconds.indexOf(':') >= 0){
      if(_seconds.split(":").length == 2)
        return "00:"+_seconds;
      return _seconds;  
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  sort_by(key){
    if ( key == 'name' )
    {
      this.podcasts.sort((n1, n2) => {
        return this.naturalCompare(n1.episodeName, n2.episodeName);
      });
    }
    else if ( key == 'date' )
    {
      this.podcasts.sort((n1, n2) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }
    else if ( key == 'duration' )
    {
      this.podcasts.sort((n1, n2) => {
        return this.naturalCompare(this.secondsToMinute(n1.playtime), this.secondsToMinute(n2.playtime));
      });
    }
    else if ( key == 'name_big' )
    {
      this.podcasts.sort((n2, n1) => {
        return this.naturalCompare(n1.episodeName, n2.episodeName);
      });
    }
    else if ( key == 'date_big' )
    {
      this.podcasts.sort((n2, n1) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }
    else if ( key == 'duration_big' )
    {
      this.podcasts.sort((n2, n1) => {
        return this.naturalCompare(this.secondsToMinute(n1.playtime), this.secondsToMinute(n2.playtime));
      });
    }
  }
  naturalCompare(a, b) {
    var ax = [], bx = [];
 
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
 
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
 
    return ax.length - bx.length;
  }
  selectAll() {
    for (var i = 0; i < this.selectValues.length; i++) {
      this.selectValues[i].selected = this.selectedAll;
    }
  }
  checkIfAllSelected() {
    this.selectedAll = this.selectValues.every(function(item:any) {
        return item.selected == true;
      })
  }
  subscribedPodcasts(podcastId:string){
    if(this.isAuthenticated){
    this.lightService.subscribedPodcasts(podcastId)
    .subscribe( data => {
      // this.success(data['message']);
      this.success('Subscribed to Podcast Successfully');
      //this.router.navigateByUrl('/')
      },
    err => {
      this.error("Podcast subscribed failed")
      this.errors = err;

      });
    }else{
      this.router.navigateByUrl('/login');

    }
  }
  listenLater(podcastId:string,episodeId:string){
    if(this.isAuthenticated){
    this.lightService.listenLater(podcastId,episodeId)
    .subscribe( data => {
      this.success(data['message']);
    },
    err => {
      this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
      this.errors = err;
    });
    }else{
      this.router.navigateByUrl('/login');
    }
  }
  apply_action(cartModal){
    for (var i = 0; i < this.selectValues.length; i++) {
      if(this.selectValues[i].selected == true)
      {
        this.transcribe(i);
      }
    }
    this.modalService.open(cartModal, { size: 'lg' });
  }
  apply_transcribe(index, cartModal){
    this.transcribe(index);
    this.modalService.open(cartModal, { size: 'lg' });
  }
  transcribe(index){
    var item = this.podcasts[index];
    this.addToCart(item.episodeId, item.podcastName, item.episodeName, this.secondsToMinute(item.playtime), item.img, item.fileUrl, item.podcastId);
  }

  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
    this.cartService.addEpisodes(this.currentUser.userid, id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
    .subscribe(data=>{
      this.success(data['message']);
      this.cartService.loadCartItems();
    });
  } 
  open_read_more_modal(content, episode, podkast, index) {
    this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
    .subscribe((res:any)=>{
      this.modal_episode = res;
    })
    // this.modal_episode = episode;
    this.modal_podcast = podkast;
    this.modal_index = index;
    this.modalService.open(content, { size: 'lg' });
  }
  onScroll(){
    this.scroll_limit += 5;
  }
  creditCheckout(link){
    if (this.cartService.checkOut() == false){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
      modalRef.componentInstance.src = link;
    }
    else{
      this.modalService.dismissAll();
    }
  }
}

