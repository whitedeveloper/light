import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListenlaterRoutingModule } from './listenlater-routing.module';
import { ListenlaterComponent } from './listenlater/listenlater.component';
import { SharedModule } from '../shared';
import { AudioPlayerComponent } from '../audio-player/audio-player/audio-player.component';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
@NgModule({
  imports: [
    CommonModule,
    ListenlaterRoutingModule,
    SharedModule,
    AudioPlayerModule,
    InfiniteScrollModule
  ],
  declarations: [ListenlaterComponent]
})
export class ListenlaterModule { }
