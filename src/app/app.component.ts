import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, Inject } from '@angular/core';
import * as $ from 'jquery';
import { UserService, ApiService, User } from './core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from './core/services/authentication.service';
import { LightService } from './core/services/light.service';
import { PlayerService } from './core/services/player.service';
import { HostListener } from "@angular/core";
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  results: any[] = [];
  queryField: FormControl = new FormControl();
 
  hide:any;
  query:any;
  submit:any;
  fileUrl:string;
  searchTerm: FormControl = new FormControl();
  private _opened: boolean = true;

  scrWidth:any;
  is_mobile_view: boolean = false;
  isAuthenticated: boolean = false;
  currentUser:User;

  @HostListener("window:scroll", [])
  onWindowScroll() {

  }

  constructor (
    private userService: UserService,private route:ActivatedRoute, private lightService:LightService
    ,private service:ApiService,
    private router:Router,private playerService:PlayerService, @Inject(DOCUMENT) private document: Document
  ) {
    this.scrWidth = window.innerWidth;
    if(this.scrWidth < 992)
        this.is_mobile_view = true;
    else
        this.is_mobile_view = false;   
  }

  ngOnInit() {
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;
        this.userService.currentUser.subscribe(
          (userData) => {
            this.currentUser = userData;
          }
        );
      }
    ); 

    window.addEventListener('scroll', this.scroll, true); //third parameter

    this.userService.populate();
  
    $(document).ready(function () {
      if ($(this.window).width() < 992){
        $('.wrapper').removeClass("slide-menu");
      }

      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });

      $('#sidebarCollapse2').on('click', function () {
        $('#sidebar').toggleClass('active');
      });

      // window.oncontextmenu = function(event) {
      //     event.preventDefault();
      //     event.stopPropagation();
      //     return false;
      // };
    });
  }
  scroll = (): void => {
    var header = this.document.getElementById("admin-header");
    var sticky = header.offsetTop;
  
    // Show To Top Button When Scrolled A Lot
    if (this.document.body.scrollTop > $('#admin-header').height()+2000) {
      $('#back-to-top').fadeIn('slow');
      header.classList.add("sticky");
    }
    else{
      $('#back-to-top').fadeOut('slow');
      header.classList.remove("sticky");
    }
    
    // Fix Side Bar In Mobile View
    if (this.document.body.scrollTop > $('#admin-header').height()) {
        header.classList.add("sticky");
        $('.main-wrapper').css('margin-top', $('#admin-header').height());
        if ($(window).width() < 992){
          $('.side-menu-fixed').addClass("fix_sidebar");
          $('.side-menu-fixed').css('top', $('#admin-header').height());
          $('.side-menu-fixed').css('bottom', $('.audioplayer').height());
        }  
    } else {
        header.classList.remove("sticky");  
        $('.main-wrapper').css('margin-top', 0);
        if ($(window).width() < 992){
          $('.side-menu-fixed').removeClass("fix_sidebar");
          $('.side-menu-fixed').css('top', 0);
          $('.side-menu-fixed').css('bottom', $('.audioplayer').height());
        } 
    }
    
  };
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  login(){
    this.router.navigateByUrl('/login');
  }
}
