import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EpisodedetailComponent } from './episodedetail/episodedetail.component';

const routes: Routes = [
  {
    path:'',
    component:EpisodedetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EpisodedetailRoutingModule { }
