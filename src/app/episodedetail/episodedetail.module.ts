import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EpisodedetailRoutingModule } from './episodedetail-routing.module';
import { EpisodedetailComponent } from './episodedetail/episodedetail.component';
import { SharedModule } from '../shared';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { TextSelectDirective } from "../core/services/text-select.directive";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { SafeHtmlPipe } from '../core/services/safe-html';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AudioPlayerModule,
    EpisodedetailRoutingModule,
    NgbModule.forRoot( ),
    NgxBootstrapSliderModule,
  ],
  declarations: [EpisodedetailComponent, TextSelectDirective, SafeHtmlPipe]
})
export class EpisodedetailModule { }
