import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, ViewContainerRef, ViewChild, Inject } from '@angular/core';
import { LightService } from '../../core/services/light.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { UserService, Errors, User } from '../../core';
import { PlayerService } from '../../core/services/player.service';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { KeyEventsPlugin } from '@angular/platform-browser/src/dom/events/key_events';
import { ToastsManager } from 'ng6-toastr';
import { TextSelectEvent } from "../../core/services/text-select.directive";
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../core/services/cart.service';
import {CommonModalComponent} from '../../common/modal.component';
import { DOCUMENT } from '@angular/platform-browser';
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie-service';

interface SelectionRectangle {
  left: number;
  top: number;
  width: number;
  height: number;
}
@Component({
  selector: 'app-episodedetail',
  templateUrl: './episodedetail.component.html',
  styleUrls: ['./episodedetail.component.css', './episodedetail.component.less']
})
export class EpisodedetailComponent implements OnInit {
  episodeId:string;
  episodes: any;
  highlight_episodes: any;
  segmentByTime: Array<string> = [];
  podcasts:Object;
  currentUser:User;
  podcastId:string;
  private content: string;
  public query: string;
  colors:Array<string>;
  isAuthenticated: boolean;
  isSubmitting = false;
  errors: Errors = {errors: {}};
  public hostRectangle: any;
  private selectedText: string;
  edited_transcript: string;
  selected_segment_index: number;
  selected_text_offset: number;
  action_selected_text: string;
  action_selected_text_offset: number;
  collection_type: boolean;
  collection_name: string;
  collections: any;
  selected_collection: string;
  max_highlight_count: number;
  modalRef: any;
  referral: string = '';
  section: number = 0; 

  //===== Business Profile ==================
  business_profile: any = [];
  public_profile: any = [];
  adsPodcast: any = [];
  is_open_createCollection: boolean = false;

  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;
  constructor(@Inject(WINDOW) private window: Window, private route:ActivatedRoute, private lightService:LightService,
  private location:Location,private userService:UserService,private router:Router, private playerService:PlayerService,
  public toastr: ToastsManager, vcr: ViewContainerRef, private modalService: NgbModal, private cartService:CartService,
  private cookieService: CookieService
  , @Inject(DOCUMENT) private document: Document) { 

    route.queryParams
    .filter(params => params.episodeId)
    .subscribe(params=>{
      this.episodeId = params.episodeId;
      this.podcastId = params.podcastId;
      this.referral = params.referral;
      this.section = params.t; 
    });

    this.toastr.setRootViewContainerRef(vcr);
    this.podcasts = {};
    this.colors = ["clr-1", "clr-2", "clr-3", "clr-4", "clr-5","clr-6", "clr-7", "clr-8", "clr-9", "clr-10"];
    this.selectedText = "";
    this.selected_text_offset = -1;
    this.selected_segment_index = -1;
    this.action_selected_text = "";
    this.action_selected_text_offset = -1;
    this.collection_type = false;
    this.collection_name = '';
    this.selected_collection = '';
    this.max_highlight_count = 0;
  }
  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {

        this.isAuthenticated = authenticated;
        this.lightService.getEpisodesDetail(this.episodeId, this.podcastId)
        .subscribe((res:any)=>this.renderEpisodes(res))
    
        // this.lightService
        // .getPodcastDetail(this.podcastId)
        // .subscribe((res:any)=>this.renderPodcast(res));
        
        // set the article list accordingly
        if (authenticated) {
          this.lightService
          .getCollection()
          .subscribe((res:any)=>this.renderCollection(res));
        }
        else {
          if(this.referral){
            var expiredDate = new Date(); 
            expiredDate.setDate( expiredDate.getDate() + 3 );
            this.cookieService.set('referral', this.referral);
          }
        }
      }
    ); 
    // Scroll Down To The Section
    setTimeout( () => { 
      if(this.section > 0){
        var elmnt = document.getElementById(`section_${this.section-1}`);  
        elmnt.scrollIntoView();
      }
    }, 6000);
  }

  search_word(event:any) {
    var keywordList = this.query.replace(/\s+/g, ' ').trim().split(" ");
    this.highlight_episodes =  JSON.parse(JSON.stringify(this.episodes));
    var flag = 0;
    for( let index in keywordList ){
      if(keywordList[index]!="")
      {
        flag = 1;
      }
    }
    if(flag == 0)
      return false;
    for (let segmentIndex in this.highlight_episodes['segmentsByTime'])
    {
        var new_content = this.highlight_episodes['segmentsByTime'][segmentIndex]['text'];
        for( let index in keywordList ){
          new_content=new_content.replace(new RegExp(keywordList[index], "gi"), match=>{
            return '<span class="'+this.colors[Number(index)%10]+'">' + match + '</span>';
          });    
        }
        this.highlight_episodes['segmentsByTime'][segmentIndex]['text'] = new_content;
    }
  }
  
  back():void {
    this.location.back();
  }
  renderEpisodes(res:any):void {
    console.log(res);
    this.episodes= res;
    this.highlight_episodes = res;
    
    this.segmentByTime = [];
    this.hostRectangle = [];
    for (let index in this.episodes['segmentsByTime'])
    {
      var hover_text = 'title="'+this.episodes['segmentsByTime'][index].otherUsersHighlightsCount+' others highlighted"';
      var text = this.episodes['segmentsByTime'][index].text.replace(new RegExp('<h5>', 'g'), '<h5 '+hover_text+'>');
      this.segmentByTime.push(text);
    } 

    if(this.episodes.hasOwnProperty("segmentsByTime"))
    {
      this.hostRectangle = new Array(this.episodes['segmentsByTime'].length);
    }
    if(this.episodes.hasOwnProperty("businessProfile"))
    {
      this.business_profile = this.episodes.businessProfile;
      let ads_podcast_url = this.business_profile.podcasturl.split("/")[1];
      
      this.lightService.getPublicByUserId(this.business_profile.userId).subscribe((res:any)=>{
        this.public_profile = res.publicProfile;  
      },
      err=>{

      });
      
      this.lightService.getPodcastDetail(ads_podcast_url).subscribe((res:any)=>{
          this.adsPodcast = res.docSearchResults[0].topPodcast;
      },
      err=>{

      });
    }
    this.slide_change();
    this.initialize_hostRectangle();

  }
  renderPodcast(res:any):void {
    this.podcasts=res;
    console.log(res);
  }
  renderCollection(res:any):void {
    if( res.length < 1)
      return;
    this.collections=res;
    this.sort_by('collection');
    this.selected_collection = this.collections[0]['collectionName'];
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  isPlayed: boolean = false;
  toggleChild(){
    this.isPlayed = !this.isPlayed;
  }
  toTime(timeString){
    if(timeString == null)
      return 0;
    var timeTokens = timeString.split(':');
    return 3600*Number(timeTokens[0])+60*Number(timeTokens[1])+Number(timeTokens[2]);
  }
  playAudios(event, fileUrl, episode=null, caption=null){
    // if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == true)
    // {
    //   this.playerService.api.pause();
    //   this.playerService.isPlaying = false;
    // }
    // else if(fileUrl == this.playerService.fileUrl && this.playerService.isPlaying == false)
    // {
    //   this.playerService.api.play();
    //   this.playerService.isPlaying = true;
    // }
    // else
    {
      this.playerService.isAudioChanged = true;
      this.playerService.playAudio(fileUrl, episode.podcastName, episode.releaseDate, episode.title, episode.thumbnail, episode, this.toTime(caption));
      if(this.playerService.api != null)
        this.playerService.api.seekTime(this.toTime(caption));
    }
  }
  subscribedPodcasts(podcastId:string){
    if(this.isAuthenticated){
      this.lightService.subscribedPodcasts(podcastId)
      .subscribe( data => {
        // this.success(data['message']);
        this.success('Subscribed to Podcast Successfully');
        },
      err => {
        this.error("Podcast subscribed failed")
        this.errors = err;
        this.isSubmitting = false;

        });
    }else{
      this.router.navigateByUrl('/login');

    }
  }
  listenLater(podcastId:string,episodeId:string){
    if(this.isAuthenticated){
    this.lightService.listenLater(podcastId,episodeId)
    .subscribe( data => {
      this.success(data['message']);
      // this.router.navigateByUrl('/');
    },
    err => {
      this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
      this.errors = err;
      this.isSubmitting = false;
    });
    }else{
      this.router.navigateByUrl('/login');
    }
  }
  success(message: string) { 
    this.toastr.success(message);
  }

  error(message: string) {
    this.toastr.error(message);
  }

  info(message: string) {
    this.toastr.info(message);
  }

  warn(message: string) {
    this.toastr.warning(message);
  }
  initialize_hostRectangle(){
    for(var i = 0; i < this.hostRectangle.length; i++) {
      this.hostRectangle[i] = null;
    }
  }
  public renderRectangles( event: TextSelectEvent, index: number ) : void {
    if(this.isAuthenticated){
      // console.group( "Text Select Event" );
      // console.log( "Text:", event.text );
      // console.log( "Start Offset", event.startOffset);
      // console.log( "Viewport Rectangle:", event.viewportRectangle );
      // console.log( "Host Rectangle:", event.hostRectangle );
      // console.groupEnd();

      // If a new selection has been created, the viewport and host rectangles will
      // exist. Or, if a selection is being removed, the rectangles will be null.
      if ( event.hostRectangle ) {

          this.hostRectangle[index] = event.hostRectangle;
          this.selectedText = event.text;
          this.selected_text_offset = event.startOffset;

      } else {

          this.initialize_hostRectangle();
          this.selectedText = "";
          this.selected_text_offset = -1;
      }
    }
  }
  // I share the selected text with friends :)
  public actionSelection(index: number, action: number, content: any) : void {
      if(this.isAuthenticated){
        // Create Collection
        if (action == 0){
          this.modalRef = this.modalService.open(content, { size: 'lg' });

          this.selected_segment_index = index;
          this.edited_transcript = this.selectedText;
          this.action_selected_text = this.selectedText;
          this.action_selected_text_offset = this.selected_text_offset;
          // console.log(this.selected_segment_index);
          // console.log(this.edited_transcript);
          // console.log(this.action_selected_text);
          // console.log(this.action_selected_text_offset);
        }
        // Highlight 
        else if (action == 1){
          this.selected_segment_index = index;
          this.edited_transcript = this.selectedText;
          this.action_selected_text = this.selectedText;
          this.action_selected_text_offset = this.selected_text_offset;
          // console.log(this.selected_segment_index);
          // console.log(this.edited_transcript);
          // console.log(this.action_selected_text);
          // console.log(this.action_selected_text_offset);

          this.add_highlight('highlight');
        }
        // Edit Transcript
        else if (action == 2){
          this.selected_segment_index = index;
          this.edited_transcript = this.selectedText;
          this.action_selected_text = this.selectedText;
          this.action_selected_text_offset = this.selected_text_offset;
          // console.log(this.selected_segment_index);
          // console.log(this.edited_transcript);
          // console.log(this.action_selected_text);
          // console.log(this.action_selected_text_offset);

          this.modalService.open(content, { size: 'sm' });
        }
        // Show create collection
        else if(action == 3){
          this.modalRef = this.modalService.open(content, { size: 'lg' });
          this.is_open_createCollection = true;
        }

        // Now that we've shared the text, let's clear the current selection.
        document.getSelection().removeAllRanges();
        // CAUTION: In modern browsers, the above call triggers a "selectionchange"
        // event, which implicitly calls our renderRectangles() callback. However,
        // in IE, the above call doesn't appear to trigger the "selectionchange"
        // event. As such, we need to remove the host rectangle explicitly.
        this.initialize_hostRectangle();
        this.selectedText = "";
      }
  }
  save_edit_transcript(){
    if(this.isAuthenticated){
      let request_form = {"podcastId": this.podcastId, "episodeId": this.episodeId, 
                          "editIndex": this.action_selected_text_offset, "oldWord": this.action_selected_text, 
                          "newWord": this.edited_transcript, "segmentKey": this.episodes['segmentsByTime'][this.selected_segment_index].caption};
      console.log(request_form);
      this.lightService.edit_transcript(request_form).subscribe( 
          data => 
          {   
            this.success(data['message']); 

            this.lightService.getEpisodesDetail(this.episodeId, this.podcastId)
            .subscribe((res:any)=>this.renderEpisodes(res));
          },
          err => {
            this.error(JSON.stringify(err)+" Edited Transcript Failed");
          }
      );
    }
    else
    {
      this.error("Please login");
    } 
    this.modalService.dismissAll();
  }
  create_collection(id){
    if(this.isAuthenticated){

      if(this.collection_name == ''){
        this.error("Collection Name Could Not Be Empty");
        return;
      }
      var scope = 'PUBLIC';
      if(this.collection_type == true)
        scope = 'PRIVATE';
      let request_form = {"collectionName": this.collection_name, "scope": scope};
      this.lightService.add_collection(request_form).subscribe( 
        data => 
        {   
          this.success(data['message']); 
          this.lightService.getCollection()
          .subscribe((res:any)=>this.renderCollection(res));

        },
        err => {
            this.error(JSON.stringify(err)+" Edited Transcript Failed");
         }
      );
    }
    else
    {
      this.error("Please login");
    } 
    this.modalRef.close();
  }
  add_highlight(action=''){
    if(this.isAuthenticated){
      let request_form = {};
      if(action == '') 
      {
        request_form = {"podcastId": this.podcastId, "editIndex": this.action_selected_text_offset, "episodeId": this.episodeId, 
                            "segmentKey": this.episodes['segmentsByTime'][this.selected_segment_index].caption, 
                            "highlightedText": this.action_selected_text, "collectionName": this.selected_collection};
      } 
      else if(action == 'highlight'){
        request_form = {"podcastId": this.podcastId, "editIndex": this.action_selected_text_offset, "episodeId": this.episodeId, 
                          "segmentKey": this.episodes['segmentsByTime'][this.selected_segment_index].caption, 
                          "highlightedText": this.action_selected_text};
      }
      // console.log(request_form);
      
      this.lightService.add_highlight(request_form).subscribe( 
        data => 
        {   
          this.success(data['message']); 
          this.lightService.getEpisodesDetail(this.episodeId, this.podcastId)
            .subscribe((res:any)=>this.renderEpisodes(res));

        },
        err => {
            this.error(JSON.stringify(err)+" Edited Transcript Failed");
        }
      );
    }
    else
    {
      this.error("Please login");
    } 
    this.modalService.dismissAll();
  }
  slide_change()
  {
    for (let segmentIndex in this.highlight_episodes['segmentsByTime'])
    {
      if(this.highlight_episodes['segmentsByTime'][segmentIndex].otherUsersHighlightsCount >= this.max_highlight_count){
        this.highlight_episodes['segmentsByTime'][segmentIndex].text = this.segmentByTime[segmentIndex];
      }
      else{
        var removed = this.highlight_episodes['segmentsByTime'][segmentIndex].text.replace(new RegExp('<(h5|/h5)[^>]{0,}>', 'g'), '');
        this.highlight_episodes['segmentsByTime'][segmentIndex].text = removed;
      }
    }
  }
  sort_by(key){
    if ( key == 'collection' )
    {
      this.collections.sort((n2, n1) => {
        return this.naturalCompare(n1.createdDate.toString(), n2.createdDate.toString());
      });
    }
  }

  naturalCompare(a, b) {
    var ax = [], bx = [];
 
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
 
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
 
    return ax.length - bx.length;
  }

  padTime(t) {
    return t < 10 ? "0"+t : t;
  }

  secondsToMinute(_seconds){
    if( _seconds == null)
      return '00:00:00';
    if(typeof _seconds == 'string')
    {
      if( _seconds.indexOf(':') >= 0){
        if(_seconds.split(":").length == 2)
          return "00:"+_seconds;
        return _seconds;  
      }
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }

  apply_transcribe(index, cartModal){
    this.transcribe(index);
    this.modalService.open(cartModal, { size: 'lg' });
  }

  transcribe(index){
    var item = this.episodes;
    // var podcast = this.podcasts['docSearchResults'][0].topPodcast;
    this.addToCart(item['episodeId'], item.podcastName, item['title'], this.secondsToMinute(item['duration']), item['thumbnail'], item['fileUrl'], item.podcastId);
  }

  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
    this.cartService.addEpisodes(this.currentUser.userid, id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
    .subscribe(data=>{
      this.success(data['message']);
      this.cartService.loadCartItems();
    });
  }  
  creditCheckout(link){
    if (this.cartService.checkOut() == false){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
      modalRef.componentInstance.src = link;
    }
    else{
      this.modalService.dismissAll();
    }
  }
  sharelink(index = ''){
    if(this.isAuthenticated){
      let params = [`t=${index}`, `referral=${this.currentUser.referralCode}`];
      this.copyStringToClipboard(this.window.location.href + '&' + params.join('&'));
      this.success('Copied Shared Link');
    }
  }
  copyStringToClipboard (str) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(str).select();
    document.execCommand("copy");
    $temp.remove();
  }
  modal_collection_close(modal){
    this.is_open_createCollection = false;
    modal.dismiss('Close click');
  }
}
