import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpisodedetailComponent } from './episodedetail.component';

describe('EpisodedetailComponent', () => {
  let component: EpisodedetailComponent;
  let fixture: ComponentFixture<EpisodedetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpisodedetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpisodedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
