﻿import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, distinctUntilChanged } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { User } from '..';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthenticationService {
    currentUser: User;
    public getLoggedInName = new Subject();

    private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUsers = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();
    constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private http: HttpClient,private router:Router) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
     }
     private loggedIn = new BehaviorSubject<boolean>(false); // {1}
     get isLoggedIn() {
        return this.loggedIn.asObservable(); // {2}
      }
    /* isLoggedIn() {
        if (this.currentUser == null) {
          return false;
        }
        return true;
      } */
  
    login(email: string, password: string) {
        return this.http.post<any>(`https://api.light.sx/ext-api/v1/auth/login`, { email:email, password: password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user.user && user.accessToken) {
                    // store uer details and jwt token in local storage to keep user logged in between page refreshes
                    this.localStorage.setItem('currentUser', JSON.stringify(user));
                    this.getLoggedInName.next(user.user.username);
                    this.currentUserSubject.next(user.user);
                    // Set isAuthenticated to true
                     this.isAuthenticatedSubject.next(true);
                     this.loggedIn.next(true);
                }
               
                
                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        this.loggedIn.next(false);
        this.isAuthenticatedSubject.next(false);
        this.localStorage.removeItem('currentUser');
       }
}