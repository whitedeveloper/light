import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JwtService } from './jwt.service';
import { throwError } from 'rxjs/';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ApiService {
  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  private formatErrors(error: any) {
    return  throwError(error.error);
  }

  get(path: string, params: HttpParams ): Observable<any> {
    let headers=
    new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', 'ng');
 
    let optionsH = {
      headers:headers
    };
    return this.http.get(`${environment.api_url}${path}`, { params })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    let headers=
    new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', 'ng');
  
 
     let optionsH = {
     headers:headers
     };
    return this.http.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body),optionsH
    ).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    const token = this.jwtService.getToken();
        let headers=
    new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
     headers:headers
     };
    return this.http.post(
      `${environment.api_url}${path}`,
      JSON.stringify(body),optionsH
    ).pipe(catchError(this.formatErrors));
  }

  delete(path): Observable<any> {
    let headers=
    new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', 'ng');
  
 
     let optionsH = {
     headers:headers
     };
    return this.http.delete(
      `${environment.api_url}${path}`,optionsH
    ).pipe(catchError(this.formatErrors));
  }
}
