import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Address } from '../../core/models/address'
import 'rxjs/add/operator/map'
import { User, UserService } from '../../core';

@Injectable()
export class StripeService {
    
    address: Address;
    currentUser:User;

    constructor(private http: HttpClient, private router: Router, private userService:UserService) {
        this.address=new Address();
        this.userService.currentUser.subscribe(
            (userData) => {
              this.currentUser = userData;
            }
        );
    }

    payment_charge(credentials, stripe_token): Observable<any>{
        let headers = new HttpHeaders();
        headers = headers.append('token', stripe_token.id);
        headers = headers.append('amount',credentials.amount);
        headers = headers.append('email', this.currentUser.email); 

        this.address.email = this.currentUser.email;
        this.address.city = credentials['city'];
        this.address.country = credentials['country'];
        this.address.housNo = credentials['address'];
        this.address.phoneNo = credentials['phone'];
        this.address.postalCode = credentials['zip_code'];
        let optionsH = {headers:headers}; 

        return this.http.post( 'https://api.light.sx/ext-api/v1/payment/charge', {address: this.address}, optionsH ).map((res: any) => res);
    }
}
