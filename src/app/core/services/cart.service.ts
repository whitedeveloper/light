import { Injectable, ViewContainerRef } from "@angular/core";
import { ToastsManager } from 'ng6-toastr';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { JwtService } from "..";
import { Cart } from "../models/cart.model";
import { EMPTY } from 'rxjs'
import { User } from '../../core/models/user.model';
import { UserService } from '../../core/services/user.service';
import { Router } from '@angular/router';
import { LightService } from '../../core/services/light.service';
import { Errors } from '../../core/models/errors.model';

@Injectable()
export class CartService {
    currentUser:User;
    public cartItems: Cart[] = [];
    public cartItem: Cart[] = [];
    public noOfItem: number = 0;
    public price: any = 0;
    public tprice:number=0;
    public totalDuration:number=0;
    public duration="00:00:00";
    public itemPrice:any=0;
    public durations:number=0;
    loadingMessage=true;
    errors: Errors = {errors: {}};
    array_price: Array<any> = [];

    selectedAll: any;
    selectValues: Array<{episodeId: string, podcastId:string, selected: boolean}> = [];

    constructor(private http: HttpClient,private jwtService:JwtService, public toastr: ToastsManager,
                private userService:UserService, private router:Router, private lightService:LightService){
        this.loadCartItems();
        this.selectedAll = false;
    }
      
    public addEpisodes(id:number,episodeId:string,podcastName:string,episodeName:string,durations:string,imgPath:string,mp3:string,podcastId:number) {
        this.cartItem = [];
        let item = this.cartItems.find(item => item.episodeId == episodeId);
        if (item != undefined) {
            this.error("Episode Already added to transcribe list");
            return EMPTY;
        } 
        else {
            this.duration=durations;
            this.durations=this.calculateTotalDuration(durations);
            this.itemPrice=this.calculatePrice(this.durations);
            this.totalDuration=this.durations;
            this.price=this.itemPrice;

            this.cartItem.push(
                new Cart(id,id,episodeId,episodeName,podcastName,
                this.itemPrice,status,this.noOfItem,this.price,
                this.totalDuration,this.durations,imgPath,mp3,podcastId));
        }
        this.cartSumTotal();
        const token = this.jwtService.getToken();
        let queryURL = "https://api.light.sx/ext-api/v1/cart/save";
        let headers = new HttpHeaders();
        headers= headers.append('X-ACCESS-TOKEN', `${token}`);
        return this.http.post(queryURL, this.cartItem, {headers:headers});

    }
    error(message: string) {
        this.toastr.error(message);
    }
    success(message: string) {
        this.toastr.success(message);
    }

    public updateCart() {
       
       this.cartSumTotal();
    }
    public removeEpisodes(id:number){
        this.delete_cart(id).subscribe(data=>{
          this.success(data['message']);
          this.loadCartItems();
        //   this.router.navigateByUrl('/checkout');
        })
    }
    public delete_cart(id: number) {
       /*  let index = this.cartItems.findIndex(item => item.episodeId == id);
        this.cartItems.splice(index); */
        const token =this.jwtService.getToken();
        let queryURL = "https://api.light.sx/ext-api/v1/cart/delete?cartId="+id;
        let headers = new HttpHeaders();
        headers= headers.append('X-ACCESS-TOKEN', `${token}`);
        //this.cartSumTotal();

        return this.http.delete(queryURL, {headers:headers}); 
        
    }
    public clear() {
        this.cartItems = [];
        this.noOfItem = 0;
        this.price = 0;
        this.totalDuration=0;
    }
    public cartSumTotal() {
        this.totalDuration=0;
        this.cartItems.forEach(p => {
            this.totalDuration= this.totalDuration + p.durations;
            this.noOfItem = this.noOfItem+p.noOfItem;
            this.price=this.price+p.price;
        })
    }
    public calculatePrice(duration):any{
        var __price = 0.0
        __price = duration*(2/60);
        return __price;
    }

    public calculateTotalDuration(duration:string):number{
        let tduration:number=0;
        if( duration.indexOf(':') >= 0){
            if(duration.split(":").length >= 2){
                var tempT:string[];
                var thour=0;
                var tmin=0;
                var tsec=0;
                tempT=  duration.split(":");
                if(Number(tempT[0])!=0){
                    thour=Math.floor(Number(tempT[0]));
                }if(Number(tempT[1])!=0){
                    tmin=Math.floor(Number(tempT[1]));
                }if(Number(tempT[2])!=0){
                    tsec=Math.floor(Number(tempT[2]));
                }
                
                var tsec=Math.floor(Number(tempT[2])/60);
                tduration=thour*60+tmin+tsec;
                var tdurations=Math.round(tduration);
            }
        }
        return tdurations;
    }

    loadCart(){
        const token =this.jwtService.getToken();
        let queryURL = "https://api.light.sx/ext-api/v1/cart/get";
        let headers = new HttpHeaders();
        headers= headers.append('X-ACCESS-TOKEN', `${token}`);
        return this.http.get(queryURL, {headers:headers}); 
    }   
    loadCartItems(){
        this.loadCart().subscribe((res:any)=>{
            this.cartItems = res.cartDtos;
            this.calc_all();
            this.userService.currentUser.subscribe(
                (userData) => {
                  this.currentUser = userData;
                }
            );
        });
    }
    calc_all(){
        this.array_price = [];
        this.selectValues = [];
        for (let cart of this.cartItems){
            this.selectValues.push({ episodeId: cart['episodeId'], podcastId: "", selected: false });  //temporary for checkbox
            
            var __price = cart['durations']*(2/60);
            this.array_price.push(__price.toFixed(4));
        }
        this.calc_total();
    }
    calc_total(){
        this.totalDuration = this.tprice = 0.0;
        for(let __price of this.array_price){
          this.tprice += parseFloat(__price);
        }
        for(let cart of this.cartItems){
          this.totalDuration += cart['durations'];
        }
    }
    checkBalance(duration:number){
          this.lightService.checkBalance(duration).subscribe(data=>{
            if(data == true)
            {
                for(let index in this.cartItems){
                    let cart = this.cartItems[index];
                    let select = this.selectValues[index];
                    this.transcript(cart['mp3'], cart['episodeId'], select.selected);
                }
                this.loadCartItems();
                this.userService.setCurrentUser();
            }
            
        })
    }
    checkOut(): boolean{
        if(this.tprice<=this.currentUser.balance){
            this.checkBalance(this.tprice);
            return true;
        }
        return false;
    }
    showPayment(){
        this.router.navigateByUrl("/addcredit");
    }
    transcript(mp3:string, episodeId:string, edited:boolean = false){
        this.loadingMessage = true;
      
        this.lightService.turboTranscripts(mp3, episodeId, edited).subscribe(
           data=>{
            this.success(data['status']);
            this.loadingMessage=false;
            if(data['status'] =='success'){
                this.success("Episode transcribe success");
                this.processTranscript(data['srtFile'],episodeId);
            }
            else
            {
                this.error("Episode transcribe failed");
                this.loadingMessage=false;
            }
          },
        )  
    }
    processTranscript(srtFile:File,episodeId:string){
        this.lightService.indexEpisode(srtFile,episodeId).subscribe(data=>{
            this.success(data['message']);
        },
        err=> {
            this.error("Update index failed!!");
        }
        
        )
    }
}