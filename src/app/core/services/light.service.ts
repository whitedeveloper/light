import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs'
import 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { map } from "rxjs/operators";
import { post } from 'selenium-webdriver/http';
import { Podcastdto } from '../models/podcastdto.interface';
import { JwtService } from '.';
@Injectable()
export class LightService {
  static BASE_URL = "https://api.light.sx/ext-api/v1/podcast";
  url:string;
  constructor(private http: HttpClient,private jwtService:JwtService) {
    this.url  = 'https://api.light.sx/ext-api/v1/podcast/searchPodcasts?p=1&query='
  }
  
  query( URL: string, params?: Array<string>): Observable<Podcastdto> {
    let queryURL = `${LightService.BASE_URL}${URL}`;
    if (params) {
      queryURL = `${queryURL}?${params.join('&')}`;
    }
    const token = this.jwtService.getToken();
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    // headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post( queryURL,null,optionsH ).map((res: any) => res);
  }
 
  search(keyword: string,author:string,t:string,w:string,p:number): Observable<Podcastdto> {
    return this.query(`/getAll`, [
      `q=${keyword}`,
      `a=${author}`,
       `t=${t}`,
       `w=${w}`,
       `p=${p}`
    
    ]);
  }

  /* searchAll(query: string): Observable<Podcastdto> {
    //return this.search(query);
  } */

  getTopPodcasts(p: number): Observable<Podcastdto> {
    return this.query(`/getTopPodcasts`,[
      `p=${p}`
    ]);
  }

  getPodcastDetail(podcastId: string): Observable<any> {
    return this.query(`/getPodcastDetail`,[
      `podcastId=${podcastId}`]);
  }

  getEpisodes(podcastId: string): Observable<any> {
    return this.query(`/getEpisodes`,[
     `podcastId= ${podcastId}`]);
  }
  getEpisodesDetail(episodeId:string, podcastId:string):Observable<any>{
    return this.query(`/getEpisodeDetail`,[
      `episodeId=${episodeId}`, `podcastId=${podcastId}` 
    ]);
  }
  subscribedPodcasts(podcastId:string):Observable<any>{
    return this.query(`/subscribePodcast`,[
      `podcastId=${podcastId}`])
  }

  listenLater(podcastId:string,episodeId:string):Observable<any>{
    return this.query(`/addListenLater`,[
      `podcastId=${podcastId}`,
       `episodetId=${episodeId}`])  
  }
  getListenLater():Observable<any>{
    return this.query(`/getListenLater`,[
      ])
      
  }
  getPodcastCategory():Observable<any>{
    return this.query(`/getCategories`,[
    ])
  }
  getSubscription():Observable<any>{
    return this.query(`/getSubscribePodcast`,[
      ])
      
  }
  turboTranscripts(mp3:string, episodeId:string, edited: boolean = false){
    const token = this.jwtService.getToken();
    let queryURL = '';

    if( edited == false)
      queryURL = "https://fetchold.light.sx/slow.php?mp3="+mp3+"&episodeId="+episodeId;
    else
      queryURL = "https://fetchold.light.sx/slow.php?mp3="+mp3+"&episodeId="+episodeId+"&method=edited";

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
   // headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post( queryURL, optionsH ).map((res: any) => res);
  }
  checkBalance(duration:number){
    const token=this.jwtService.getToken();
    let queryURL="https://api.light.sx/ext-api/v1/podcast/checkBalance?totalAmount="+duration
    let headers=new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.get<any[]>(queryURL,optionsH).map((res: any) => res);
  }
  indexEpisode(srtFile:File,episodeId:string){
    const token=this.jwtService.getToken();
    let queryURL="https://api.light.sx/ext-api/v1/podcast/index/episode?srtfile="+srtFile+"&episodeId="+episodeId
    let headers=new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post<any[]>(queryURL,null,optionsH).map((res: any) => res);
  }
  getInprogress():Observable<any>{
    return this.query(`/getInprogressEpisode`,[])
  }
  
  addInprogress(podcastId:number,episodetId:string,durationLeft:string,duration:string):Observable<any>{
    return this.query(`/addInprogress`,[`podcastId=${podcastId}`, `episodetId=${episodetId}`, 
                                        `durationLeft=${durationLeft}`, `duration=${duration}`])
  }


  search_word(term){
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', 'ng');
    let optionsH = { headers:headers };        
    return this.http.post<any[]>(this.url + term,null,{headers:headers}).map(res => {
      return res;
    })
  }

  getBookmark(params){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/bookmark";
    if (params) {
      queryURL = `${queryURL}?${params.join('&')}`;
    }
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }

  bookmark(form){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/bookmark";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post( queryURL, form, optionsH ).map((res: any) => res);
  }

  edit_transcript(form){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/transcript/edit";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post( queryURL, form, optionsH ).map((res: any) => res);
  }
  add_collection(form){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/collections";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post( queryURL, form, optionsH ).map((res: any) => res);
  }
  getCollection(){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/collections";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
  add_highlight(form){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/highlight-text";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post( queryURL, form, optionsH ).map((res: any) => res);
  }
  getPublicByName(url){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/profile/public/get?url=" + url;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
  searchCollection(form){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/collections/search";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.post( queryURL, form, optionsH ).map((res: any) => res);
  }
  getHighlightByCollectionId(collectionId:string):Observable<any>{
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/highlight-text?collectionId=" + collectionId;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
  getPublicByUserId(userId: string):Observable<any>{
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/profile/public/get?userId=" + userId;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
  getBusinessByUserId(userId: string):Observable<any>{
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/profile/business/get?userId=" + userId;
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
  getNewRelease():Observable<any>{
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/new_releases";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
}

export const LIGHT_PROVIDERS: Array<any> = [
  { provide: LightService, useClass: LightService }
];