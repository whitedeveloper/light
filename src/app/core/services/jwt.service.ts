import { WINDOW } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';


@Injectable()
export class JwtService {
 constructor(@Inject(WINDOW) private window: Window) {}


  getToken(): String {
    return this.window.localStorage['jwtToken'];
  }

  saveToken(token: String) {
    this.window.localStorage['jwtToken'] = token;
  }

  destroyToken() {
    this.window.localStorage.removeItem('jwtToken');
  }

}
