import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Profile } from '../models';
import { map } from 'rxjs/operators';
import { HttpParams, HttpHeaders, HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JwtService } from './jwt.service';
import { Jsonp } from '@angular/http';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';

@Injectable()
export class ProfilesService {
  constructor (
    private apiService: ApiService, private jwtService:JwtService,private http: HttpClient
  ) {}

  get(username: string): Observable<Profile> {
    return this.apiService.get('/profile/' + username,new HttpParams())
      .pipe(map((data: {profile: Profile}) => data.profile));
  }

  follow(username: string): Observable<Profile> {
    return this.apiService.post('/profile/' + username + '/follow');
  }


  getPublicProfile(){

    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/profile/public/get";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
  getBusinessProfile(){

    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/profile/business/get";
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('X-ACCESS-TOKEN', `Token ${token}`);
    let optionsH = {
      headers:headers
    };
    return this.http.get( queryURL, optionsH ).map((res: any) => res);
  }
 
  updateBusinessProfile(params){
    const token = this.jwtService.getToken();
    let queryURL='https://api.light.sx/ext-api/v1/profile/business/save';
    let headers = new HttpHeaders();
    headers= headers.append('X-ACCESS-TOKEN', `${token}`);
    return this.http.post(queryURL, params, {headers:headers});
  }

  updatePublicProfile(params){

    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/profile/public/save";
    let headers = new HttpHeaders();
    headers= headers.append('X-ACCESS-TOKEN', `${token}`);
    return this.http.post(queryURL, params, {headers:headers});
  }

  getTransactionHistory(){
    const token=this.jwtService.getToken();
    let queryURL="https://api.light.sx/ext-api/v1/profile/transaction";
    let headers = new HttpHeaders();
    headers= headers.append('X-ACCESS-TOKEN', `${token}`);
    return this.http.get(queryURL, {headers:headers});
  }
  getPurchaseHistory(){
    const token=this.jwtService.getToken();
    let queryURL="https://api.light.sx/ext-api/v1/transcribe/get";
    let headers = new HttpHeaders();
    headers= headers.append('X-ACCESS-TOKEN', `${token}`);
    return this.http.get(queryURL, {headers:headers});
  }
  updateProfile(params){
    const token = this.jwtService.getToken();
    let queryURL = "https://api.light.sx/ext-api/v1/profile/update";
    let headers = new HttpHeaders();
    headers= headers.append('X-ACCESS-TOKEN', `${token}`);
    return this.http.post(queryURL, params, {headers:headers});
  }
}
