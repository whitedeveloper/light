import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Song } from '../models/song';
import { VgAPI } from "videogular2/core";
import { LightService } from '../../core/services/light.service';
import { Inprogress } from '../../core/models/inprogress';
import { UserService } from '../../core/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private audio: any;
  public song: Subject<Song> = new Subject<Song>();
  public currentTime: Subject<string> = new Subject<string>();
  public fullTime: Subject<string> = new Subject<string>();
  // public fileUrl:Subject<string>=new Subject<string>();
  public fileUrl: string;
  api:VgAPI;
  current_fileUrl: string;
//----------Player Bar-------------------------------------------
  public imageUrl: String;
  public episodeUrl: String;
  public podcastUrl: String;
  public releaseDate: String;
  public episode: any;
  public seekTime: number;
  public isPlaying: boolean;
  public isAudioChanged: boolean;
  //public fileUrl=this.fileUrls.asObservable();
  inProgress: Array<any> = [];
  podcasts: Inprogress;
  selectValues: Array<{episodeId: string, podcastId: string, selected: boolean}> = [];
  selectedAll: any;
  selectedAction: string;

  public fileUrls:Subject<string>=new Subject<string>();

  constructor(private lightService:LightService, private userService:UserService) {
    this.audio = new Audio();
    this.seekTime = 0;
    this.isAudioChanged = false;
    this.isPlaying = false;
    this.imageUrl = this.episodeUrl = this.podcastUrl = this.releaseDate = null;
    this.episode = {title: ''};
    this.getInProgress(); 
   
  }

  playAudio(fileUrl:string, podcastName=null, releaseDate=null, episodeName=null, podcastImg=null, episode=null, seekTime=0){
    this.fileUrl = fileUrl;
    this.podcastUrl = podcastName;
    this.releaseDate = releaseDate;
    this.episodeUrl = episodeName;
    this.imageUrl = podcastImg;
    this.seekTime = seekTime;
    this.episode = episode;
    this.isPlaying = true;
    
    //--- Check inProgress--------------
    if(seekTime != 0)
    {
  
    }
    else{
      var __index = this.isInProgress(episode.episodeId);
      if( __index != -1){
        this.seekTime = this.inProgress[__index].timeLeft;
      }
    }  
  }
  getInProgress(){
    //------Get InProgress Episodes to Play From Saved Pos.
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        if(authenticated){
          this.lightService.getInprogress()
          .subscribe((res:any)=>this.renderPodcast(res));
        }
      }
    ); 
  }
  
  renderPodcast(res:any):void {
    this.podcasts=res;
    var count = 0;
    this.inProgress = [];
    this.podcasts.inprogressDto.forEach((item, index) => {
      this.inProgress.push(item);
      this.selectValues.push({ episodeId: item['episodeId'], podcastId: item['podcastId'], selected: false });    
    });
    this.sort_by('date_big');
  }

  naturalCompare(a, b) {
    var ax = [], bx = [];
 
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
 
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
 
    return ax.length - bx.length;
  }
  refreshSelect() {
    for (var i = 0; i < this.selectValues.length; i++) {
        this.selectValues[i].selected = false;
    }
  }
  sort_by(key){
    this.refreshSelect();
    if ( key == 'name' )
    {
      this.inProgress.sort((n1, n2) => {
        return this.naturalCompare(n1['episodeName'].toString(), n2['episodeName'].toString());
      });
    }
    else if ( key == 'date' )
    {
      this.inProgress.sort((n1, n2) => {
        return this.naturalCompare(n1.createdDate.toString(), n2.createdDate.toString());
      });
    }
    else if ( key == 'name_big' )
    {
      this.inProgress.sort((n2, n1) => {
        return this.naturalCompare(n1.episodeName, n2.episodeName);
      });
    }
    else if ( key == 'date_big' )
    {
      this.inProgress.sort((n2, n1) => {
        return this.naturalCompare(n1.createdDate.toString(), n2.createdDate.toString());
      });
    }
  }

  selectAll() {
    for (var i = 0; i < this.selectValues.length; i++) {
      this.selectValues[i].selected = this.selectedAll;
    }
  }
  
  checkIfAllSelected() {
    this.selectedAll = this.selectValues.every(function(item:any) {
        return item.selected == true;
      })
  }

  isInProgress(episodeId: string):number{
    for(var index = 0; index < this.inProgress.length; index++)
    {
      if(episodeId == this.inProgress[index].episodeId)
      {
        return index;
      }
    }
    return -1;
  }

  toggleAudio() {
    if (this.audio.paused) {
      this.audio.play();
    } else {
      this.audio.pause();
    }

    return this.audio.paused;
  }

}
