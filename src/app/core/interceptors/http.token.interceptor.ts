import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JwtService } from '../services';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
  constructor(private jwtService: JwtService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headersConfig = {
     
     
    };

    const token = this.jwtService.getToken();

    if (token) {
      headersConfig['X-ACCESS-TOKEN'] = `${token}`;
    }else{
      headersConfig['X-ACCESS-TOKEN'] = `ng`;

    }

    const request = req.clone({ setHeaders: headersConfig });
    return next.handle(request);
  }
}
