export interface User {
  email: string;
  username: string;
  password: string;
  firstName: string;
  lastName:string;
  balance:number;
  userid:number;
  referralCode: string;
  noOfRefferral: number;
}
