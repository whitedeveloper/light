export interface BusinessProfile {
    banner: File,
    name: string,
    tags: string,
    podcasturl: string,
    userId: number,
    bannerAds:string
}