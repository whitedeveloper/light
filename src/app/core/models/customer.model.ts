export class Customer {
    public id:number;
	  public cartId:number;
	  public  status:string;
	  public  createDate:Date;
	  public userId:number;
	  public amount:number;
}