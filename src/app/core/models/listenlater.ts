import { Time } from "@angular/common";

export interface Listenlater{
    bookMarks:BookMarks[];
}

export interface BookMarks{
    podcastName: string;
	episodeName: string;
	fileUrl: string;
    img:string;
    playtime:string;
    releaseDate:Time;
}