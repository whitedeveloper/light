import { Time } from "@angular/common";

export interface SubscribedPodcast{
    subscribeDto:SubscribeDto[];
    responseMessage:string;
}

export interface SubscribeDto{
    podcastName: string;
	img:string;
    releaseDate:Time;
    podcastId:string;
}