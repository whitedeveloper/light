export class Address {
    city:string;
    housNo:string;
    country:string;
    email:string;
    phoneNo:string;
    postalCode:string;
    constructor(init?:Partial<Address>) {
        Object.assign(this, init);
    }
   
}