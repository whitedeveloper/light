import { Time } from "@angular/common";

export interface Inprogress{
    inprogressDto: InprogressDto[];
}

export interface InprogressDto {
    podcastName: string;
	episodeName: string;
	fileUrl: string;
    img:string;
    playtime:string;
    createdDate:Time;
    timeLeft:string;

}