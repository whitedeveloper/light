export class Cart {
    public noOfItem: number;
    public price: number;
    public totalDuration:number;
    public podcastName:string;
	public episodeName:string;
	public durations:number;
	public imgPath:string;
	public episodeId:string;
	public itemPrice:number;
    public status:string;
    public userId:any;
    public id:any; 
    public mp3:string;
	public podcastId:number;
    constructor(id:any,userId:any,episodeId:string,episodeName:string,
        podcastName:string,itemPrice:number,status:string,
        noOfItem:number,price:number,totalDuration:number,durations:number,imgPath:string,mp3:string,podcastId:number) {
        this.noOfItem=noOfItem;
        this.price=price;
        this.totalDuration=totalDuration;
        this.podcastName=podcastName;
        this.episodeName=episodeName;
        this.durations=durations;
        this.imgPath=imgPath;
        this.episodeId=episodeId;
        this.itemPrice=itemPrice;
        this.status=status;
        this.userId=userId;
        this.id=id;
        this.mp3=mp3;
        this.podcastId=podcastId;
      } 
  
}