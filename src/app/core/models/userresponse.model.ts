import { User } from ".";

export interface Userresponse{
    user: User;
    accessToken: string;
}