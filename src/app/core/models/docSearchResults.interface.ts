
import { Deserializable } from "./Deserializable";
import { Time } from "@angular/common";
export interface DocSearchResults  {
   
      topPodcast:TopPocast;
      highlightText:HighlightText;

     
      
}
export interface TopPocast {
    podcastId:number;
	name:string;
     thumbnail:string;
	 description:string;
	 publishDate:Time;
	 authorName:string;
	 isSubscribed:boolean;
	 categories:string[];
	 episodes:Episodes[];
}

export interface Episodes {

    episodeId:string;
	 title:string;
	summary:string;
	 capacity:string;
	 authorName:string;
	 releaseDate:Time;
	 fileUrl:string;
	thumbnail:string;
	 categories:string[];
	duration:string;
	 isPlayed:number;
	 playedSecond:number;
	 delFlag:number;
	fileType:string;
	 podcastId:number;
}

export interface HighlightText {
    page:number;
    startTime:number;
    text:string;
}