import { Episodes } from "./podcastdto.interface";

export class CartItem {
  id:number;
  podcastName:string;
  episodeName:string;
  durations:string;
  imgPath:string;
    constructor(id:number,podcastName:string,episodeName:string,durations:string,imgPath:string){
      this.id=id;
      this.episodeName=episodeName;
      this.durations=durations;
      this.imgPath=imgPath;
      this.podcastName=podcastName;
    }
}