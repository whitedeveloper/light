export interface PublicProfile {
  banner: File,
  id: number,
  name: string,
  tags: string,
  url: string,
  userId: number,
  bannerAds:string

}