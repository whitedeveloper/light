import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeAuthResolver } from './home-auth-resolver.service';
import { SharedModule } from '../shared';
import { HomeRoutingModule } from './home-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  imports: [
    SharedModule,
    HomeRoutingModule,
    NgbModule.forRoot( ),
    MatAutocompleteModule,
    MatProgressSpinnerModule,
  ],
  declarations: [
    HomeComponent,
  ],
  exports:[ 
  ],
  providers: [
    HomeAuthResolver
  ]
})
export class HomeModule {}
