import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {  UserService, Errors, User } from '../core';
import { Podcastdto, DocSearchResults } from '../core/models/podcastdto.interface';
import { LightService } from '../core/services/light.service';
import { AuthenticationService } from '../core/services/authentication.service';
import { PlayerService } from '../core/services/player.service';
import { AudioPlayerComponent } from '../audio-player/audio-player/audio-player.component';
import { AlertService } from '../core/services/alert.service';
import { ToastsManager } from 'ng6-toastr';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchResult } from '../search/search/search-result.model';
import { Cart } from '../core/models/cart.model';
import { CartService } from '../core/services/cart.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  errors: Errors = {errors: {}};
  results: SearchResult[];
  searchResult: SearchResult[];
  loading: boolean;
  isSubmitting = false;
  p:number;
  podcasts:any;
  docSearchResults:DocSearchResults;
  itemsPerPage: number;
  totalItems: number;
  pages: number;
  previousPage: number;
  fileUrl:string;
  loadingMessage=true;
  view_more_state: Array<boolean> = [];
  isPlayed: boolean = false;
  isAuthenticated: boolean;
  currentUser:User
  //----Modal View----------------------------------
  modal_title: string;
  modal_description: string;
  modal_image: string;
  modal_link: string;
  modal_podcast_title: string;
  modal_audio_url:  string;
  modal_published_date: string;
  modal_duration: string;
  modal_episode: any;
  modal_podcast: any;
  //---- Search View ----------------------------------
  isLoading: boolean;
  searchTerm: FormControl = new FormControl()

  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;
  constructor( private router: Router, private userService: UserService, private route:ActivatedRoute, private alertService:AlertService,
     private lightService:LightService,private playerService:PlayerService, public toastr: ToastsManager, 
     vcr: ViewContainerRef, config: NgbModalConfig, private modalService: NgbModal,private cartService:CartService
  ) {
    this.toastr.setRootViewContainerRef(vcr);
    route.queryParams
    .filter(params => params.page)
    .subscribe(params=>{
      this.pages=params.page
    });
    config.backdrop = 'static';
    config.keyboard = false;
    this.podcasts = {};
  }
  toggleChild(){
    this.isPlayed = !this.isPlayed;
  }
  ngOnInit() {
    //put here transcipts scripts javascripts 
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );

    //------- Search Bar Value Changes ------------------
    this.searchTerm.valueChanges
       .debounceTime(300)
		   .subscribe(data => {
          this.isLoading = true;
   			  this.lightService.search_word(data).subscribe(response =>{
            this.isLoading = false;
            this.searchResult=response['docSearchResults'] ;
          },  
          err => {
  
          }
        )
    })
       
    this.userService.isAuthenticated.subscribe(
    (authenticated) => {
      this.isAuthenticated = authenticated;

      // set the article list accordingly
      if (authenticated) {
        // this.loadPage(1);
        // this.itemsPerPage=10;
      } 
      else {
        // this.router.navigateByUrl('/register'); 
      }
    }
  ); 
}


open_transcribe(transcribe, episode, podkast){
  this.modal_title = episode.title;
  this.modal_description = podkast.topPodcast.description;
  this.modal_image = podkast.topPodcast.thumbnail;
  this.modal_link = podkast.topPodcast.podcastId;
  this.modal_podcast_title = podkast.topPodcast.name;
  this.modal_audio_url = episode.fileUrl;
  this.modal_published_date = podkast.topPodcast.publishDate;
  this.modal_duration = episode.duration;
  this.modal_episode = episode;
  this.modal_podcast = podkast;
  this.modalService.open(transcribe, { size: 'lg' });

}
 
  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }
  renderPodcast(res:any):void {
    this.podcasts=res;
    this.view_more_state = [];
    for (let index in this.podcasts.docSearchResults[0].topPodcast.episodes)
    {
      this.view_more_state.push(false);
    } 
    this.totalItems=this.podcasts.maxPage;
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.pages=page;
      this.loadData();
    }
  }

  loadData() {
    this.lightService.getTopPodcasts( this.pages ).subscribe((res:any)=>this.renderPodcast(res));
  }
  
  subscribedPodcasts(podcastId:string){
    if(this.isAuthenticated){
      this.lightService.subscribedPodcasts(podcastId)
      .subscribe( data => {
        // this.success(data['message']);
        this.success('Subscribed to Podcast Successfully');
        },
      err => {
        this.error("Podcast subscribed failed")
        this.errors = err;
        this.isSubmitting = false;

        });
    }else{
      this.router.navigateByUrl('/login');

    }
  }
  updateResults(results: SearchResult[]): void {
    this.results = results;
  }
  playAudios(event, episode, podkast=null){
    var current_fileUrl = this.playerService.current_fileUrl;

    if(this.isPlayed == false && current_fileUrl != episode.fileUrl)
    {
      this.playerService.playAudio(episode.fileUrl, episode, podkast.topPodcast.publishDate, podkast.topPodcast.name, podkast.topPodcast.thumbnail);
      this.isPlayed=true; 
      this.child.ngOnInit();
    }
    else if(this.isPlayed == false && current_fileUrl == episode.fileUrl){
      this.playerService.api.play();
      this.isPlayed = true;
    }
    else if(this.isPlayed == true){
      this.playerService.api.pause();
      this.isPlayed = false;
    }
  }
  listenLater(podcastId:string,episodeId:string){
    if(this.isAuthenticated){
    this.lightService.listenLater(podcastId,episodeId)
    .subscribe( data => {
      this.success(data['message']);
    },
    err => {
      this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
      this.errors = err;
      this.isSubmitting = false;
    });
    }else{
      this.router.navigateByUrl('/login');
    }
  }

  turboTranscripts(episodeId:string,duration:string){

  }

  success(message: string) { 
    this.toastr.success(message);
  }

  error(message: string) {
    this.toastr.error(message);
  }

  info(message: string) {
    this.toastr.info(message);
  }

  warn(message: string) {
    this.toastr.warning(message);
  }

  clear() {
    this.alertService.clear();
  }
  open_read_more_modal(content, episode, podkast) {
    this.modal_title = episode.title;
    this.modal_description = episode.summary;
    this.modal_image = podkast.topPodcast.thumbnail;
    this.modal_link = podkast.topPodcast.podcastId;
    this.modal_podcast_title = podkast.topPodcast.name;
    this.modal_audio_url = episode.fileUrl;
    this.modal_published_date = podkast.topPodcast.publishDate;
    this.modal_duration = episode.duration;
    this.modal_episode = episode;
    this.modal_podcast = podkast;
    this.modalService.open(content, { size: 'lg' });
  }
  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if( _seconds.indexOf(':') >= 0){
      if(_seconds.split(":").length == 2)
        return "00:"+_seconds;
      return _seconds;  
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  view_more(index){
    this.view_more_state[index] = !this.view_more_state[index];
  }

  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
   this.cartService.addEpisodes(this.currentUser.userid,id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
   .subscribe(data=>{
     this.success(data['message']);
     });
  }  
  search_podcast(){
    let keyword = this.searchTerm.value;
    const author='';
    const t='';
    const w= '';
    this.router.navigate(['/search'],{queryParams:{ q:keyword,a:author,t:t,w:w,p:0}});
  }
  onEnterSearch(event) {
    this.search_podcast();
  }
}
