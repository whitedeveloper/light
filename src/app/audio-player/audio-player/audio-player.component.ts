import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit, Input , Inject} from '@angular/core';
import { PlayerService } from "../../core/services/player.service";
import { VgAPI } from "videogular2/core";
import {  UserService, Errors, User } from '../../core';
import { LightService } from '../../core/services/light.service';
import { ToastsManager } from 'ng6-toastr';
import { Router} from '@angular/router';
import { HostListener } from "@angular/core";
import * as $ from 'jquery';

@Component({
    selector: 'app-audio-player',
    templateUrl: './audio-player.component.html',
    styleUrls: [ './audio-player.component.css' ]
})
export class AudioPlayerComponent implements OnInit {
    @Input() showMePartially: boolean;
   
    isAuthenticated: boolean;
    type:string;
    preload:string = 'auto';
    api:VgAPI;
    sources:Array<Object>;
    currentUser:User
    episode: any;
    isLoaded: boolean;
    knob_left: any;
    is_minimized: boolean;
    scrHeight:any;
    scrWidth:any;
    fileUrl: string;
    playback_data = [ {key:0.5,value:"0.5x"}, {key:0.8,value:"0.8x"}, {key:1.0,value:"1.0x"}, {key:1.2,value:"1.2x"}, {key:1.3,value:"1.3x"}, {key:1.4,value:"1.4x"}
                    , {key:1.5,value:"1.5x"}, {key:1.6,value:"1.6x"}, {key:1.7,value:"1.7x"}, {key:1.8,value:"1.8x"}, {key:1.9,value:"1.9x"}, {key:2.0,value:"2.0x"}
                    , {key:2.3,value:"2.3x"}, {key:2.5,value:"2.5x"}, {key:2.7,value:"2.7x"}, {key:3.0,value:"3.0x"} ]
    is_mobile_view: boolean = false;
    @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
            this.scrHeight = this.window.innerHeight;
            this.scrWidth = this.window.innerWidth;
            if(this.scrWidth < 992)
                this.is_mobile_view = true;
            else
                this.is_mobile_view = false;    
    }

    constructor(@Inject(WINDOW) private window: Window, public playerService:PlayerService, private userService: UserService, private lightService:LightService, public toastr: ToastsManager,
        private router: Router) {
        this.playerService.fileUrls.subscribe(fileUrls=>{this.fileUrl=fileUrls;});
        this.userService.currentUser.subscribe(
            (userData) => {
              this.currentUser = userData;
            }
        );
        this.isLoaded = false;
        this.episode = null;
        this.knob_left = 0;
        this.is_minimized = true;
        
        this.scrWidth = window.innerWidth;
        if(this.scrWidth < 992)
        {
            this.is_mobile_view = true;
            this.is_minimized = false;
        }
        else{
            this.is_mobile_view = false;    
        }
        $('.side-menu-fixed').css('bottom', $('.audioplayer').height());

    }
    onPlayerReady(api:VgAPI) {
        this.isLoaded = true;
        this.playerService.api = api; //sharing api in playerService.
        this.api = api;
        this.api.getDefaultMedia().subscriptions.ended.subscribe(
            () => {
                // Set the video to the beginning
                this.api.getDefaultMedia().currentTime = 0;
                this.api.play();
                this.knob_left = 0;
            }
        );
        this.api.getDefaultMedia().subscriptions.seeking.subscribe(
            () => {
                this.knob_left = this.api.currentTime/this.api.duration*100;
                this.api.play();    
            }
        );
        this.api.getDefaultMedia().subscriptions.seeked.subscribe(
            () => {
                this.knob_left = this.api.currentTime/this.api.duration*100;
                this.api.play();
            }
        );
        this.api.getDefaultMedia().subscriptions.play.subscribe(
            () => {
                this.playerService.isPlaying = true;  
                if(this.playerService.isAudioChanged == true)
                {
                    this.playerService.isAudioChanged = false;
                    this.api.seekTime(this.playerService.seekTime);
                }
            }
        );
        this.api.getDefaultMedia().subscriptions.timeUpdate.subscribe(
            () => {
                // this.knob_left = this.api.currentTime/this.api.duration*100;
                if(Math.round(this.api.currentTime)% 60 == 0 && Math.round(this.api.currentTime) > 0){
                    if(this.isAuthenticated){
                        this.episode = this.playerService.episode;
                        this.lightService.addInprogress(this.episode.podcastId, this.episode.episodeId, this.api.currentTime, this.api.duration).subscribe( 
                            data => 
                            {   
                                this.playerService.getInProgress();
                            },
                            err => {
                            }
                        );
                    }
                }
            }
        );
        this.api.getDefaultMedia().subscriptions.pause.subscribe(
            () => {
                this.playerService.isPlaying = false;
                if(this.isAuthenticated){
                    this.episode = this.playerService.episode;
                    this.lightService.addInprogress(this.episode.podcastId, this.episode.episodeId, this.api.currentTime, this.api.duration).subscribe( 
                        data => 
                        {   
                            this.playerService.getInProgress();
                        },
                        err => {
                        }
                    );
                }    
            }
        );
    }
    ngOnInit() {
        // this.setAudio();
        this.userService.isAuthenticated.subscribe(
            (authenticated) => {
              this.isAuthenticated = authenticated;
      
              // set the article list accordingly
              if (authenticated) {
                
              } 
              else {
              }
            }
        ); 
    }
    buttonBackPress(){
        this.api.seekTime(this.api.currentTime-15);
    }
    buttonForwardPress(){
        this.api.seekTime(this.api.currentTime+30);
    }
    change_playback(value){
        this.api.playbackRate = value;
    }
    secondstotime(secs)
    {
        let t:any = new Date(1970,0,1);
        t.setSeconds(secs);
        var s = t.toTimeString().substr(0,8);
        if(secs > 86399)
            s = Math.floor((t - Date.parse("1/1/70")) / 3600000) + s.substr(2);
        return s;
    }
    success(message: string) { 
        this.toastr.success(message);
    }
    
    error(message: string) {
        this.toastr.error(message);
    }
    bookmark(){
        this.episode = this.playerService.episode;
        if(this.episode == null)
            return false;
        if(this.isAuthenticated){
            let request_form = {"podcastId": this.episode.podcastId, "episodeId": this.episode.episodeId, 
                                "timestamp": this.secondstotime(this.api.currentTime), "userId": this.currentUser['userid']};
            this.lightService.bookmark(request_form).subscribe( 
                data => 
                {   
                    this.success(data['message']); 
                },
                err => {
                    this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
                }
            );
        }
        else
        {
            this.error("Please login");
        } 
    }
    minimize(){
        this.is_minimized = !this.is_minimized;
        $('.side-menu-fixed').css('bottom', 0);
    }
}
