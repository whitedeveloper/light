import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { LightService } from '../../core/services/light.service';
import { PlayerService } from '../../core/services/player.service';
import { AudioPlayerComponent } from '../../audio-player/audio-player/audio-player.component';
import { UserService, User, Errors } from '../../core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng6-toastr';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartService } from '../../core/services/cart.service';
import {CommonModalComponent} from '../../common/modal.component';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent implements OnInit {

  @ViewChild(AudioPlayerComponent) child:AudioPlayerComponent;
  currentUser:User;
  bookmarks:any = [];
  sort_data = [{ key:"name", value:"Episode Name(A → Z)" }, { key:"name_big", value:"Episode Name(Z → A)" },
               { key:"date", value:"Created Date(Old → New)" }, { key:"date_big", value:"Created Date(New → Old)" },
               { key:"p_date", value:"Released Date(Old → New)" }, { key:"p_date_big", value:"Released Date(New → Old)" }]
  selectedAll: any;
  selectValues: Array<{episodeId: string, selected: boolean}> = [];
  modal_episode: any;
  modal_podcast: any;
  modal_index: number;
  errors: Errors = {errors: {}};
  scroll_limit: any;

  constructor(private userService:UserService,private router:Router,
    private lightService:LightService, public toastr: ToastsManager, vcr: ViewContainerRef, private playerService:PlayerService,  
    private modalService: NgbModal, private cartService:CartService) {
      this.toastr.setRootViewContainerRef(vcr);

  }

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.getBookmark();
        } 
        else {
          this.router.navigateByUrl('/register');
        }
      }
    ); 
    this.scroll_limit = 10;
  }

  renderPodcast(res:any):void {
    this.bookmarks=res;
    this.sort_by('date_big');
    // //--initialize check box values---------------
    for (let item of this.bookmarks)
    {
      this.selectValues.push({ episodeId: item.episodeId, selected: false });
    } 
  }

  getBookmark(){
    this.lightService.getBookmark([`userId= ${this.currentUser['userid']}`])
    .subscribe((res:any)=>this.renderPodcast(res));
  }

  logout() {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/');
  }

  selectAll() {
    for (var i = 0; i < this.selectValues.length; i++) {
      this.selectValues[i].selected = this.selectedAll;
    }
  }
  isPlayed: boolean = false;
  toggleChild(){
    this.isPlayed = !this.isPlayed;
  }
  toTime(timeString){
    var timeTokens = timeString.split(':');
    return 3600*Number(timeTokens[0])+60*Number(timeTokens[1])+Number(timeTokens[2]);
  }
  playAudios(event, episode){
    if(episode.filePath == this.playerService.fileUrl && this.playerService.isPlaying == true)
    {
      this.playerService.api.pause();
      this.playerService.isPlaying = false;
    }
    else if(episode.filePath == this.playerService.fileUrl && this.playerService.isPlaying == false)
    {
      this.playerService.api.play();
      this.playerService.isPlaying = true;
    }
    else{
      this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
      .subscribe((res:any)=>{
        this.playerService.playAudio(episode.filePath, episode.podcastName, res.releaseDate, episode.episodeName, episode.image, episode, this.toTime(episode.timestamp));
        this.isPlayed=true;
        this.playerService.isAudioChanged = true;
      }) 
    }
  }
  isAuthenticated: boolean;
  success(message: string) { 
    this.toastr.success(message);
  }
  error(message: string) {
    this.toastr.error(message);
  }
  subscribedPodcasts(podcastId:string){
    // if(this.isAuthenticated){
    this.lightService.subscribedPodcasts(podcastId)
    .subscribe( data => {
      this.success(data['message']);
      this.success('Subscribed to Podcast Successfully');
      //this.router.navigateByUrl('/')
      },
    err => {
      this.error("Podcast subscribed failed")
      this.errors = err;

      });
  }
  listenLater(podcastId:string,episodeId:string){
    // if(this.isAuthenticated){
    this.lightService.listenLater(podcastId,episodeId)
    .subscribe( data => {
      this.success(data['message']);
    },
    err => {
      this.error(JSON.stringify(err)+" Episode add to ListenLater failed");
      this.errors = err;
    });
  }
  sort_by(key){
    if ( key == 'name' )
    {
      this.bookmarks.sort((n1, n2) => {
        return this.naturalCompare(n1.episodeName, n2.episodeName);
      });
    }
    else if ( key == 'date' )
    {
        this.bookmarks.sort((n1, n2) => {
        return this.naturalCompare(n1.createdDate.toString(), n2.createdDate.toString());
      });
    }
  
    else if ( key == 'name_big' )
    {
      this.bookmarks.sort((n2, n1) => {
        return this.naturalCompare(n1.episodeName, n2.episodeName);
      });
    }
  
    else if ( key == 'date_big' )
    {
      this.bookmarks.sort((n2, n1) => {
        return this.naturalCompare(n1.createdDate.toString(), n2.createdDate.toString());
      });
    }

    else if ( key == 'p_date' )
    {
      this.bookmarks.sort((n1, n2) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }

    else if ( key == 'p_date_big' )
    {
      this.bookmarks.sort((n2, n1) => {
        return this.naturalCompare(n1.releaseDate.toString(), n2.releaseDate.toString());
      });
    }
  
  }
  naturalCompare(a, b) {
    var ax = [], bx = [];
 
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
 
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
 
    return ax.length - bx.length;
  }

  padTime(t) {
    return t < 10 ? "0"+t : t;
  }
  secondsToMinute(_seconds){
    if( _seconds.indexOf(':') >= 0){
      if(_seconds.split(":").length == 2)
        return "00:"+_seconds;
      return _seconds;  
    }
    var hours = Math.floor(_seconds / 3600);
    var minutes = Math.floor((_seconds % 3600) / 60);
    var seconds = Math.floor(_seconds % 60);
    return this.padTime(hours) + ":" + this.padTime(minutes) + ":" + this.padTime(seconds);
  }
  open_read_more_modal(content, episode, podkast, index) {
    this.lightService.getEpisodesDetail(episode.episodeId, episode.podcastId)
    .subscribe((res:any)=>{
      this.modal_episode = res;
    })
    this.modal_podcast = podkast;
    this.modal_index = index;
    this.modalService.open(content, { size: 'lg' });
  }
  onScroll(){
    this.scroll_limit += 5; 
  }
  apply_transcribe(index, cartModal){
    this.transcribe(index);
    this.modalService.open(cartModal, { size: 'lg' });
  }
  transcribe(index){
    var item = this.bookmarks[index];
    this.lightService.getEpisodesDetail(item.episodeId, item.podcastId)
    .subscribe((res:any)=>{

      this.addToCart(item.episodeId, item.podcastName, item.episodeName, this.secondsToMinute(res.duration), item.image, item.filePath, item.podcastId);
    })
    
  }
  addToCart(id:string,podcastName:string,episodeName:string,duration:string,imgPath:string,mp3:string,podcastId){
    this.cartService.addEpisodes(this.currentUser.userid, id,podcastName,episodeName,duration,imgPath,mp3,podcastId)
    .subscribe(data=>{
      this.success(data['message']);
      this.cartService.loadCartItems();
    });
  } 
  creditCheckout(link){
    if (this.cartService.checkOut() == false){
      this.toastr.info("Your balance is not enough.");
      const modalRef = this.modalService.open(CommonModalComponent, { centered: true });
      modalRef.componentInstance.src = link;
    }
    else{
      this.modalService.dismissAll();
    }
  }
}

