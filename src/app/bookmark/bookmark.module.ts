import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookmarkRoutingModule } from './bookmark-routing.module';
import { BookmarkComponent } from './bookmark/bookmark.component';
import { AudioPlayerModule } from '../audio-player/audio-player.module';
import { SharedModule } from '../shared/shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    BookmarkRoutingModule,
    SharedModule,
    AudioPlayerModule,
    InfiniteScrollModule
  ],
  declarations: [BookmarkComponent]
})
export class BookmarkModule { }
