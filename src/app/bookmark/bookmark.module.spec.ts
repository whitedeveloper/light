import { BookmarkModule } from './bookmark.module';

describe('InprogressModule', () => {
  let inprogressModule: BookmarkModule;

  beforeEach(() => {
    inprogressModule = new BookmarkModule();
  });

  it('should create an instance', () => {
    expect(inprogressModule).toBeTruthy();
  });
});
