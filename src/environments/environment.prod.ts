export const environment = {
  production: true,
  api_url: 'https://api.light.sx/ext-api/v1/podcast',
  auth_url:'https://api.light.sx/ext-api/v1/auth'
};
